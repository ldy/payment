<?php
/**
 *
 * @createTime: 2016-08-01 11:37
 * @description: 微信配置文件
 */

return [

    'app_id'            => '00032161',  // 公众账号ID
    'cus_id'            => '55079104816PJXP',// 商户id
    'md5_key'           => '55079104816PJXP04',// md5 秘钥
    'sign_type'         => 'MD5',// MD5  HMAC-SHA256

    'limit_pay'          =>  ['no_credit'],

    'notify_url'        => 'http://172.16.2.46:8080/vo-apidemo/OrderServlet',

    'redirect_url'      => 'https://helei112g.github.io/',// 如果是h5支付，可以设置该值，返回到指定页面

    'return_raw'        => false,// 在处理回调时，是否直接返回原始数据，默认为true
];

//测试账号

//商户号：55079104816PJXP
//交易密码：Zyzswz1234
// 登录密码：Zyzs123456
//appid：00020281 交易密钥：55079104816PJXP04