<?php
require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Query;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$config = require_once __DIR__ . '/config.php';

$payData = [
    'transNo' => 'WechatRefund20230426234103',//原退款交易对应的商户订单号
];
try {
    $ret = Query::run(Config::HSQ_QUERY_REFUND, $config, $payData);
} catch (PayException $e) {
    exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);