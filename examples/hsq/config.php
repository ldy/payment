<?php
/**
 *
 * @createTime: 2023-04-24 18:37
 * @description: 慧收钱配置文件
 */

return [

    /* 测试账号 begin */
//    'key'=> '86a8eec5ae958e9948b7450439cc57e2', //商户key
//    'merchantNo' => '814000473149', //商户号
//    'appid' => 'wxb63c89abb9243ad0', //微信公众号APPID
//    'private_key' => dirname(__FILE__) .'/certFile/hsq/hsq_demo_pri.pfx', //私钥
//    'public_key'  => dirname(__FILE__) .'/certFile/hsq/hsq_demo_pub.cer', //公钥
//    'private_key_pass' => '123456', //私钥密码
//    'request_url' => 'https://test-api.huishouqian.com/api/acquiring',
    /* 测试账号 end */

    /* 猎电 begin */
//    'key'=> '86a88c6b33321988db7ae217aa4ed36a', //商户key
//    'merchantNo' => '862003023920', //商户号
//    'appid' => 'wxbda017d7ba602dda', //微信公众号APPID
//    'private_key' => dirname(__FILE__) .'/certFile/hsq/liedian_pri.pfx', //私钥
//    'public_key'  => dirname(__FILE__) .'/certFile/hsq/liedian_pub.cer', //公钥
//    'private_key_pass' => 'Ldcharge888', //私钥密码
    /* 猎电 begin */

    /* 猎维(智能充) begin */
    'key'=> '6a84817f0214990c0e52871aa33ca777', //商户key
    'merchantNo' => '861002923881', //商户号
    'appid' => 'wxfbe753bb28212636', //微信公众号APPID
//    'private_key' => dirname(__FILE__) .'/certFile/hsq/liewei_pri.pfx', //私钥
    'private_key' => '-----BEGIN PRIVATE KEY-----
MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDXwmROITU7SwEN
CEuPuLdpD1xAXafgvDXbDWR/Loj4VQnWVil3Yp6XKvec29uYj8mBDymGmJwn+zSi
vSzl6ZzoGdS1bBpaTCC9tUT0+PSXuR+piWqtgXPstuwWCT7tg19hDo36J4pZoTJC
J4VCnO7ZsSuDr/xhHIbBs6/IC/JPo5GKZ1sroIJpGrSa/bhYpTAq7md5LY/Shri5
YoKMSz+2HpRVmnrsboEoGYS/CeNwegbzJXajFl0T1RLKMV5hWyLJjfNkEiv0bOrd
ZiYgqMblfUNnxH0SiPWT2mCLUXnAExIgH77qbt6krnwMPWTkjKtMnVa3adPyCQgg
NEFw1AvBAgMBAAECggEAQfE5st9XY53S3mbQoUZ1V/jH2yxEqN0dIKwBMRXJ6wwi
i42d8s0DZ3RMZXaUsRYRIg6+RFLUNywBtFNZK5OwzwdGCYTWsQ5fW1vRjYWhS9Xa
pXhIUGsmQQzPLxT4VKM4PXOzVOdM04P+1lU511gbcBFEGWVqH0wWq5D9goiUMF5f
6JZH8D5eu06bIss1WK8LK6p0gpizyPvRLNnJFEDSPm+QYjd//x7Dq4GN1RnlvjzO
qaXM2lFUFB+i9Fv+/rGl0aK0x9jnwF9FZYqbS1TczLkAXkjWDUz74+rM86qlQ6vr
DJMjtA+1PNtm3bp08MZEd6jGhCc1HhDAfhn17r90gQKBgQDvHs+ucy5KfXXcQpR/
9RCbBrgIMQjvVusv1FuSgp2enSd2da+ngmqnNzCKfo/IgwfFTGZ+XJpoL/s5C2Jk
2/rWtilP99dOPum8T4Ow7Rn1doQoO5LgQyBefo2ukbRkeHEcfGonE1DBbTid93Vy
yRj6rz5rSYAfd4pGSMa8GQbK6QKBgQDm/WtYFEJ7e+whgq+DVk8VEYgXLzsDluMN
4ibj/23l0Rafrfim50HnqgmaSPeGg8r+uaFP2RqCsAP86xGy6S+sHrxcVCemGJoW
ipzXvgyHVeZN6/EkSQSG0IEcbvJe15s6RChPbzEDXdiJ/pOcytCkS4Gn26cTOCOF
P93wy42DGQKBgQDPTeX9NqwuNkCk2TAHEv33eMa5i1FUtgUlrTRKe2SM63AX+gVz
8tPl7inuF7Utv7NP8GF3MW9H3jkthloiWLhJxxlf091cX5yh/kmMbLIxpTMD8knu
lSON6KeKbZV3+RcO/CnHgK5lUvxfrvaqsf8u7hjuSIWEQk2li1iIICZMaQKBgQDO
5HVdMuhJSjm52h/ydz/mEwneGkO1d1Rvfvxugr6gdaLZ6y0n8UKqiTaIpopuRWVw
ujuuFTsViypHQxVbNlmoxO7Zk9bfgnGhdw38fxEhtuLNMGOQqIYRanf6S7VzFZLH
oXypiWJRLaFiGE+GxHIUdjzvaGJBaKsIEqhroSZjIQKBgCePXQbCGno8hHM2N6ME
qQYYnfqrXsO7KMUHB+Soq/kApqHDXhfzjN4wScnyku2j5Wm6AES6RWRTyWiJmXys
2fY4O3MK04L5iazSRRt3XWncHUGjxmuxBwHv2FnUnGCqTvwBvmAW4+Y999Gnx/bb
o/jP9AKFHhvs9aCh2SawdrE9
-----END PRIVATE KEY-----
',
//    'public_key'  => dirname(__FILE__) .'/certFile/hsq/liewei_pub.cer', //公钥
    'public_key'  => '-----BEGIN CERTIFICATE-----
MIIDCTCCAfGgAwIBAgIGAYeYip3AMA0GCSqGSIb3DQEBDQUAMCExEjAQBgNVBAoMCUNGQ0EgT0NB
MTELMAkGA1UEBhMCQ04wIBcNMjMwNDE5MDgwMzUzWhgPMjA1MDA5MDQwODAzNTNaMGgxETAPBgNV
BAMMCGtjYmViYW5rMRcwFQYDVQQLDA5rY2JlYmFuay1ydXlhbjEZMBcGA1UECwwQT3JnYW5pemF0
aW9uYWwtMTESMBAGA1UECgwJQ0ZDQSBPQ0ExMQswCQYDVQQGEwJDTjCCASIwDQYJKoZIhvcNAQEB
BQADggEPADCCAQoCggEBALYPjL4VLm0awYyHdtsTELLixB7efvPjd9u+Edd9Ht+KQ3I0GCEUvoOe
gnrCljbkWNe1t4CXHg7/mI1PTCKYzdkG7jeGvgNeuxFqdgxbKfzCojRR3FOMn1GUw99XZXh6japk
oDoSUbeF7fCtiSXhyOD6P+wzSPuh4hjqSTvzWZjDGL8svgdbav+7kWLNdwFDUh5nTJ4NyfejOqdy
l+QnfmdncnmGq4qpumIzrtX9QQh4uhggJ0WiZ7NtSHg9wp6Jc6F77K9vZw8SrvXIpG9H+xTWbkdJ
iYu0sROzXa6XcUD08LqrQPBc8R2uOneJdaIg7gLNUlpYVHZGiqKoQeqbBOUCAwEAATANBgkqhkiG
9w0BAQ0FAAOCAQEAHybso+WrC9tnOoDEzB6atUfjmlmRXvrCW4TzMGWoS7UfZJi4Gc+TXKhcqK26
fFWADfhKOJ75wyIUFL3c+YWAJ/pSYQR6c1DnSHvGd9OnOZs5G2W7dX0PiLDz68faOTpiMCojC9Ud
km3dpiUHS7FZcHI4Rrn/w+azvo/Ce1xMVrxq6FX0cicCmoiQEy1l1rUvrfUkQYLqTEgd36+L6Mff
oTlgHaWUgf43Gnn1YlH0UiJnVShPfpagXAhMz/jM5d5R1CChnLPMBHbnF3oXIh/3ywqlbG5kZDb3
ba8DPD6ZQ0qdNp9boo6Jb3JH0lhs6eTHEw6YXtyThHX8QF68fAll9Q==
-----END CERTIFICATE-----', //公钥

    'private_key_pass' => 'Ldcharge888', //私钥密码
    /* 猎维(智能充) begin */

    'returnUrl' => 'https://api.ldne.liediantech.com/Users/StoreUsersCardTrade/userCardRecharge', //异步回调地址

];
