<?php
require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Transfer;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$config = require_once __DIR__ . '/transferConfig.php';

/*
 * 代付交易一次处理的请求条数（payData）有限制，不超过 5 个，超过 5 个：交易请求记录条数超过上限!
 * 请求报文体中的 trans_no 要求全局唯一，如果发现有重复则报错
 */
$payData = [
//    [
//        'trans_no' => "TN" . strtotime(date('Y-m-d H:i:s',time())) . rand(1000,9999), //商户订单号
//        'trans_money' => 1,//转账金额，单位：元
//        'to_acc_name' => '王勇', //收款人姓名
//        'to_acc_no' => '6212262201023557228', //收款人银行帐号
//        'to_bank_name' => '中国工商银行', //收款人银行名称
//        //支付方式说明：对私可不填写省、市、支行；对公必须填写。
//        'to_pro_name' => '上海市', //收款人开户行省名
//        'to_city_name' => '上海市', //收款人开户行市名
//        'to_acc_dept' => '支行', // 收款人开户行机构名
//        'trans_card_id' => '310115200501018559', //银行卡身份证件号码
//    ],
    [
        'trans_no' => "TN" . strtotime(date('Y-m-d H:i:s',time())) . rand(1000,9999), //商户订单号
        'trans_money' => 1,//转账金额，单位：元
        'to_acc_name' => '郑健康', //收款人姓名
        'to_acc_no' => '6217921957329496', //收款人银行帐号
        'to_bank_name' => '浦发银行', //收款人银行名称
        'trans_card_id' => '610112199210152519', //银行卡身份证件号码
    ],
];


try {
    $ret = Transfer::run(Config::HSQ_TRANSFER, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}
echo json_encode($ret, JSON_UNESCAPED_UNICODE);