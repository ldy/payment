<?php
require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/../testNotify.php';

use Payment\Common\PayException;
use Payment\Client\Notify;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$config = require_once __DIR__ . '/config.php';

$_POST['req'] = 'method=CALLBACK&format=json&sign=115a949ed99f50978e0f301ee49b288cb83ff3cc0fe25ae8a6e9397524f34417ad32887fde2f3c20879879a5cb77f77c80161a7e868cf6d4b73879ec859f95d3a4531216f551abb8da050921a3a0db49362a7410c9ed8c71ea789ea56e49f4c9a7d82e437dcd41e1e58734720f0bba1c450f380948241c3444ada85700a6d856e0f2a8a01ff085a23092e0cad006d8a142e76af4907e4c17b2db47f2b512acb839e4b40a896bb8f98b594677590d39ee3af92cd324dddc6d4ce01cb19bbc4020b205415cd8d5b5b0ab6c0fbc7968ecb00571c43dd7f4a20cf8e9dc74209b17b7f4c75a4f4cae71fae16cac6ba2578f826d5ff9cf1dde2bed9ed8f6430909cac3&signType=RSA2&signContent=%7B%22buyerName%22%3A%22oAFzv5a67bJ0aGk7cB-RfK0Yap8s%22%2C%22channelOrderNo%22%3A%2223050815371110591231100592117922%22%2C%22finishedDate%22%3A%2220230508153801%22%2C%22fundBankCode%22%3A%22OTHERS%22%2C%22fundChannel%22%3A%22OTHERS%22%2C%22goodsInfo%22%3A%22%E6%94%AF%E4%BB%98%E6%B5%8B%E8%AF%95%22%2C%22memo%22%3A%22%7B%5C%22openid%5C%22%3A%5C%22oAFzv5a67bJ0aGk7cB-RfK0Yap8s%5C%22%2C%5C%22appid%5C%22%3A%5C%22wxfbe753bb28212636%5C%22%2C%5C%22latitude%5C%22%3A%5C%2222.33%5C%22%2C%5C%22spbillCreateIp%5C%22%3A%5C%22150.158.53.11%5C%22%2C%5C%22paylimit%5C%22%3A%5C%22no_credit%5C%22%2C%5C%22longitude%5C%22%3A%5C%22171.21%5C%22%7D%22%2C%22orderAmt%22%3A%221%22%2C%22orderStatus%22%3A%22SUCCESS%22%2C%22payOrderNo%22%3A%224200001871202305087489820896%22%2C%22payType%22%3A%22WECHAT_APPLET%22%2C%22requestDate%22%3A%2220230508153751%22%2C%22respCode%22%3A%22000000%22%2C%22respMsg%22%3A%22%E4%BA%A4%E6%98%93%E6%88%90%E5%8A%9F%22%2C%22tradeNo%22%3A%2225000020230420101055003876003836%22%2C%22transNo%22%3A%22WechatPay20230508153751%22%7D&version=1.0.0&merchantNo=861002923881';

try {
    $ret = Notify::run(Config::HSQ_CHARGE, $config, new TestNotify());
    var_dump($ret);
} catch (PayException $e) {
    echo $e->errorMessage();
}