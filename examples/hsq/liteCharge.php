<?php
require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$config = require_once __DIR__ . '/config.php';
//print_r($config);die;

$payData = [
    'transNo' => "WechatPay".date("YmdHis"), //商户订单号
    'orderAmt' => 100,//交易金额(单位分)
    'goodsInfo' => '支付测试', //商品说明
    'openid' => 'oAFzv5a67bJ0aGk7cB-RfK0Yap8s',
    'longitude' => '171.21', //经度
    'latitude' => '22.33', //纬度
];

try {
    $ret = Charge::run(Config::HSQ_CHANNEL_LITE, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);
