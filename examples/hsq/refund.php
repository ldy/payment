<?php
require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Refund;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$config = require_once __DIR__ . '/config.php';

$payData = [
    'transNo' => "WechatRefund".date("YmdHis"), //退款订单号
    'refundType' => 1, //退款类型,
    'origTransNo' => 'WechatPay20230426234103',//原商户订单号
    'origOrderAmt' => 1, //原订单金额
    'orderAmt' => 1, //退款金额
    'requestDate' => date('YmdHis'), //原交易日期（Ymd，如：20190620）
    'refundReason' => '商品已售完',//退款原因
];

try {
    $ret = Refund::run(Config::HSQ_REFUND, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);