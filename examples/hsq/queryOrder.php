<?php
require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Query;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$config = require_once __DIR__ . '/config.php';

$payData = [
    'transNo' => 'WechatPay20230506225945', //商户订单号
    'queryType' => 1, //查询类型
];

try {
    $ret = Query::run(Config::HSQ_QUERY, $config, $payData);
} catch (PayException $e) {
    exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);