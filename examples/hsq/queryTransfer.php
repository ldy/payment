<?php
require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Query;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$config = require_once __DIR__ . '/transferConfig.php';

//代付交易一次处理的请求条数（payData）有限制，不超过 5 个，超过 5 个：交易请求记录条数超过上限!
//请求报文体中的 trans_no 要求全局唯一，如果发现有重复则报错

//代付交易查证一次处理的请求条数（payData）有限制，不超过 5 个，超过 5 报：交易请求记录条数超过上限!
//批量查询的多笔订单每笔订单时间间隔不得超过 24 小时

//测试环境必须传 trans_batchid 正式环境不需要
$payData = [
//    [
//        'trans_batchid' => 24716624, //宝付批次号
//        'trans_no' => "TN16826631972112", //商户订单号
//    ],
    [
//        'trans_batchid' => '291675877', //宝付批次号
        'trans_no' => "TN16833643347311", //商户订单号
    ],
];


try {
    $ret = Query::run(Config::HSQ_QUERY_TRANSFER, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}
echo json_encode($ret, JSON_UNESCAPED_UNICODE);