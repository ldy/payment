<?php
/**
 *
 * @createTime: 2023-04-24 18:37
 * @description: 宝付代付配置文件
 */

return [

    /* demo begin */
//    'member_id'=> '100000178', //商户号
//    'terminal_id' => '100000859', //终端号
//    'private_key' => dirname(__FILE__) .'/certFile/baofoo/demo_pri.pfx', //私钥
//    'private_key' => '-----BEGIN PRIVATE KEY-----
//MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAPI7Hb+2X0/1r4+x
//JXBoFRgFSN9G4xSSvlfKFd6ynYpHklLLfNG6z21bw+DDWvrkNpcbAjAzfgMZAXmB
//JKQRTMQM9RBxu2978ot1KJJBfk1Njw1lWrYCZiQ1uaNhXykHrBHYiBxDE/uVSAEm
//Q9IJyxB++khsNe7WWT2re2JW+OzfAgMBAAECgYEAz5OLRbth6CKRJODYRYBb+y6k
//KPoVJI8v4AlEPofv6wy0PpE0UIH2uS83J0ghkfi5Mzoo4OdvZ/Yoxle9738HuRrD
//VYzDgsr2mGmr11ESVhe7jmdrPryURai0qedjpSVgfY9QrQsgOilPR+GYkkOh0szh
//wkm4KHIKx8bdxpHvHCECQQD+IGFGNHov+3/HQJmcuqLRUizEbsi4mkKhiqcInQ48
//2o+7uoysVYjszIkAlpDXVlxMqPEeVn0p9rVChJxiBMlxAkEA9ARJECnNP+y6eePT
//6ueii10p1wajUjcxL30bqYLPVWuhSICmttCoNiApIj4HFQSQBxDZXoSODEuo9m3T
//WJFzTwJBAN7wXSYP55msk36jx59dhHUKGEgDwIdinU3Gq5682b69JxdUIxEUwNis
//3wvrCwo+sx51n4Iz8f4cdwvx9pdvB6ECQQDd9Lfwt9U2fEHydUVhumijk45nRGZy
//djmLFKWAvreQ32HI7Ry31XvsH7zKpNkUSR4pDy5pRvFeRcPew28mdMcJAkAPW9D4
//FCgJAfH1v1be8YkP44sJ4nRlcm03hOx8/Ebp/Xx+lqHNsf0RIg+vO9PuD7vEm3ZA
//z5UXe4TamX6HwMxH
//-----END PRIVATE KEY-----
//',
//    'public_key'  => dirname(__FILE__) .'/certFile/baofoo/demo_pub.cer', //公钥
//    'public_key'  => '-----BEGIN CERTIFICATE-----
//MIIDFjCCAn+gAwIBAgIJAKIW6tKw+5gIMA0GCSqGSIb3DQEBBQUAMGYxDzANBgNV
//BAMTBlJvb3RDQTEPMA0GA1UECxMGQmFvZm9vMQ8wDQYDVQQKEwZCYW9mb28xETAP
//BgNVBAcTCFNoYW5naGFpMREwDwYDVQQIEwhTaGFuZ2hhaTELMAkGA1UEBhMCQ04w
//HhcNMTQxMjIyMTAxMTE0WhcNMjQxMjE5MTAxMTE0WjBmMQ8wDQYDVQQDEwZSb290
//Q0ExDzANBgNVBAsTBkJhb2ZvbzEPMA0GA1UEChMGQmFvZm9vMREwDwYDVQQHEwhT
//aGFuZ2hhaTERMA8GA1UECBMIU2hhbmdoYWkxCzAJBgNVBAYTAkNOMIGfMA0GCSqG
//SIb3DQEBAQUAA4GNADCBiQKBgQCpBL+SzNNpeJVAhP8XO91dPrQHf6w2k7aqwnWG
//jnYjRlmy7iaeUJT/wb3PoH+ioGNTJyPHvfYHalEmvi133K3N8NdHizaYTjm1hgNB
//hBIK8aPZCR76C3UtQPv4mqA5fTZEWnRbr8JrVJoFNEN2+4JQwCeDtaC3OzPPflET
//t6zdOQIDAQABo4HLMIHIMB0GA1UdDgQWBBSjwZD5kVF2fblxVW68Z2iWSvakoDCB
//mAYDVR0jBIGQMIGNgBSjwZD5kVF2fblxVW68Z2iWSvakoKFqpGgwZjEPMA0GA1UE
//AxMGUm9vdENBMQ8wDQYDVQQLEwZCYW9mb28xDzANBgNVBAoTBkJhb2ZvbzERMA8G
//A1UEBxMIU2hhbmdoYWkxETAPBgNVBAgTCFNoYW5naGFpMQswCQYDVQQGEwJDToIJ
//AKIW6tKw+5gIMAwGA1UdEwQFMAMBAf8wDQYJKoZIhvcNAQEFBQADgYEAcg56wgbV
//yZjb1mD3NZH3PIdodOA4CP5XC/fWQqJMSFB9ngvR/hH058E2+QiWHhN5HkQgmeOR
//xkuiDTVKosyylEYVsyFNaKKASufKH2eVXc4fUj/vd660ictNbbwTYuV4m7r5ZI8O
//4avXIZc6APJTqHAXYK84LzvSaQtXbPhJA0E=
//-----END CERTIFICATE-----
//', //公钥
//    'private_key_pass' => '123456', //私钥密码
//    'base_url' => 'https://paytest.baofoo.com/baofoo-fopay/pay', //测试接口上线时取消注释
    /* demo end */



    /* 猎电 begin */
//    'member_id'=> '1272710', //商户号
//    'terminal_id' => '80808', //终端号
//    'private_key' => dirname(__FILE__) .'/certFile/baofoo/liedian_pri.pfx', //私钥
//    'public_key'  => dirname(__FILE__) .'/certFile/baofoo/liedian_pub.cer', //公钥
//    'private_key_pass' => 'Ldcharge888', //私钥密码
    /* 猎电 end */


    /* 猎维(智能充) begin */
    'member_id'=> '1272712', //商户号
    'terminal_id' => '80814', //终端号
//    'private_key' => dirname(__FILE__) .'/certFile/baofoo/liewei_pri.pfx', //私钥
    'private_key' => '-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDI3+qU67U/JPFF
nOL5qOt8QsbG6gHxUv/PjYNnMyLy6UXf04EXNfAPeqzHLg1n4RCU2EHazoeGufby
rnQfgb4q6xaq1RykxP2gCUPEj6VdpC82K99Gj6gMiquvU3AdhTMFBZnGWx3ZdU8O
IbFO0yplOjUXAYwpeYAAu2FxUxoXr6gKR+KXQpmKe+fTQSXeF6c/qAysNOut4L6L
YmbO+hErb0qVqpCcDt07MI9PFJ2vOiWAGoLToePcgNMX9jFcfvLzSc1srJnKYL+k
GFrlomwRWzhQK7u1/OADknb8D4DwAoEGaDwqqgO+5xSfT9QN+c4Ns/OS0x8F7K4Z
Xg5CJnxxAgMBAAECggEAW7/at7aJjJPlF/S2v23mUVV20Xgk9wMAEN8EO+jhd64Q
2lESz3U1dvyDWl6whNgJ6NJ2EejvghwCrYn9RD7Qi23hZpn8gU3wwl4489jgoUrS
vZ2AWAxZp5DXJjeoJ/OlABA00qLfmWm5eCzvfZ95586UXTO5v1/dlXgA4/AfJN2o
DVf7T/yktKtJ4AAayHVQ0Na7m1bLlQGNVpKe9p9868ioflTFpXxpFa/ALmal4HBz
VSPbZh4pmz5IT6SeRkbBE/qJc99y60BDwAcFJ7xPA78IYmaHCphfXXNUhjNOH/HD
GyBor38ir3vDEtd3KKyM9WOnT5Mi5f/KbQv3wMsj+QKBgQDKGpgjRaklP7x/VKFr
hgSlV3Vnwd4JW4kYbylAfQ0DqsLVEm8FANyptz9RtFXZaXgv92u70LD94Aa+Y3RO
MLNw79A3bHMI4P7LQwg9fW8PzrlnZhimgBT3glDIkVB/k7/hpzPHQBd8GpgOvIsl
BS+IrCoi6q0Ins1YLLep57RsDwKBgQD+cWe7jlqxU3QtyBqquRbGz3OnYLb8OCD/
aSRAkIG6KN0ir2b0y6KN8epzjNURfTT8yiXV9frN226tFoHNdUQmZY3kwz2HCVcn
OkxLdmJ5745eC/erYG6jnD2/XxC6E3uAoLxRVxVkrOQmjMNmoXLNg73f4uGQS6FB
mOvLXbAPfwKBgD2jpsr7L/9gVQkwspqsoTYcWn5url2EQWfmX1p/yTxZFFsP6DRK
cwKAFRvCOHy6oiFfAMijeZe0f9oJUyuVxuSm/4Yn2V3X4V3078pxOzJDBmD2zoXZ
gouKf0DssPR4B4m3CR+3+HGtWrsD3hxIXJr+h0tm76c1Z8OAv/Nd67UZAoGAGn9i
5yRfC4J+Ydop9MeLuK6YA8FNrRyHSzH+5X5nFCT3SoprtWiTiCa+XMQ42DZq0zyW
Dh8svE1yzDHA2M2yiDEhQsaWXL67RcmLcnxkqwAommCyrTVAsGcwAwL6cfZfcTjM
A8iG4NIkfGNCYijEFOT4XB17khReOb+cJrSeBi8CgYAv/8eDFUByQlEjiJruIaLz
px8i10gUHVgCj1Cfeieftp6A2r6cpcPHCH3XPgYB79c2HLFdDV17E8vFxiouI4xz
3GIGef+eCn486yTn7fkijMoRrE3Ds4WUawC4ESYYPC6wF5x6DnBtP7I+z1VXMwzW
6YxH6Zx/J/4c8CcYm2PdqQ==
-----END PRIVATE KEY-----
',
    'public_key'  => dirname(__FILE__) .'/certFile/baofoo/liewei_pub.cer', //公钥
    'private_key_pass' => 'Ldcharge888', //私钥密码
    /* 猎维(智能充) end */




];
