<?php

require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$configStr  = '{"pay_var":"100","pay_type":"010","mch_id":"879104816000039","merchant_no":"879104816000039","terminal_id":"11558708","sub_appid":"wx2eaddb8e674394aa","app_id":"wx2eaddb8e674394aa","limit_pay":[""],"access_token":"3846bb6b7909488595c58e19a2211131","base_url":"https://pay.lcsw.cn/lcsw","notify_url":"http://api.test.ledianyun.com/payments/notify","use_sandbox":false,"return_raw":false} [] []
';
$config = json_decode($configStr,true);

// 订单信息
$payDataStr = '{"service_id":"015","terminal_trace":"20210610120224954476481015748920","total_fee":2,"order_body":"【乐店云】微信支付","open_id":"oDqg_5WEH2ROdZhRN7g3ERebi4e4","attach":"c7264439ab86385bc16a73b85ce3d134"} [] []
';
$payData = json_decode($payDataStr,true);

$config['base_url'] = 'https://pay.lcsw.cn/lcsw';

try {
    $ret = Charge::run(Config::SW_CHANNEL_LITE, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);