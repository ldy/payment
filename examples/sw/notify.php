<?php
/**
 * 第三方支付回调处理
 *
 * @createTime: 2016-07-25 15:57
 * @description: 支付通知回调
 */

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/../testNotify.php';

use Payment\Common\PayException;
use Payment\Client\Notify;
use Payment\Config;
use Payment\Utils\ArrayUtil;

date_default_timezone_set('Asia/Shanghai');
$configStr  = '{"pay_var":"201","pay_type":"010","mch_id":"879104816000039","merchant_no":"879104816000039","terminal_id":"11558708","sub_appid":"wx2eaddb8e674394aa","app_id":"wx2eaddb8e674394aa","limit_pay":[""],"access_token":"3846bb6b7909488595c58e19a2211131","base_url":"https://pay.lcsw.cn/lcsw","notify_url":"http://api.test.ledianyun.com/payments/notify","use_sandbox":false,"return_raw":false}';
$config = json_decode($configStr,true);
$callback = new TestNotify();

try {
    if (!empty($config)){
        $retDataStr = "{\"attach\":\"a3f7a2b605e84762868ecf92f97146bf\",\"channel_trade_no\":\"4200000277201903277386709856\",\"end_time\":\"20190327172739\",\"key_sign\":\"572c4d043a58f605202e4d2f0e4e66d8\",\"merchant_name\":\"袁梦对接专用(勿动)\",\"merchant_no\":\"858100307000002\",\"out_trade_no\":\"300559730021619032717272800016\",\"pay_type\":\"010\",\"receipt_fee\":\"1\",\"result_code\":\"01\",\"return_code\":\"01\",\"return_msg\":\"支付成功\",\"terminal_id\":\"30055973\",\"terminal_time\":\"20190327172726\",\"terminal_trace\":\"20190327172726469910101555098120\",\"total_fee\":\"1\",\"user_id\":\"oO4lo5KBwCtX5CPayZV2WedgMjRc\"}";
        $retData = json_decode($retDataStr,true);
        $notify = new \Payment\Notify\SwNotify($config);
        $ret =  $notify->checkNotifyData($retData);
        die($ret);
    }
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

var_dump($ret);
exit;
