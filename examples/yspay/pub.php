<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__.'/../../autoload.php';
//require_once __DIR__.'/common.php';


use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$config = [
	'channelid'     => 'D01X00000800856',
	'key'           => 'lldilrd58lr0o1204l3a32ji5ladavf5',
	'merid' => '531000009218530',
	'termid'     => 'W0810460',
];
$payData = [
    'notifyurl'    => 'https://www.test.com/test/test.t',
    'tradetrace' => '2019062017085196425851101912',
    'openid'    =>'oDqg_5Za0tc23LhTkUR6PfFZ8O2g',
	'tradeamt'=>1
];


try{
	$ret = Charge::run(Config::YS_CHANNEL_PUB, $config, $payData);
}catch(PayException $e){
	exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
