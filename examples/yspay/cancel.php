<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__.'/../../autoload.php';
//require_once __DIR__.'/common.php';


use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$config = [
	'channelid'     => 'D01X00000800856',
	'key'           => 'lldilrd58lr0o1204l3a32ji5ladavf5',
	'merid' => '531000008882415',
	'termid'     => 'W0725858',
];

// {"sign":"5A21EDDAA1092578740FF5CFE4B12F27","wxopenid":"ohWpr5DmiDNk0tOh7eJ2NKEaOfjQ","wxtimeend":"20210814114050","tradetrace":"2021081411404288702697100565","clearamt":"1680","wxtransactionid":"4200001236202108145338315235","acctype":"U","payamt":"1680","wtorderid":"11620210814114046400091"}

$payData = [
    'tradetrace' => '11620210814155733791279',
    'oritradetrace' => '2021081415572994384657101545',
];


try{
	$ret = \Payment\Client\Cancel::run(Config::YS_CHARGE, $config, $payData);
}catch(PayException $e){
	exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
