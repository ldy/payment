<?php


require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$miConfig = require_once 'MiConfig.php';

$orderNo = 12134567980211;//time() . rand(1000, 9999);
// 订单信息
$payData = [
    'body'    => '商品名称',
    'out_trade_sn'    => $orderNo, //自定义订单号
    'sub_appid' => 'wx03b4aeb839cc8d85',//小程序appid
    'sub_openid'=>'oUlAM0Wp1K1rJEGJ0t__Ls5-qFKE',
//    'spbill_create_ip'=>'',
   // 'timeout_express' => time() + 600,// 表示必须 600s 内付款
    'attach'    => '备注信息',
    // 如果是服务商，请提供以下参数
//    'sub_store_id' => 6,//微信分配的子商户公众账号ID
    'limit_pay' => 'no_credit',
    'total_amount' =>1
];

try {
    $ret = Charge::run(Config::MI_CHANNEL_LITE, $miConfig, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo '--result---'.json_encode($ret, JSON_UNESCAPED_UNICODE);