<?php
/**
 * 退款处理  金额必须是 3.01
 * Created by PhpStorm.
 * 
 * Date: 2017/4/30
 * Time: 下午3:51
 */


require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Refund;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');
$wxConfig = require_once  'MiConfig.php';


$data = [
    'out_refund_sn' => '1231321',
    'out_trade_sn' => '1213456798',
    'meepay_trade_no' => 'LD0118103116581749379',
    'refund_amount'=>1

];

try {
    $ret = Refund::run(Config::MI_REFUND, $wxConfig, $data);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);