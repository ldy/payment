<?php


date_default_timezone_set('Asia/Shanghai');
//die(date('Y-m-d H:i:s', '1541417375'));


require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;


$miConfig = require_once 'MiConfig.php';

$orderNo = 12345678;//time() . rand(1000, 9999);
// 订单信息
$data = [
    'out_refund_sn' => '1231321',
    'out_trade_sn' => '1213456798',
    'meepay_trade_no' => 'LD0118103116581749379',
    'refund_amount'=>1
];
try {
    $ret = \Payment\Client\Cancel::run(Config::MI_CHANNEL_LITE, $miConfig, $data);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);