<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__.'/../../autoload.php';
//require_once __DIR__.'/common.php';

use Payment\Client\Cancel;
use Payment\Client\Close;
use Payment\Client\Query;
use Payment\Client\Refund;
use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$config = [
	'ltf_appid'     => 'EW_N3213842400',
	'key'           => '730ed24645b1a54e82a3d2bcff63db37',
	'merchant_code' => 'EW_N5247492162',
//	'notify_url'    => 'https://www.test.com/test/test.t',
//	'order_type'    => 'WXPAY',
//	'trade_type'    => 'MINIAPP',
//	'sub_appid'     => 'wxd678efh567hg6787',
];
$payData = [
	'out_trade_no' => '2019062017085196425851101912',
	'refund_no' => '2019062017085196425851101912',
	'total_fee'=>0.9
];

//2019062017085196425851101985

try{
	$ret = Charge::run(Config::LTF_CHANNEL_LITE, $config, $payData);
}catch(PayException $e){
	exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
