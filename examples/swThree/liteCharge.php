<?php

require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

//$configStr  = "{\"pay_var\":\"201\",\"pay_type\":\"010\",\"service_id\":\"015\",\"mch_id\":\"858100307000002\",\"merchant_no\":\"858100307000002\",\"terminal_id\":\"30055973\",\"sub_appid\":\"wx24863377e74518f8\",\"app_id\":\"wx24863377e74518f8\",\"limit_pay\":[\"\"],\"access_token\":\"bae3420022594dd9a14703f890765f9e\",\"notify_url\":\"http://www.ledian.com/payments/notify\",\"use_sandbox\":false,\"return_raw\":false}";
$configStr  = '{"pay_var":"201","pay_type":"010","mch_id":"879104816000039","merchant_no":"879104816000039","terminal_id":"11558708","sub_appid":"wx2eaddb8e674394aa","app_id":"wx2eaddb8e674394aa","limit_pay":[""],"access_token":"3846bb6b7909488595c58e19a2211131","base_url":"https://pay.lcsw.cn/lcsw","notify_url":"http://api.test.ledianyun.com/payments/notify","use_sandbox":false,"return_raw":false}';
$config = json_decode($configStr,true);

// 订单信息
//$payDataStr = "{\"terminal_trace\":\"20190327151722366145505357545320\",\"total_fee\":1,\"order_body\":\"支付测试\",\"open_id\":\"oUlAM0Wp1K1rJEGJ0t__Ls5-qFKE\",\"attach\":\"10054c238f670a2e9649a8b100f20562\"}";
$payDataStr = '{"service_id":"015","terminal_trace":"20210610120224954476481015748920","total_fee":2,"order_body":"【乐店云】微信支付","open_id":"oDqg_5WEH2ROdZhRN7g3ERebi4e4","attach":"c7264439ab86385bc16a73b85ce3d134"}';
$payData = json_decode($payDataStr,true);

$config['base_url'] = 'https://pay.lcsw.cn/lcsw';

try {
    $ret = Charge::run(Config::SW_T_CHANNEL_LITE, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);

/**
 *{"jspackage":{"timeStamp":"1624435123","nonceStr":"c15e4cbeb7eb4bc0b3e0f854c78aa567","package":"prepay_id=wx231558436993717e6c28d89771ac690000","signType":"RSA","paySign":"HNLOvvAnQ8r4Y1Ciu5wt8fxckykaLMBheT7vqFgZGKcR4aPPJwpJ5OVMs9fhoLIADkPPFaVLIxd7eFxVzQWlW2axzZQrKJwA\/52KtRxlEfwAY2Uko\/lmNecfcdJmwFMtmb+qw+8aQrAo7Ck4p7fnqZnErtwV3R0i8c+g2dnbum37YP1XbvFpXwPCCDoSpcOThDHf4ss\/TxKMJUh3oK36HAXVfJTB\/Vgs3Va\/QaUpWNbhXCoQUbvlAiO26mFXotTxd2Gph7hzkcwJcmyEwvaeenAvmrq5XGihnFVZ0t7Wvec0rxpmfHA2FPcQMWBqfzuDsW4pDK1A9PF5OLmhs3j5mA=="},"other":{"pay_type":"010","merchant_name":"西安乐店蜂抢信息科技有限公司1","merchan16000039","terminal_id":"11558708","terminal_trace":"20210610120224954476481015748920","terminal_time":"20210623155842","total_fee":"2","out_trade_no":"115587084621621062315584324331"}}
 */