<?php

require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Refund;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$configStr  = '{"pay_type":"010","mch_id":"879104816000039","merchant_no":"879104816000039","terminal_id":"11558708","sub_appid":"wx2eaddb8e674394aa","app_id":"wx2eaddb8e674394aa","limit_pay":[""],"access_token":"3846bb6b7909488595c58e19a2211131","base_url":"https://pay.lcsw.cn/lcsw","notify_url":"http://api.test.ledianyun.com/payments/notify","use_sandbox":false,"return_raw":false}';
$config = json_decode($configStr,true);

// 订单信息
$payDataStr = '{"pay_type":"010","terminal_trace":"20210610120232330798565348995620","out_trade_no":"115587081421321062314555821032","refund_fee":"2"}';
$payData = json_decode($payDataStr,true);

$config['base_url'] = 'https://pay.lcsw.cn/lcsw';

try {
    $ret = Refund::run(Config::SW_T_REFUND, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}
echo json_encode($ret, JSON_UNESCAPED_UNICODE);

/**
 * 支付平台返回错误提示:02 : 原交易状态为非成功
 */