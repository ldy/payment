<?php

require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Query;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

//$configStr  = "{\"pay_var\":\"201\",\"pay_type\":\"010\",\"service_id\":\"015\",\"mch_id\":\"858100307000002\",\"merchant_no\":\"858100307000002\",\"terminal_id\":\"30055973\",\"sub_appid\":\"wx24863377e74518f8\",\"app_id\":\"wx24863377e74518f8\",\"limit_pay\":[\"\"],\"access_token\":\"bae3420022594dd9a14703f890765f9e\",\"notify_url\":\"http://www.ledian.com/payments/notify\",\"use_sandbox\":false,\"return_raw\":false}";
$configStr  = '{"pay_type":"010","mch_id":"879104816000039","merchant_no":"879104816000039","terminal_id":"11558708","sub_appid":"wx2eaddb8e674394aa","app_id":"wx2eaddb8e674394aa","limit_pay":[""],"access_token":"3846bb6b7909488595c58e19a2211131","base_url":"https://pay.lcsw.cn/lcsw","notify_url":"http://api.test.ledianyun.com/payments/notify","use_sandbox":false,"return_raw":false}';
$config = json_decode($configStr,true);

// 订单信息
//$payDataStr = "{\"terminal_trace\":\"20190327151722366145505357545320\",\"total_fee\":1,\"order_body\":\"支付测试\",\"open_id\":\"oUlAM0Wp1K1rJEGJ0t__Ls5-qFKE\",\"attach\":\"10054c238f670a2e9649a8b100f20562\"}";
$payDataStr = '{"pay_type":"010","terminal_trace":"20210610120232330798565348995620","out_trade_no":"115587081421321062314555821032"}';
$payData = json_decode($payDataStr,true);

$config['base_url'] = 'https://pay.lcsw.cn/lcsw';

try {
    $ret = Query::run(Config::SW_T_QUERY, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}
echo json_encode($ret, JSON_UNESCAPED_UNICODE);

/**
 * {"attach":"e0c3b706044a99f143be92ee11071370","channel_order_no":"115587081421321062314555821032","channel_trade_no":"","end_time":"20210623155646","merchant_name":"西安乐店蜂抢信息科技有限公司1","mercha816000039","out_trade_no":"115587081421321062314555821032","pay_time":"20210623145558","pay_trace":"20210610120232330798565348995620","pay_type":"010","result_code":"03","return_code":"01","return_msg":"未支付","terminal_id":"11558708","terminal_time":"20210623155645","terminal_trace":"20210610120232330798565348995620","total_fee":"2","trade_state":"NOTPAY","typeMsg":"","user_id":""}%
 */