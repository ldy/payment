<?php
/**
 * 第三方支付回调处理
 *
 * @createTime: 2016-07-25 15:57
 * @description: 支付通知回调
 */

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/../testNotify.php';

use Payment\Common\PayException;
use Payment\Client\Notify;
use Payment\Config;
use Payment\Utils\ArrayUtil;

date_default_timezone_set('Asia/Shanghai');
$configStr = "{\"pay_var\":\"100\",\"pay_type\":\"010\",\"service_id\":\"015\",\"mch_id\":\"858100307000002\",\"merchant_no\":\"858100307000002\",\"terminal_id\":\"30055973\",\"sub_appid\":\"wx24863377e74518f8\",\"app_id\":\"wx24863377e74518f8\",\"limit_pay\":[\"\"],\"access_token\":\"bae3420022594dd9a14703f890765f9e\",\"notify_url\":\"http://www.ledian.com/payments/notify\",\"use_sandbox\":false,\"return_raw\":false}";
$config = json_decode($configStr,true);
$callback = new TestNotify();

try {
    if (!empty($config)){
//        Notify::run(Config::SW_CHARGE,$config,$callback);

       // $retData = Notify::getNotifyData($type, $config);// 获取第三方的原始数据，未进行签名检查

        $retDataStr = "{\"attach\":\"a3f7a2b605e84762868ecf92f97146bf\",\"channel_trade_no\":\"4200000277201903277386709856\",\"end_time\":\"20190327172739\",\"key_sign\":\"572c4d043a58f605202e4d2f0e4e66d8\",\"merchant_name\":\"袁梦对接专用(勿动)\",\"merchant_no\":\"858100307000002\",\"out_trade_no\":\"300559730021619032717272800016\",\"pay_type\":\"010\",\"receipt_fee\":\"1\",\"result_code\":\"01\",\"return_code\":\"01\",\"return_msg\":\"支付成功\",\"terminal_id\":\"30055973\",\"terminal_time\":\"20190327172726\",\"terminal_trace\":\"20190327172726469910101555098120\",\"total_fee\":\"1\",\"user_id\":\"oO4lo5KBwCtX5CPayZV2WedgMjRc\"}";
        $retData = json_decode($retDataStr,true);
        $notify = new \Payment\Notify\SwNotify($config);
        $ret =  $notify->checkNotifyData($retData);
        die($ret);

//        $basePath = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'CacertFile' . DIRECTORY_SEPARATOR;
//        $publicKeyPath = "{$basePath}mipay".DIRECTORY_SEPARATOR."meepay_public_key.pem";

//        $publicKeyPath = "/project/ldy/payment/src/CacertFile/mipay/meepay_public_key.pem";
//        $retData = "{\"result_code\":\"SUCCESS\",\"out_trade_sn\":\"SN20181031020706\",\"meepay_trade_no\":\"MF0118103102070684504\",\"transaction_id\":\"4200000203201810316768166604\",\"sub_merchant_id\":\"10002\",\"sub_appid\":\"wx03b4aeb839cc8d85\",\"sub_openid\":\"oUlAM0fD5RsWs9CvOCCMU_nYEDSY\",\"total_amount\":\"1\",\"trade_type\":\"NATIVE\",\"nonce_str\":\"af7rhpf50cjqjadyzthu82hyl7rzdkgk\",\"fee_type\":\"CNY\",\"cash_fee\":\"1\",\"attach\":\"1233\",\"sign_type\":\"RSA2\",\"sign\":\"d0AsaPytw9by+gfD9DLBHp9s1U+aQt9qVd1DYjPP4xvp+7umDU/bLsuxER+0XPF5M0vwdcks/Tge+KCWX3wWdMIOnltVKfDRFfsf7HOznwr4sE5zUxb0nkfCayLMZ/m/kcgqy9QryAl+SY/+MmLlmnxEI6eyjtnX0rf8qZyZDGI=\"}";
//
//        $ret = ArrayUtil::SignVerify(json_decode($retData,true),$publicKeyPath);
//        echo $ret;
    }
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

var_dump($ret);
exit;
