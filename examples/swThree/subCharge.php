<?php

require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

//$configStr  = "{\"pay_var\":\"201\",\"pay_type\":\"010\",\"service_id\":\"015\",\"mch_id\":\"858100307000002\",\"merchant_no\":\"858100307000002\",\"terminal_id\":\"30055973\",\"sub_appid\":\"wx24863377e74518f8\",\"app_id\":\"wx24863377e74518f8\",\"limit_pay\":[\"\"],\"access_token\":\"bae3420022594dd9a14703f890765f9e\",\"notify_url\":\"http://www.ledian.com/payments/notify\",\"use_sandbox\":false,\"return_raw\":false}";
$configStr  = '{"pay_var":"201","pay_type":"010","mch_id":"879104816000039","merchant_no":"879104816000039","terminal_id":"11558708","sub_appid":"wx2eaddb8e674394aa","app_id":"wx2eaddb8e674394aa","limit_pay":[""],"access_token":"3846bb6b7909488595c58e19a2211131","base_url":"https://pay.lcsw.cn/lcsw","notify_url":"http://api.test.ledianyun.com/payments/notify","use_sandbox":false,"return_raw":false}';
$config = json_decode($configStr,true);

// 订单信息
//$payDataStr = "{\"terminal_trace\":\"20190327151722366145505357545320\",\"total_fee\":1,\"order_body\":\"支付测试\",\"open_id\":\"oUlAM0Wp1K1rJEGJ0t__Ls5-qFKE\",\"attach\":\"10054c238f670a2e9649a8b100f20562\"}";
$payDataStr = '{"pay_ver":"201","pay_type":"010","service_id":"012","terminal_trace":"20210610120232330798565348995620","total_fee":2,"order_body":"【乐店云】微信支付","open_id":"oDqg_5WEH2ROdZhRN7g3ERebi4e4","openid":"oDqg_5WEH2ROdZhRN7g3ERebi4e4","attach":"e0c3b706044a99f143be92ee11071370"}';
$payData = json_decode($payDataStr,true);

$config['base_url'] = 'https://pay.lcsw.cn/lcsw';

try {
    $ret = Charge::run(Config::SW_T_CHANNEL_PUB, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);

/**
 * {"jspackage":{"appId":"wx2eaddb8e674394aa","timeStamp":"1624435057","nonceStr":"a410b9b125304c2992c8be9cdb752f17","package":"prepay_id=wx23155737206767b91d90be7ec033180000","signType":"RSA","paySign":"3C1FR8cXtqBzbp8dBQSjdfnUGKwv9TqyRbf7RadvNqFCd7I3ayRkzDSVPy7\/Nd2g1FbQIgtyeN7RH1y+RA7If\/jqUP5NVTJJnxmUpqi0a4zFq\/5UYNmGJ0xTXHmquVBvnFaddkyQ\/MmsxMrbTlTDaHb6CQwr6o9qEaTutzMOd97\/MAj1T5h86Vyu4fu5BHcaMWo4mUyBWBXl7bB3fqrlNzKLxijEZzov\/MhTZQ4MpTC7OBOGU7t3yJ+iSw+W87WHiHhsssFO\/jZZ\/PJ0pKiKrVF5hj\/te9whqXpjCpoWoc9hDKd92NrhO82H3acpfjge1FyimbYjPTQXzgkcoZNB7Q=="},"other":{"out_trade_no":"115587085921321062315573623420"}}
 */