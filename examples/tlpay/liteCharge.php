<?php
/**
 * 公众号支付
 * Created by PhpStorm.
 * 
 * Date: 2017/4/30
 * Time: 下午3:33
 */


require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$tlConfig = require_once __DIR__ . '/../TLConfig.php';

$orderNo = 12345678;//time() . rand(1000, 9999);
// 订单信息
$payData = [
    'body'    => '商品名称',
    'reqsn'    => $orderNo,
   // 'timeout_express' => time() + 600,// 表示必须 600s 内付款
    'trxamt'    => '1',// 微信沙箱模式，需要金额固定为3.01
    'randomstr' => '1450432107647',//md5(uniqid(md5(microtime(true)), true)),
    'remark'    => '备注信息',
    // 如果是服务商，请提供以下参数
    'validtime' => 6,//微信分配的子商户公众账号ID
    'acct' => 'odNM24283i2wwQPzqy8Fxyax2y8U',// 微信支付分配的子商户号
    'limit_pay' => 'no_credit'
];

try {
    $ret = Charge::run(Config::TL_CHANNEL_LITE, $tlConfig, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);