<?php
/**
 * 查询支付的订单
 * Created by PhpStorm.
 * 
 * Date: 2017/4/30
 * Time: 下午3:43
 */

require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Query;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');

$wxConfig = require_once __DIR__ . '/../TLconfig.php';

$data = [
    'randomstr' => '123413',
    'reqsn' => '123456',//商户的交易订单号
    'trxid'  => '123456789123',//支付的收银宝平台流水
];

try {
    $ret = Query::run(Config::TL_QUERY, $wxConfig, $data);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);