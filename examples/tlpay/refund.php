<?php
/**
 * 退款处理  金额必须是 3.01
 * Created by PhpStorm.
 * 
 * Date: 2017/4/30
 * Time: 下午3:51
 */


require_once __DIR__ . '/../../autoload.php';

use Payment\Common\PayException;
use Payment\Client\Refund;
use Payment\Config;

date_default_timezone_set('Asia/Shanghai');
$wxConfig = require_once __DIR__ . '/../TLConfig.php';


$data = [
    'randomstr' => 'asdasda',
    'reqsn' => '14935385689468',
    'oldtrxid'=>'111817080000227489',
    'trxamt' => '100',
    'remark' => 'dasd',

];

try {
    $ret = Refund::run(Config::TL_REFUND, $wxConfig, $data);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);