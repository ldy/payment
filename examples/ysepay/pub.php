<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__.'/../../autoload.php';
//require_once __DIR__.'/common.php';


use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$config = [
	'partner_id'     => '826791873720039',
	'seller_id'           => 'QRY210802574454',
	'seller_name' => '铜川市耀州区青丝坊发型设计室',
	'business_code'     => '3010002',
];
$payData = [
//    'method'=>'ysepay.online.weixin.pay',
    'notify_url'    => 'http://www.test.com/test/test.t',
    'out_trade_no'  =>'2019062017085196425851101912',
    'shopdate'  =>date('Ymd'),
    'timestamp'  =>date('Y-m-d H:i:s',time()),
    'subject'  =>'乐店云支付测试',
    'total_amount'  => 0.01,
    'timeout_express'   => '9h',
    'sub_openid' => 'ohWpr5HtNcnMC94YBd6i9pqNU07Y',
    'appid' => 'wxe48ea18f1edba2c7',
    'extra_common_param' => '123123123213',   //公用回传参数
];

try{
	$ret = Charge::run(Config::YSE_CHANNEL_PUB, $config, $payData);
}catch(PayException $e){
	exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
