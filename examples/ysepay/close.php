<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__.'/../../autoload.php';
//require_once __DIR__.'/common.php';


use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$config = [
	'channelid'     => '860010010210008',
	'key'           => 'fkdkldvkij5dfd24dl24dk2dddflaaf2',
	'merid' => '880210010215460',
	'termid'     => '10006468',
];
$payData = [
//    'tradetrace' => '11620210304155042147189',
    'tradetrace' => '2019062017085196425851101914',
//    'oritradetrace' => '2019062017085196425851101914',
];


try{
	$ret = \Payment\Client\Close::run(Config::YS_CHARGE, $config, $payData);
}catch(PayException $e){
	exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
