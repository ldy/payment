<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__.'/../../autoload.php';
//require_once __DIR__.'/common.php';


use Payment\Common\PayException;
use Payment\Client\Query;
use Payment\Config;

$config = [
    'partner_id'     => '826791873720039',
    'seller_id'           => '826791873720039',
    'seller_name' => '西安汇集信电子技术有限公司',
    'business_code'     => '3010002',
];
$payData = [
//    'method'=>'ysepay.online.weixin.pay',
    'out_trade_no'  =>'2019062017085196425851101912',
    'shopdate'  =>date('Ymd'),
    'timestamp'  =>date('Y-m-d H:i:s',time()),
];

try{
	$ret = Query::run(Config::YSE_QUERY, $config, $payData);
}catch(PayException $e){
	exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
