<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__.'/../../autoload.php';
//require_once __DIR__.'/common.php';


use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$config = [
	'channelid'     => '860010010210008',
	'key'           => 'fkdkldvkij5dfd24dl24dk2dddflaaf2',
	'merid' => '880210010215460',
	'termid'     => '10006468'
];
$payData = [
    'notifyurl'    => 'https://www.test.com/test/test.t',
    'tradetrace' => '2019062017085196425851101915',
//    'openid'    =>'oDqg_5Za0tc23LhTkUR6PfFZ8O2g',
    'openid'    =>'ohWpr5L1oy5SK4LMyoazVG4BJVGY',
	'tradeamt'=>1
];
//            $payData = [
//                "out_trade_no" => "2019062017085196425851101915",
//                "total_fee" => 1,
//                "body" => 'hello 你好呀',
//                "extra_param"   => '',
//                'open_id' => "oDqg_5Za0tc23LhTkUR6PfFZ8O2g"
//            ];


try{
	$ret = Charge::run(Config::YS_CHANNEL_LITE, $config, $payData);
}catch(PayException $e){
	exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
