<?php
/**
 * 第三方支付回调处理
 *
 * @createTime: 2016-07-25 15:57
 * @description: 支付通知回调
 */

require_once __DIR__ . '/../autoload.php';
require_once __DIR__ . '/testNotify.php';

use Payment\Common\PayException;
use Payment\Client\Notify;
use Payment\Utils\ArrayUtil;

date_default_timezone_set('Asia/Shanghai');

$aliConfig = require_once __DIR__ . '/aliconfig.php';
$wxConfig = require_once __DIR__ . '/wxconfig.php';
$cmbConfig = require_once __DIR__ . '/cmbconfig.php';
$tlConfig = require_once __DIR__ . '/TLConfig.php';
$miConfig = require_once __DIR__ . '/mipay/MiConfig.php';
$callback = new TestNotify();

$type = 'mi_charge';// xx_charge

if (stripos($type, 'ali') !== false) {
    $config = $aliConfig;
} elseif (stripos($type, 'wx') !== false) {
    $config = $wxConfig;
} elseif (stripos($type, 'cmb') !== false) {
    $config = $cmbConfig;
}elseif (stripos($type, 'tl') !== false){
    $config = $tlConfig;
}elseif (stripos($type, 'mi') !== false){
    $config = $miConfig;
}


try {
    if (!empty($config)){
       // $retData = Notify::getNotifyData($type, $config);// 获取第三方的原始数据，未进行签名检查

//        $retData = "{\"acct\":\"odNM242ffm2FaJlLIpkcwM1O3YsE\",\"appid\":\"00020281\",\"chnltrxid\":\"4200000135201805042268978922\",\"cusid\":\"55079104816PJXP\",\"cusorderid\":\"20180504091414288930545254561086\",\"outtrxid\":\"20180504091414288930545254561086\",\"paytime\":\"20180504091423\",\"sign\":\"AD9FB6CD0E1455574C690F8AFE26F2A1\",\"termauthno\":\"CFT\",\"termrefnum\":\"4200000135201805042268978922\",\"termtraceno\":\"0\",\"trxamt\":\"1\",\"trxcode\":\"VSP501\",\"trxdate\":\"20180504\",\"trxid\":\"111817080000227489\",\"trxreserved\":\"通联支付测试备注信息\",\"trxstatus\":\"0000\"}";
//
//        $ret =  ArrayUtil::ValidSign(json_decode($retData,true), $tlConfig['md5_key']);

//        $basePath = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'CacertFile' . DIRECTORY_SEPARATOR;
//        $publicKeyPath = "{$basePath}mipay".DIRECTORY_SEPARATOR."meepay_public_key.pem";

        $publicKeyPath = "/project/ldy/payment/src/CacertFile/mipay/meepay_public_key.pem";
        $retData = "{\"result_code\":\"SUCCESS\",\"out_trade_sn\":\"SN20181031020706\",\"meepay_trade_no\":\"MF0118103102070684504\",\"transaction_id\":\"4200000203201810316768166604\",\"sub_merchant_id\":\"10002\",\"sub_appid\":\"wx03b4aeb839cc8d85\",\"sub_openid\":\"oUlAM0fD5RsWs9CvOCCMU_nYEDSY\",\"total_amount\":\"1\",\"trade_type\":\"NATIVE\",\"nonce_str\":\"af7rhpf50cjqjadyzthu82hyl7rzdkgk\",\"fee_type\":\"CNY\",\"cash_fee\":\"1\",\"attach\":\"1233\",\"sign_type\":\"RSA2\",\"sign\":\"d0AsaPytw9by+gfD9DLBHp9s1U+aQt9qVd1DYjPP4xvp+7umDU/bLsuxER+0XPF5M0vwdcks/Tge+KCWX3wWdMIOnltVKfDRFfsf7HOznwr4sE5zUxb0nkfCayLMZ/m/kcgqy9QryAl+SY/+MmLlmnxEI6eyjtnX0rf8qZyZDGI=\"}";

        $ret = ArrayUtil::SignVerify(json_decode($retData,true),$publicKeyPath);
        echo $ret;
    }
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

var_dump($ret);
exit;
