<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/common.php';

use Payment\Client\Cancel;
use Payment\Common\PayException;
use Payment\Config;

$payData = [
    'order_type' => 'WECHAT', //ALIPAY
    'order_no' => trade_no(null, true),
    'refund_no' => trade_no(null, true)
];

try {
    $ret = Cancel::run(Config::FU_CHARGE, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);