<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/common.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$payData = [
    'order_type' => 'WECHAT', //ALIPAY
    'order_no' => trade_no(null, true),
    'amount' => 1,
    'subject' => '支付测试',
    'attach' => '10054c238f670a2e9649a8b100f20562'
];

$config['notify_url'] = '';

try {
    $ret = Charge::run(Config::FU_CHANNEL_SCAN, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);