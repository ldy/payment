<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/common.php';
require_once __DIR__ . '/../testNotify.php';

use Payment\Client\Notify;
use Payment\Common\PayException;

$channel = 'fu_charge';
$callback = new TestNotify();

$_POST['req'] = '%3C%3Fxml+version%3D%221.0%22+encoding%3D%22GBK%22+standalone%3D%22yes%22%3F%3E%3Cxml%3E%3Ccurr_type%3ECNY%3C%2Fcurr_type%3E%3Cins_cd%3E08A9999999%3C%2Fins_cd%3E%3Cmchnt_cd%3E0002900F0370542%3C%2Fmchnt_cd%3E%3Cmchnt_order_no%3E2019062017530653068450564957%3C%2Fmchnt_order_no%3E%3Corder_amt%3E1%3C%2Forder_amt%3E%3Corder_type%3EWECHAT%3C%2Forder_type%3E%3Crandom_str%3ELBY9GBWNNFV2RSTJX9PK549HGZK9J5U7%3C%2Frandom_str%3E%3Creserved_addn_inf%3E10054c238f670a2e9649a8b100f20562%3C%2Freserved_addn_inf%3E%3Creserved_bank_type%3ECFT%3C%2Freserved_bank_type%3E%3Creserved_buyer_logon_id%3E%3C%2Freserved_buyer_logon_id%3E%3Creserved_channel_order_id%3E2019062017530653068450564957%3C%2Freserved_channel_order_id%3E%3Creserved_coupon_fee%3E0%3C%2Freserved_coupon_fee%3E%3Creserved_fund_bill_list%3E%3C%2Freserved_fund_bill_list%3E%3Creserved_fy_settle_dt%3E20190620%3C%2Freserved_fy_settle_dt%3E%3Creserved_fy_trace_no%3E060001686318%3C%2Freserved_fy_trace_no%3E%3Creserved_is_credit%3E0%3C%2Freserved_is_credit%3E%3Creserved_settlement_amt%3E1%3C%2Freserved_settlement_amt%3E%3Cresult_code%3E000000%3C%2Fresult_code%3E%3Cresult_msg%3ESUCCESS%3C%2Fresult_msg%3E%3Csettle_order_amt%3E1%3C%2Fsettle_order_amt%3E%3Csign%3E2lvzV829wI5miCS%2FYPkdhcPMmwwd5Z3en%2BNeXfwgEqvqXNXbUeMipHM9JeNZX1l3zuCnSgEyyQWcijeyzPSOC5UiOoxSGOwx1tsQPF9rDUFu25ApVz%2B7hT6rn5R3bc9zK0phgYjdkkKF64%2BCrWsI4gEnJHIyo568FgDqyVziXhA%3D%3C%2Fsign%3E%3Cterm_id%3E%3C%2Fterm_id%3E%3Ctransaction_id%3E4200000334201906209673443163%3C%2Ftransaction_id%3E%3Ctxn_fin_ts%3E20190620175355%3C%2Ftxn_fin_ts%3E%3Cuser_id%3EoUpF8uBpoepFxyJcDAonTatbs8Zc%3C%2Fuser_id%3E%3C%2Fxml%3E';
try {
    $ret = Notify::run($channel, $config, $callback);
//    var_dump($ret);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}