<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/common.php';

use Payment\Common\PayException;
use Payment\Client\Charge;
use Payment\Config;

$payData = [
    'order_type' => 'WECHAT', //ALIPAY、UNIONPAY、BESTPAY
    'order_no' => trade_no(null, true),
    'amount' => 1,
    'auth_code' => '',
    'subject' => '支付测试',
    'attach' => '10054c238f670a2e9649a8b100f20562'
];

var_dump($payData);
//2019062017085196425851101985
try {
    $ret = Charge::run(Config::FU_CHANNEL_BAR, $config, $payData);
} catch (PayException $e) {
    exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);