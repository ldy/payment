<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/common.php';

use Payment\Client\Query;
use Payment\Common\PayException;
use Payment\Config;

$payData = [
    'order_no' => trade_no(null, true), //商户订单号
    'refund_no' => trade_no(null, true), //商户退款单号
];
try {
    $ret = Query::run(Config::FU_REFUND, $config, $payData);
} catch (PayException $e) {
    exit($e->errorMessage());
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);