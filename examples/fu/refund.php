<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/common.php';

use Payment\Client\Refund;
use Payment\Common\PayException;
use Payment\Config;

$payData = [
    'order_type' => 'WECHAT', //ALIPAY、UNIONPAY、BESTPAY
    'order_no' => trade_no(null, true),
    'refund_no' => trade_no(null, true),
    'total_fee' => 1, //订单总金额
    'refund_fee' => 1, //退款金额
    'trade_date' => '', //原交易日期（Ymd，如：20190620）
];

//var_dump($payData);

try {
    $ret = Refund::run(Config::FU_REFUND, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);