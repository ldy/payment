<?php

date_default_timezone_set('Asia/Shanghai');

require_once __DIR__ . '/../../autoload.php';
require_once __DIR__ . '/common.php';

use Payment\Client\Close;
use Payment\Common\PayException;
use Payment\Config;

$payData = [
    'order_type' => 'WECHAT', //WXAPP、WXH5、ALIPAY
    'order_no' => trade_no(null, true)
];

try {
    $ret = Close::run(Config::FU_CHARGE, $config, $payData);
} catch (PayException $e) {
    echo $e->errorMessage();
    exit;
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);