<?php
/**
 *
 * @createTime: 2016-07-27 15:43
 * @description:
 */

namespace Payment\Trans;

use Payment\Common\Hsq\Data\TransferData;
use Payment\Common\Hsq\HsqTransBaseStrategy;
use Payment\Common\HsqTransConfig;
use Payment\Config;
use Payment\Utils\Curl;

/**
 * 微信企业付款接口
 * Class WxTransfer
 * @package Payment\Trans
 *
 */

class HsqTransfer extends HsqTransBaseStrategy
{
    public function getBuildDataClass()
    {
        return TransferData::class;
    }

    /*
     * 返回转款的url
     */
    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?: HsqTransConfig::PAY_URL);
    }

    /**
     * 转款的返回数据
     * @param array $ret
     * @return mixed
     */
    protected function retData(array $ret)
    {
        $head = $ret['trans_content']['trans_head'];
        $data = $ret['trans_content']['trans_reqDatas'][0]['trans_reqData'] ?? '';
        if($head['return_code'] != '0000')
        {
            $retData = [
                'is_success'    => 'F',
                'response'  => [
                    'error' => $head['return_msg'],
                    'channel' => Config::HSQ_TRANSFER,
                ],
            ];
        }
        else
        {
            $retData = [
                'is_success'    => 'T',
                'response'  => [
                  'datas' => $data,
                  'channel' => Config::HSQ_TRANSFER,
                ],
            ];
        }
        return $retData;
    }


}
