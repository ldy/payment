<?php

/**
 * @author: yeran
 * @createTime: 2018-10-30 11:35
 * @description: 退款统一接口
 */

namespace Payment;

use Payment\Common\BaseStrategy;
use Payment\Common\PayException;
use Payment\Refund\AliRefund;
use Payment\Refund\CmbRefund;
use Payment\Refund\FuRefund;
use Payment\Refund\LTFRefund;
use Payment\Refund\MiRefund;
use Payment\Refund\SwRefund;
use Payment\Refund\SwThreeRefund;
use Payment\Refund\TLRefund;
use Payment\Refund\WNFRefund;
use Payment\Refund\WxRefund;
use Payment\Refund\YSRefund;
use Payment\Refund\YSERefund;
use Payment\Refund\HsqRefund;

class RefundContext
{
    /**
     * 退款的渠道
     * @var BaseStrategy
     */
    protected $refund;


    /**
     * 设置对应的退款渠道
     * @param string $channel 退款渠道
     *  - @see Config
     *
     * @param array $config 配置文件
     * @throws PayException
     *
     */
    public function initRefund($channel, array $config)
    {
        try {
            switch ($channel) {
                case Config::ALI_REFUND:
                    $this->refund = new AliRefund($config);
                    break;
                case Config::WX_REFUND:
                    $this->refund = new WxRefund($config);
                    break;
                case Config::CMB_REFUND:
                    $this->refund = new CmbRefund($config);
                    break;
                case Config::TL_REFUND:
                    $this->refund = new TLRefund($config);
                    break;
                case Config::MI_REFUND:
                    $this->refund = new MiRefund($config);
                    break;
                case Config::SW_REFUND:
                    $this->refund = new SwRefund($config);
                    break;
                case Config::SW_T_REFUND:
                    $this->refund = new SwThreeRefund($config);
                    break;
                case Config::FU_REFUND:
                    $this->refund = new FuRefund($config);
                    break;
				case Config::LTF_REFUND:
					$this->refund= new LTFRefund($config);
					break;
                case Config::YS_REFUND:
                    $this->refund = new YSRefund($config);
                    break;
                case Config::YSE_REFUND:
                    $this->refund = new YSERefund($config);
                    break;
                case Config::WNF_REFUND:
                    $this->refund = new WNFRefund($config);
                    break;
                case Config::HSQ_REFUND:
                    $this->refund = new HsqRefund($config);
                    break;
                default:
                    throw new PayException('当前仅支持：ALI WEIXIN CMB TL MI SW FU WNF HSQ');
            }
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 通过环境类调用支付退款操作
     *
     * @param array $data
     *
     * @return array
     * @throws PayException
     *
     */
    public function refund(array $data)
    {
        if (! $this->refund instanceof BaseStrategy) {
            throw new PayException('请检查初始化是否正确');
        }

        try {
            return $this->refund->handle($data);
        } catch (PayException $e) {
            throw $e;
        }
    }
}
