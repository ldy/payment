<?php

namespace Payment\Cancel;

use Payment\Common\Fu\Data\Cancel\CancelData;
use Payment\Common\Fu\FuBaseStrategy;
use Payment\Common\FuConfig;

/**
 * 撤单，仅支持条码支付
 * Class FuCancel
 * @package Payment\Charge\Fu
 */
class FuCancel extends FuBaseStrategy
{

    public function getBuildDataClass()
    {
        return CancelData::class;
    }

    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? FuConfig::CANCEL_URL);
    }

    /**
     * retData
     * @param array $data
     * @return array|mixed
     */
    protected function retData(array $data)
    {
        $backData = [
            'order_type' => $data['order_type'] ?? '', //订单类型
            'order_no' => $data['mchnt_order_no'] ?? '', //原交易商户订单号
            'refund_no' => $data['cancel_order_no'] ?? '', //商户撤销单号
            'out_trade_no' => $data['transaction_id'] ?? '', //渠道交易流水号
            'out_cancel_no' => $data['cancel_id'] ?? '', //渠道撤销流水号
            'other' => $data
        ];
        return $backData;
    }

}
