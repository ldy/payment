<?php
/**
 * @author: yeran
 * @createTime: 2018-10-30 11:26
 * @description: 支付异步回调
 */

namespace Payment;

use Payment\Notify\AliNotify;
use Payment\Notify\CmbNotify;
use Payment\Notify\FuNotify;
use Payment\Notify\LTFNotify;
use Payment\Notify\MiNotify;
use Payment\Notify\NotifyStrategy;
use Payment\Notify\PayNotifyInterface;
use Payment\Notify\SwNotify;
use Payment\Notify\SwThreeNotify;
use Payment\Notify\TLNotify;
use Payment\Notify\WxNotify;
use Payment\Common\PayException;
use Payment\Notify\YSNotify;
use Payment\Notify\YSENotify;
use Payment\Notify\HsqNotify;
use Payment\Notify\HsqTransferNotify;

class NotifyContext
{
    /**
     * 支付的渠道
     * @var NotifyStrategy
     */
    protected $notify;


    /**
     * 设置对应的通知渠道
     * @param string $channel 通知渠道
     *  - @see Config
     *
     * @param array $config 配置文件
     * @throws PayException
     *
     */
    public function initNotify($channel, array $config)
    {
        try {
            switch ($channel) {
                case Config::ALI_CHARGE:
                    $this->notify = new AliNotify($config);
                    break;
                case Config::WX_CHARGE:
                    $this->notify = new WxNotify($config);
                    break;
                case Config::CMB_CHARGE:
                    $this->notify = new CmbNotify($config);
                    break;
                case Config::TL_CHARGE:
                    $this->notify = new TLNotify($config);
                    break;
                case Config::MI_CHARGE:
                    $this->notify = new MiNotify($config);
                    break;
                case Config::SW_CHARGE:
                    $this->notify = new SwNotify($config);
                    break;
                case Config::SW_T_CHARGE:
                    $this->notify = new SwThreeNotify($config);
                    break;
                case Config::FU_CHARGE:
                    $this->notify = new FuNotify($config);
                    break;
				case Config::LTF_CHARGE:
					$this->notify = new LTFNotify($config);
					break;
                case Config::YS_CHARGE:
                    $this->notify = new YSNotify($config);
                    break;
                case Config::YSE_CHARGE:
                    $this->notify = new YSENotify($config);
                    break;
                case Config::HSQ_CHARGE:
                    $this->notify = new HsqNotify($config);
                    break;
                case Config::HSQ_TRANSFER:
                    $this->notify = new HsqTransferNotify($config);
                    break;

                default:
                    throw new PayException('当前仅支持：ALI_CHARGE WX_CHARGE CMB_CHARGE TL_CHARGE MI_CHARGE SW_CHARGE FU_CHARGE HSQ_CHARGE ');
            }
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 返回异步通知的数据
     * @return array|false
     */
    public function getNotifyData()
    {
        return $this->notify->getNotifyData();
    }

    /**
     * 通过环境类调用支付异步通知
     *
     * @param PayNotifyInterface $notify
     * @return array
     * @throws PayException
     *
     */
    public function notify(PayNotifyInterface $notify)
    {
        if (!$this->notify instanceof NotifyStrategy) {
            throw new PayException('请检查初始化是否正确');
        }

        return $this->notify->handle($notify);
    }
}
