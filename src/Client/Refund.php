<?php

/**
 * @author yeran
 * @time 2018-10-30 11:34
 * @desc 退款
 *
 */

namespace Payment\Client;

use Payment\Common\PayException;
use Payment\Config;
use Payment\RefundContext;

/**
 * 退款操作客户端接口
 * Class Refund
 * @package Payment\Client
 */
class Refund
{
    private static $supportChannel = [
        Config::ALI_REFUND,// 支付宝

        Config::WX_REFUND,// 微信

        Config::CMB_REFUND,// 招行一网通

        Config::APPLE_PAY,// Apple Pay

        Config::TL_REFUND,//通联支付
        Config::MI_REFUND,//米付

        Config::SW_REFUND,//扫呗
        Config::SW_T_REFUND,//扫呗(3.0)

        Config::FU_REFUND, //富友
		Config::LTF_REFUND,

		Config::YS_REFUND,//易生支付

		Config::YSE_REFUND,//银盛支付

        Config::WNF_REFUND, //微诺付

        Config::HSQ_REFUND, //微诺付
    ];

    /**
     * 异步通知类
     * @var RefundContext
     */
    protected static $instance;

    protected static function getInstance($channel, $config)
    {
        if (is_null(self::$instance)) {
            static::$instance = new RefundContext();

            try {
                static::$instance->initRefund($channel, $config);
            } catch (PayException $e) {
                throw $e;
            }
        }

        return static::$instance;
    }

    public static function run($channel, $config, $refundData)
    {
        if (!in_array($channel, self::$supportChannel)) {
            throw new PayException('sdk当前不支持该退款渠道，当前仅支持：' . implode(',', self::$supportChannel));
        }

        try {
            $instance = self::getInstance($channel, $config);

            $ret = $instance->refund($refundData);
        } catch (PayException $e) {
            throw $e;
        }

        return $ret;
    }
}
