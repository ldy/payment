<?php

/**
 * @author yeran
 * @time 2018-10-30 11:29
 * @desc 交易查询
 */

namespace Payment\Client;

use Payment\Common\PayException;
use Payment\Config;
use Payment\QueryContext;

/**
 * 查询的客户端类
 * Class Query
 * @package Payment\Client
 */
class Query
{
    protected static $supportType = [
        Config::ALI_CHARGE,
        Config::ALI_REFUND,
        Config::ALI_TRANSFER,
        Config::ALI_RED,

        Config::WX_CHARGE,
        Config::WX_REFUND,
        Config::WX_RED,
        Config::WX_TRANSFER, //微信企业付款到零钱
        Config::WX_PAY_BANK, //微信企业付款到银行卡

        Config::CMB_CHARGE,
        Config::CMB_REFUND,

        Config::TL_QUERY,//通联支付

        Config::MI_QUERY,//米付

        Config::SW_QUERY,//扫呗
        Config::SW_T_QUERY,//扫呗(3.0)

        Config::FU_CHARGE,
        Config::FU_REFUND,
		Config::LTF_CHARGE,
		Config::LTF_REFUND,

		Config::YS_QUERY,//易生

		Config::YSE_QUERY,//易生

        Config::WNF_QUERY,//微诺付

        Config::HSQ_QUERY, //慧收钱
        Config::HSQ_QUERY_REFUND, //慧收钱查询退款
        Config::HSQ_QUERY_TRANSFER, //宝付代付交易状态查证接口


    ];

    /**
     * 异步通知类
     * @var QueryContext
     */
    protected static $instance;

    protected static function getInstance($queryType, $config)
    {
        if (is_null(self::$instance)) {
            static::$instance = new QueryContext();

            try {
                static::$instance->initQuery($queryType, $config);
            } catch (PayException $e) {
                throw $e;
            }
        }

        return static::$instance;
    }

    /**
     * @param string $queryType
     * @param array $config
     * @param array $metadata
     * @return array
     * @throws PayException
     */
    public static function run($queryType, $config, $metadata)
    {
        if (!in_array($queryType, self::$supportType)) {
            throw new PayException('sdk当前不支持该类型查询，当前仅支持：' . implode(',', self::$supportType));
        }

        try {
            $instance = self::getInstance($queryType, $config);

            $ret = $instance->query($metadata);
        } catch (PayException $e) {
            throw $e;
        }

        return $ret;
    }

}
