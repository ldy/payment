<?php


namespace Payment\Client;


use Payment\ChargeContext;
use Payment\Common\PayException;
use Payment\Config;

class Charge
{
    private static $supportChannel = [
        Config::ALI_CHANNEL_APP,// 支付宝 APP 支付
        Config::ALI_CHANNEL_WAP, // 支付宝手机网页支付
        Config::ALI_CHANNEL_WEB, // 支付宝电脑网站支付
        Config::ALI_CHANNEL_QR, // 支付宝当面付-扫码支付
        Config::ALI_CHANNEL_BAR,// 支付宝当面付-条码支付

        Config::WX_CHANNEL_APP,// 微信 APP 支付
        Config::WX_CHANNEL_PUB,// 微信公众号支付
        Config::WX_CHANNEL_QR,// 微信公众号扫码支付
        Config::WX_CHANNEL_BAR,// 微信刷卡支付
        Config::WX_CHANNEL_WAP,// 微信 WAP 支付（此渠道仅针对特定客户开放）
        Config::WX_CHANNEL_LITE,// 微信小程序支付

        Config::CMB_CHANNEL_APP,// 招行一网通
        Config::APPLE_PAY,// Apple Pay

        Config::TL_CHANNEL_LITE, // 通联支付
        Config::MI_CHANNEL_LITE,//米付小程序通道

        //立楚扫呗：公众号、小程序、扫码、自助收款
        Config::SW_CHANNEL_PUB,
        Config::SW_CHANNEL_LITE,
        Config::SW_CHANNEL_SCAN,
        Config::SW_CHANNEL_FACEPAY,
        Config::SW_CHANNEL_BAR,

        Config::SW_T_CHANNEL_PUB,
        Config::SW_T_CHANNEL_LITE,
        Config::SW_T_CHANNEL_SCAN,
        Config::SW_T_CHANNEL_FACEPAY,
        Config::SW_T_CHANNEL_BAR,

        Config::FU_CHANNEL_PUB,
        Config::FU_CHANNEL_LITE,
        Config::FU_CHANNEL_SCAN,
        Config::FU_CHANNEL_BAR,

		Config::LTF_CHANNEL_PUB,
		Config::LTF_CHANNEL_LITE,
		Config::LTF_CHANNEL_SCAN,
		Config::LTF_CHANNEL_BAR,
		Config::LTF_CHANNEL_PAY,

        //易生支付
        Config::YS_CHANNEL_PUB,
        Config::YS_CHANNEL_LITE,
        Config::YS_CHANNEL_SCAN,
        Config::YS_CHANNEL_NATIVE,
//        Config::YS_CHANNEL_BAR

        //盛银支付
        Config::YSE_CHANNEL_PUB,
        Config::YSE_CHANNEL_LITE,

        //微诺付
        Config::WNF_WECHAT_LITE,
        Config::WNF_ALI_APP,

        //慧收钱
        Config::HSQ_CHANNEL_LITE,
        Config::HSQ_CHANNEL_APP,
        Config::HSQ_CHANNEL_JS,

    ];

    /**
     * 异步通知类
     * @var ChargeContext
     */
    protected static $instance;

    protected static function getInstance($channel, $config)
    {
        if (is_null(self::$instance)) {
            static::$instance = new ChargeContext();

            try {
                static::$instance->initCharge($channel, $config);
            } catch (PayException $e) {
                throw $e;
            }
        }

        return static::$instance;
    }

    /**
     * @param string $channel
     * @param array $config
     * @param array $metadata
     *
     * @return mixed
     * @throws PayException
     */
    public static function run($channel, $config, $metadata)
    {
        if (! in_array($channel, self::$supportChannel)) {
            throw new PayException('sdk当前不支持该支付渠道，当前仅支持：' . implode(',', self::$supportChannel));
        }

        try {
            $instance = self::getInstance($channel, $config);

            $ret = $instance->charge($metadata);
        } catch (PayException $e) {
            throw $e;
        }

        return $ret;
    }
}
