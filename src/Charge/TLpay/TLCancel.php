<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/4/23
 * Time: 下午5:44
 */

namespace Payment\Charge\TLpay;

use Payment\Common\TLConfig;
use Payment\Common\TLpay\Data\Cancel\TLCancelData;
use Payment\Common\TLpay\TLBaseStrategy;

/**
 * Class TLCancel
 * @package Payment\Charge\Wx
 */
class TLCancel extends TLBaseStrategy{

    /**
     * 交易撤销 的url
     * @return null|string
     *
     */
    protected function getReqUrl()
    {
        return TLConfig::ORDER_CANCEL_URL;
    }

    /**
     * 获取交易退款对应的数据完成类
     * @return TLCancelData
     *
     */
    public function getBuildDataClass()
    {
        // TODO: Implement getBuildDataClass() method.

        $this->config->paytype = 'W06';
        return TLCancelData::class;
    }




}