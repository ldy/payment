<?php
/**
 *
 * @createTime: 2016-07-14 18:28
 * @description: 微信 公众号 支付接口
 * @link      https://github.com/helei112g/payment/tree/paymentv2
 * @link      https://helei112g.github.io/
 */

namespace Payment\Charge\TLpay;

use Payment\Common\TLpay\Data\BackTLChargeData;
use Payment\Common\TLpay\Data\Charge\TLChargeData;
use Payment\Common\TLpay\TLBaseStrategy;

/**
 * Class TLLiteCharge
 *
 * 通联支付-微信小程序支付
 *
 * @package Payment\Charge\TLpay
 *
 */
class TLLiteCharge extends TLBaseStrategy
{
    public function getBuildDataClass()
    {
        $this->config->paytype = 'W06';
        return TLChargeData::class;
    }

    /**
     * 处理返回值。直接返回与微信小程序文档对应的字段
     * @param array $ret
     *
     * @return array $backData  包含以下键
     *
     * ```php
     * $backData = [
     *  'appId' => '',   // 小程序id
     *  'package'   => '',  // 订单详情扩展字符串  统一下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
     *  'nonceStr'  => '',   // 随机字符串
     *  'timeStamp' => '',   // 时间戳
     *  'signType'  => '',   // 签名算法，暂支持MD5
     *  'paySign'  => '',  // 签名
     * ];
     * ```
     * 
     */
    protected function retData(array $ret)
    {
        return json_decode($ret['payinfo'],true);
    }
}
