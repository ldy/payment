<?php
/**
 * @author: yeran
 * @createTime: 2018-10-30 11:13
 * @description: 米付支付发起支付接口
 */

namespace Payment\Charge\MiPay;

use Payment\Common\MiPay\Data\Charge\MiChargeData;
use Payment\Common\MiPay\MiBaseStrategy;


/**
 * Class MiLiteCharge
 *
 * 米付-微信小程序支付
 *
 * @package Payment\Charge\MiPay
 */
class MiLiteCharge extends MiBaseStrategy
{
    public function getBuildDataClass()
    {
        return MiChargeData::class;
    }

    /**
     * 处理返回值。直接返回与微信小程序文档对应的字段
     * @param array $ret
     *
     * @return array $backData  包含以下键
     *
     * ```php
     * $backData = [
     *  'appId' => '',   // 小程序id
     *  'package'   => '',  // 订单详情扩展字符串  统一下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
     *  'nonceStr'  => '',   // 随机字符串
     *  'timeStamp' => '',   // 时间戳
     *  'signType'  => '',   // 签名算法，暂支持MD5
     *  'paySign'  => '',  // 签名
     * ];
     * ```
     *
     */
    protected function retData(array $ret)
    {
        return $ret['jspackage'];
    }
}
