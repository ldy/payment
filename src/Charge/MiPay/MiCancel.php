<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/4/23
 * Time: 下午5:44
 */

namespace Payment\Charge\MiPay;

use Payment\Common\MiConfig;
use Payment\Common\MiPay\Data\Cancel\MiCancelData;
use Payment\Common\MiPay\MiBaseStrategy;

/**
 * Class MiCancel
 * @package Payment\Charge\MiPay
 */
class MiCancel extends MiBaseStrategy{

    /**
     * 交易撤销 的url
     * @return null|string
     *
     */
    protected function getReqUrl()
    {
        return MiConfig::GATEWAY_URL;
    }

    /**
     * 获取交易退款对应的数据完成类
     * @return MiCancelData
     *
     */
    public function getBuildDataClass()
    {
        // TODO: Implement getBuildDataClass() method.

        $this->config->paytype = 'W06';
        return MiCancelData::class;
    }




}