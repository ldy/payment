<?php

namespace Payment\Charge\YSE;

use Payment\Common\BaseData;
use Payment\Common\YSEConfig;
use Payment\Common\YSEpay\Data\Charge\ScanChargeData;
use Payment\Common\YSEpay\YSEBaseStrategy;

class YSEScanCharge extends YSEBaseStrategy{

	/**
	 * 获取支付对应的数据完成类
	 *
	 * @return BaseData
	 */
	public function getBuildDataClass(){
		// TODO: Implement getBuildDataClass() method.
		return ScanChargeData::class;
	}

	protected function getReqUrl($url = null){
		return parent::getReqUrl($url??YSEConfig::SCANPAY_URL); // TODO: Change the autogenerated stub
	}
}
