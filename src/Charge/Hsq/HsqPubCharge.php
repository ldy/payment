<?php

namespace Payment\Charge\Hsq;

use Payment\Common\Hsq\Data\BackPubChargeData;
use Payment\Common\Hsq\Data\Charge\PubChargeData;
use Payment\Common\Hsq\HsqBaseStrategy;
use Payment\Common\HsqConfig;

/**
 * Class FuPubCharge
 * @package Payment\Charge\Fu
 */
class HsqPubCharge extends HsqBaseStrategy
{

    public function getBuildDataClass()
    {
        $this->config->method = HsqConfig::UNIFIED_URL;
        return PubChargeData::class;
    }

    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url);
    }

    /**
     * 处理支付的返回值。直接返回与微信文档对应的字段
     * @param array $ret
     *
     * @return array $data  包含以下键
     *
     * ```php
     * $data = [
     *  'appId' => '',   // 公众号id
     *  'package'   => '',  // 订单详情扩展字符串  统一下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
     *  'nonceStr'  => '',   // 随机字符串
     *  'timeStamp' => '',   // 时间戳
     *  'signType'  => '',   // 签名算法，暂支持MD5
     *  'paySign'  => '',  // 签名
     * ];
     * ```
     *
     * @throws \Payment\Common\PayException
     */
    protected function retData(array $ret)
    {
        $back = new BackPubChargeData($this->config, $ret);
//        $back->setSign();
        $backData = $back->getData();
//        $backData['paySign'] = $backData['sign'];
        // 移除sign
        unset($backData['sign']);

        // 公众号支付返回数组结构
        return $backData;
    }
}
