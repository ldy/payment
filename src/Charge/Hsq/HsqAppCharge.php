<?php

namespace Payment\Charge\Hsq;

use Payment\Common\Hsq\Data\BackAppChargeData;
use Payment\Common\Hsq\Data\Charge\AppChargeData;
use Payment\Common\Hsq\HsqBaseStrategy;
use Payment\Common\HsqConfig;
use Payment\Common\PayException;

/**
 * HsqAppCharge
 */
class HsqAppCharge extends HsqBaseStrategy
{

    public function getBuildDataClass()
    {
        $this->config->method = HsqConfig::APP_ORDER_URL;
        return AppChargeData::class;
    }

    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url);
    }

    /**
     * 处理支付的返回值。直接返回与微信文档对应的字段
     * @param array $ret
     *
     * @return array $data  包含以下键
     *
     * ```php
     * $data = [
     *  'appId' => '',   // 公众号id
     *  'package'   => '',  // 订单详情扩展字符串  统一下单接口返回的prepay_id参数值，提交格式如：prepay_id=***
     *  'nonceStr'  => '',   // 随机字符串
     *  'timeStamp' => '',   // 时间戳
     *  'signType'  => '',   // 签名算法，暂支持MD5
     *  'paySign'  => '',  // 签名
     * ];
     * ```
     *
     * @throws PayException
     */
    protected function retData(array $ret)
    {
        $back = new BackAppChargeData($this->config, $ret);
        $backData = $back->getData();
        unset($backData['sign']);
        return $backData;
    }
}
