<?php

namespace Payment\Charge\Sw;

use Payment\Common\Sw\Data\BackCancelData;
use Payment\Common\Sw\Data\Cancel\CancelData;

use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * Class SwCancel
 *
 * 扫呗取消订单 (只针对刷卡支付!!!!!!)
 *
 * @package Payment\Charge\Sw
 *
 */
class SwCancel extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return CancelData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwConfig::CANCEL_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackCancelData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
