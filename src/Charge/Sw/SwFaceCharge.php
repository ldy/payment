<?php

namespace Payment\Charge\Sw;

use Payment\Common\Sw\Data\BackFaceChargeData;
use Payment\Common\Sw\Data\Charge\FaceChargeData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * Class SwFaceCharge
 *
 * 扫呗自主收银（人脸支付）
 *
 * @package Payment\Charge\Sw
 *
 */
class SwFaceCharge extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return FaceChargeData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwConfig::FACEPAY_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackFaceChargeData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
