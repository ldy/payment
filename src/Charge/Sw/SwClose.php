<?php

namespace Payment\Charge\Sw;

use Payment\Common\Sw\Data\Cancel\BackCloseData;
use Payment\Common\Sw\Data\Cancel\CloseData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * Class SwClose
 *
 * 扫呗关闭订单 (（仅限服务商模式商户且为微信支付时可用）!!!!!!)
 *
 * 此接口也支持支付宝，但仅限扫了二维码但没有输密码的情况下
 * 刷卡支付不能进行关单操作
 *
 * @package Payment\Charge\Sw
 *
 */
class SwClose extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return CloseData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwConfig::CLOSE_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackCloseData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
