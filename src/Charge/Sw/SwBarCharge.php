<?php

namespace Payment\Charge\Sw;
use Payment\Common\Sw\Data\BackBarChargeData;
use Payment\Common\Sw\Data\Charge\BarChargeData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * Class SwBarCharge
 *
 * 扫呗刷卡（条码）支付
 *
 * @package Payment\Charge\Sw
 *
 */
class SwBarCharge extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return BarChargeData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwConfig::BARCODE_URL);
    }

    /**
     * 处理扫码支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackBarChargeData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
