<?php

namespace Payment\Charge\Sw;
use Payment\Common\Sw\Data\BackScanChargeData;
use Payment\Common\Sw\Data\Charge\ScanChargeData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * Class SwScanCharge
 *
 * 扫呗扫码支付
 *
 * @package Payment\Charge\Sw
 *
 */
class SwScanCharge extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return ScanChargeData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwConfig::SCANPAY_URL);
    }

    /**
     * 处理扫码支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackScanChargeData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
