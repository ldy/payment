<?php

namespace Payment\Charge\Sw;

use Payment\Common\Sw\Data\BackLiteChargeData;
use Payment\Common\Sw\Data\Charge\LiteChargeData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * Class SwLiteCharge
 *
 * 扫呗小程序支付
 *
 * @package Payment\Charge\Sw
 *
 */
class SwLiteCharge extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return LiteChargeData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwConfig::LITEPAY_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackLiteChargeData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
