<?php

namespace Payment\Charge\Fu;

use Payment\Common\Fu\Data\BackScanChargeData;
use Payment\Common\Fu\Data\Charge\ScanChargeData;
use Payment\Common\Fu\FuBaseStrategy;
use Payment\Common\FuConfig;

/**
 * Class FuScanCharge
 * @package Payment\Charge\Fu
 */
class FuScanCharge extends FuBaseStrategy
{

    public function getBuildDataClass()
    {
        return ScanChargeData::class;
    }

    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? FuConfig::SCANPAY_URL);
    }

    /**
     * 处理扫码支付的返回值
     * @param array $ret
     * @return array|mixed
     * @throws \Payment\Common\PayException
     */
    protected function retData(array $ret)
    {
        $back = new BackScanChargeData($this->config, $ret);
        $backData = $back->getData();
        return $backData;
    }

}
