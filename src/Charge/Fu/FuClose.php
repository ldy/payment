<?php

namespace Payment\Charge\Fu;

use Payment\Common\Fu\Data\Cancel\CloseData;
use Payment\Common\Fu\FuBaseStrategy;
use Payment\Common\FuConfig;

/**
 * Class FuClose
 * @package Payment\Charge\Fu
 */
class FuClose extends FuBaseStrategy
{

    public function getBuildDataClass()
    {
        return CloseData::class;
    }

    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? FuConfig::CLOSE_URL);
    }

    /**
     * retData
     * @param array $data
     * @return array|mixed
     */
    protected function retData(array $data)
    {
        $backData = [
            'order_type' => $data['order_type'] ?? '', //订单类型
            'order_no' => $data['mchnt_order_no'] ?? '', //商户订单号, 商户系统内部的订单号
        ];
        return $backData;
    }

}
