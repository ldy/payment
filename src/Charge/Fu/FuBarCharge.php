<?php

namespace Payment\Charge\Fu;

use Payment\Common\Fu\Data\BackBarChargeData;
use Payment\Common\Fu\Data\Charge\BarChargeData;
use Payment\Common\Fu\FuBaseStrategy;
use Payment\Common\FuConfig;

/**
 * Class FuScanCharge
 * @package Payment\Charge\Fu
 */
class FuBarCharge extends FuBaseStrategy
{

    public function getBuildDataClass()
    {
        return BarChargeData::class;
    }

    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? FuConfig::MICROPAY_URL);
    }

    /**
     * 处理扫码支付的返回值
     * @param array $ret
     * @return array|mixed
     * @throws \Payment\Common\PayException
     */
    protected function retData(array $ret)
    {
        $back = new BackBarChargeData($this->config, $ret);
        $backData = $back->getData();
        return $backData;
    }

}
