<?php

namespace Payment\Charge\SwThree;

use Payment\Common\SwThree\Data\BackLiteChargeData;
use Payment\Common\SwThree\Data\Charge\LiteChargeData;
use Payment\Common\SwThree\SwBaseStrategy;
use Payment\Common\SwThreeConfig;

/**
 * Class SwLiteCharge
 *
 * 扫呗小程序支付
 *
 * @package Payment\Charge\Sw
 *
 */
class SwLiteThreeCharge extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return LiteChargeData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwThreeConfig::LITEPAY_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackLiteChargeData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
