<?php

namespace Payment\Charge\SwThree;
use Payment\Common\SwThree\Data\BackBarChargeData;
use Payment\Common\SwThree\Data\Charge\BarChargeData;
use Payment\Common\SwThree\SwBaseStrategy;
use Payment\Common\SwThreeConfig;

/**
 * Class SwBarCharge
 *
 * 扫呗刷卡（条码）支付
 *
 * @package Payment\Charge\Sw
 *
 */
class SwBarThreeCharge extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return BarChargeData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwThreeConfig::BARCODE_URL);
    }

    /**
     * 处理扫码支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackBarChargeData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
