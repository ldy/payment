<?php

namespace Payment\Charge\SwThree;

use Payment\Common\SwThree\Data\BackCancelData;
use Payment\Common\SwThree\Data\Cancel\CancelData;

use Payment\Common\SwThree\SwBaseStrategy;
use Payment\Common\SwThreeConfig;

/**
 * Class SwCancel
 *
 * 扫呗取消订单 (只针对刷卡支付!!!!!!)
 *
 * @package Payment\Charge\Sw
 *
 */
class SwThreeCancel extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return CancelData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwThreeConfig::CANCEL_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackCancelData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
