<?php

namespace Payment\Charge\Sw;


use Payment\Common\Sw\Data\BackFaceInfoData;
use Payment\Common\Sw\Data\Charge\FaceInfoData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * Class SwFaceInfo
 *
 * 自助收银SDK调用凭证获取接口
 *
 * @package Payment\Charge\Sw
 *
 */
class SwFaceInfo extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return FaceInfoData::class;
    }


    protected function getReqUrl($url = null){

        return parent::getReqUrl($url??SwConfig::FACEPAY_ACCESSTOKEN_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackFaceInfoData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
