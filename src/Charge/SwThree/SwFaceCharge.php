<?php

namespace Payment\Charge\SwThree;

use Payment\Common\SwThree\Data\BackFaceChargeData;
use Payment\Common\SwThree\Data\Charge\FaceChargeData;
use Payment\Common\SwThree\SwBaseStrategy;
use Payment\Common\SwThreeConfig;

/**
 * Class SwFaceCharge
 *
 * 扫呗自主收银（人脸支付）
 *
 * @package Payment\Charge\Sw
 *
 */
class SwFaceCharge extends SwBaseStrategy
{
    public function getBuildDataClass()
    {
        return FaceChargeData::class;
    }


    protected function getReqUrl($url=null){

        return parent::getReqUrl($url??SwThreeConfig::FACEPAY_URL);
    }

    /**
     * 处理小程序支付的返回值
     * @param array $ret
     *
     * @return array $backData
     *
     *
     */
    protected function retData(array $ret)
    {
        $back = new BackFaceChargeData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
