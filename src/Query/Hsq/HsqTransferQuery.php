<?php
/**
 *
 * @createTime: 2016-08-04 10:30
 * @description:
 */

namespace Payment\Query\Hsq;

use Payment\Common\Hsq\Data\TransferQueryData;
use Payment\Common\Hsq\HsqTransBaseStrategy;
use Payment\Common\HsqTransConfig;
use Payment\Config;

/**
 * Class HsqTransferQuery
 * @package Payment\Query
 *
 */
class HsqTransferQuery extends HsqTransBaseStrategy
{
    public function getBuildDataClass()
    {
        return TransferQueryData::class;
    }

    /**
     * 返回查询url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? HsqTransConfig::QUERY_URL);
    }

    /**
     * 处理通知的返回数据
     * @param array $ret
     * @return mixed
     * 
     */
    protected function retData(array $ret)
    {
        $head = $ret['trans_content']['trans_head'];
        if($head['return_code'] != '0000')
        {
            $retData = [
                'is_success'    => 'F',
                'error' => $head['return_msg'],
                'channel' => Config::HSQ_QUERY_TRANSFER,
            ];
        }
        else
        {
            $data = $ret['trans_content']['trans_reqDatas'][0]['trans_reqData'];
            $retData = [
                'is_success'    => 'T',
                'response'  => [
                    'datas' => $data,
                    'channel' => Config::HSQ_QUERY_TRANSFER,
                ],
            ];
        }
        return $retData;

    }



}
