<?php

namespace Payment\Query\Hsq;

use Payment\Common\Hsq\Data\Query\ChargeQueryData;
use Payment\Common\Hsq\HsqBaseStrategy;
use Payment\Common\HsqConfig;
use Payment\Config;

class HsqChargeQuery extends HsqBaseStrategy
{

    /**
     * 返回查询订单的数据
     * @return \Payment\Common\BaseData|string
     */
    public function getBuildDataClass()
    {
        $this->config->method = HsqConfig::QUERY_URL;
        return ChargeQueryData::class;
    }

    /**
     * 返回查询url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url);
    }

    /**
     * retData
     * @param array $data
     * @return array|mixed
     */
    protected function retData(array $data)
    {
        $backData = json_decode($data['result'],true);
        $backData['memo'] = json_decode($backData['memo'],true);
        return $backData;
    }

}
