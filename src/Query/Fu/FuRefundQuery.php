<?php

namespace Payment\Query\Fu;

use Payment\Common\Fu\Data\Query\RefundQueryData;
use Payment\Common\Fu\FuBaseStrategy;
use Payment\Common\FuConfig;

class FuRefundQuery extends FuBaseStrategy
{

    /**
     * 返回查询订单的数据
     * @return \Payment\Common\BaseData|string
     */
    public function getBuildDataClass()
    {
        return RefundQueryData::class;
    }

    /**
     * 返回查询url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? FuConfig::REFUND_QUERY_URL);
    }

    /**
     * retData
     * @param array $data
     * @return array|mixed
     */
    protected function retData(array $data)
    {
        $backData = [
            'order_type' => $data['order_type'] ?? '', //订单类型
            'order_no' => $data['mchnt_order_no'] ?? '', //原交易商户订单号
            'refund_no' => $data['cancel_order_no'] ?? '', //商户撤销单号
            'out_trade_no' => $data['transaction_id'] ?? '', //渠道交易流水号
            'out_refund_no' => $data['refund_id'] ?? '', //渠道撤销流水号
            'refund_fee' => $data['reserved_refund_amt'] ?? 0, //退款金额
            'other' => $data
        ];
        return $backData;
    }

}
