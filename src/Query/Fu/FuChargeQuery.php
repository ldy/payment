<?php

namespace Payment\Query\Fu;

use Payment\Common\Fu\Data\Query\ChargeQueryData;
use Payment\Common\Fu\FuBaseStrategy;
use Payment\Common\FuConfig;
use Payment\Config;

class FuChargeQuery extends FuBaseStrategy
{

    /**
     * 返回查询订单的数据
     * @return \Payment\Common\BaseData|string
     */
    public function getBuildDataClass()
    {
        return ChargeQueryData::class;
    }

    /**
     * 返回查询url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? FuConfig::CHARGE_QUERY_URL);
    }

    /**
     * retData
     * @param array $data
     * @return array|mixed
     */
    protected function retData(array $data)
    {
        $backData = [
            'amount' => $data['order_amt'],
            'channel' => Config::FU_CHARGE,
            'order_type' => $data['order_type'] ?? '',
            'order_no' => $data['mchnt_order_no'], //商户订单号, 商户系统内部的订单号
            'user_id' => $data['buyer_id'] ?? '', //用户在商户的id
            'buyer_id' => $data['reserved_buyer_logon_id'] ?? '', //买家在渠道登录账号
            'trade_state' => $data['trans_stat'], //查询状态
            'transaction_id' => $data['transaction_id'], //渠道订单号
            'time_end' => empty($data['reserved_txn_fin_ts']) ? '' : date('Y-m-d H:i:s', strtotime($data['reserved_txn_fin_ts'])), //用户支付时间
            'return_param' => $data['addn_inf'] ?? '', //附加数据
            'bank_type' => $data['reserved_bank_type'] ?? '', //付款方式
            'other' => $data
        ];
        return $backData;
    }

}
