<?php
/**
 *
 * @createTime: 2019-03-24 22:42
 * @description: 支付查询
 */

namespace Payment\Query\Sw;

use Payment\Common\Sw\Data\Query\BackChargeQueryData;
use Payment\Common\Sw\Data\Query\ChargeQueryData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;


class SwChargeQuery extends SwBaseStrategy
{

    /**
     * 返回查询订单的数据
     * 
     */
    public function getBuildDataClass()
    {
        return ChargeQueryData::class;
    }

    /**
     * 返回微信查询的url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url=null)
    {
        return parent::getReqUrl($url??SwConfig::CHARGE_QUERY_URL);
    }

    /**
     * 处理通知的返回数据
     * @param array $ret
     * @return mixed
     * 
     */
    protected function retData(array $ret)
    {
        $back = new BackChargeQueryData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
