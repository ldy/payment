<?php
/**
 * Created by IntelliJ IDEA.
 * User: yeran
 * Date: 2018/4/23
 * Time: 下午7:07
 */


namespace Payment\Query\TLpay;

use Payment\Common\TLConfig;
use Payment\Common\TLpay\Data\Query\TLQueryData;
use Payment\Common\TLpay\TLBaseStrategy;


class TLQuery extends TLBaseStrategy {

    /**
     * 获取支付对应的数据完成类
     *
     */
    public function getBuildDataClass()
    {
        // TODO: Implement getBuildDataClass() method.
        return TlQueryData::class;
    }

    /**
     * 返回查询的url
     * @return string
     *
     */
    protected function getReqUrl()
    {
        return TLConfig::CHARGE_QUERY_URL;
    }


    public function retData(array $ret)
    {
        //cusid	商户号		平台分配的商户号	否	15
        //appid	应用ID		平台分配的APPID	否	8
        //trxid	交易单号		平台的交易流水号	否	20
        //chnltrxid	支付渠道交易单号		如支付宝,微信平台的交易单号	是	50
        //reqsn	商户订单号		商户的交易订单号	否	32
        //trxcode	交易类型		交易类型	否	8	见3.2
        //trxamt	交易金额		单位为分	否	16
        //trxstatus	交易状态		交易的状态	是	4  如果trxstatus为空,则交易正在处理中,尚未完成
        //acct	支付平台用户标识		"JS支付时使用 微信支付-用户的微信openid 支付宝支付-用户user_id "	是	32	如果为空,则默认填000000
        //fintime	交易完成时间		yyyyMMddHHmmss	是	14
        //randomstr	随机字符串		随机生成的字符串	否	32
        //errmsg	错误原因		失败的原因说明	是	100
        //sign	签名			否	32	详见1.5

        return $ret;
    }
}