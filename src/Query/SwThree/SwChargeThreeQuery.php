<?php
/**
 *
 * @createTime: 2019-03-24 22:42
 * @description: 支付查询
 */

namespace Payment\Query\SwThree;

use Payment\Common\SwThree\Data\Query\BackChargeQueryData;
use Payment\Common\SwThree\Data\Query\ChargeQueryData;
use Payment\Common\SwThree\SwBaseStrategy;
use Payment\Common\SwThreeConfig;


class SwChargeThreeQuery extends SwBaseStrategy
{

    /**
     * 返回查询订单的数据
     * 
     */
    public function getBuildDataClass()
    {
        return ChargeQueryData::class;
    }

    /**
     * 返回微信查询的url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url=null)
    {
        return parent::getReqUrl($url??SwThreeConfig::CHARGE_QUERY_URL);
    }

    /**
     * 处理通知的返回数据
     * @param array $ret
     * @return mixed
     * 
     */
    protected function retData(array $ret)
    {
        // 移除sign
        unset($ret['key_sign']);
        return $ret;
    }
}
