<?php


namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class FuConfig extends ConfigInterface
{

    public const UNIFIED_URL = '/wxPreCreate';  //公众号/服务窗统一下单

    public const SCANPAY_URL = '/preCreate'; //主扫统一下单，扫码支付（预支付）

    public const MICROPAY_URL = '/micropay'; //条码支付，商户扫用户二维码收款

    public const QUERY_URL = '/commonQuery'; //订单查询

    public const REFUND_URL = '/commonRefund'; //退款申请

    public const CLOSE_URL = '/closeorder'; //关闭订单（只有未支付的订单才能发起关闭。订单生成后不能马上调用关单接口，最短调用时间间隔为5分钟）

    public const CANCEL_URL = '/cancelorder'; //撤销交易

    public const CHARGE_QUERY_URL = '/commonQuery'; //订单查询

    public const REFUND_QUERY_URL = '/refundQuery'; //退款查询

    public $base_url = 'https://fundwx.fuiou.com';

    public $version = '1.0';

    public $signType = 'RSA';

    public $institutionId; //机构号

    public $merchantId; //商户号

    public $terminalId = '88888888'; //终端号

    public $subAppid; //子商户公众号id，微信交易为商户的appid（小程序，公众号必填）

    public $randomStr; //随机字符串

    public $attach; //附加数据

    public $startTime; //交易开始时间（秒级时间戳）

    public $certainSignData = []; //专门用于签名的数据

    public $privateKey; //私钥

    public $spbillCreateIp;

    /**
     * FuConfig constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */
    private function initConfig(array $config)
    {
        $config = ArrayUtil::paraFilter($config);

        if (key_exists('ins_cd', $config) && !empty($config['ins_cd'])) $this->institutionId = $config['ins_cd']; else throw new PayException('必须提供机构号');

        if (key_exists('mchnt_cd', $config) && !empty($config['mchnt_cd'])) $this->merchantId = $config['mchnt_cd']; else throw new PayException('必须提供商户号');

        if (key_exists('term_id', $config) && !empty($config['term_id'])) $this->terminalId = $config['term_id']; //else throw new PayException('必须提供商户号');

        if (key_exists('sub_appid', $config) && !empty($config['sub_appid'])) $this->subAppid = $config['sub_appid'];

        if (key_exists('notify_url', $config) && !empty($config['notify_url'])) $this->notifyUrl = $config['notify_url']; else throw new PayException('必须提供通知地址');
        if (key_exists('private_key', $config) && !empty($config['private_key'])) $this->privateKey = $config['private_key']; else throw new PayException('必须提供私钥');

        if (key_exists('limit_pay', $config) && !empty($config['limit_pay']) && $config['limit_pay'][0] === 'no_credit') $this->limitPay = $config['limit_pay'][0]; //设置禁止使用的支付方式

        if (key_exists('return_raw', $config)) $this->returnRaw = filter_var($config['return_raw'], FILTER_VALIDATE_BOOLEAN);

        $this->randomStr = StrUtil::getNonceStr(); //生成随机字符串

        $this->attach = $config['attach'] ?? ($config['addn_inf'] ?? '');

        if (key_exists('base_url', $config) && !empty($config['base_url'])) $this->base_url = rtrim($config['base_url'], '/');

        $this->startTime = time();

        $this->trade_type = $this->order_type = $config['trade_type'] ?? ($config['order_type'] ?? '');
    }
}
