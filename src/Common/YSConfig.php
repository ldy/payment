<?php

namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class YSConfig extends ConfigInterface{

	public const REQUEST_URL = 'https://open.eycard.cn:8443/WorthTech_Access_AppPaySystemV2/apppayacc';

	public $request_url = 'https://open.eycard.cn:8443/WorthTech_Access_AppPaySystemV2/apppayacc';

	public $signType = 'MD5';

//	public $notifyurl;

	public $channelid;//渠道编号

	public $merid;//门店编号

    public $termid; //终端编号

	public $key; //签名密钥

//    public $tradetrace; //订单号
//
//    public $tradeamt; //订单号
//
//    public $openid; //订单号

	/**
	 * FuConfig constructor.
	 * @param array $config
	 * @throws PayException
	 */
	public function __construct(array $config)
	{
		try {
			$this->initConfig($config);
		} catch (PayException $e) {
			throw $e;
		}
	}

	/**
	 * 初始化配置文件参数
	 * @param array $config
	 * @throws PayException
	 */

	/**
	 * 初始化配置文件参数
	 * @param array $config
	 * @throws PayException
	 */
	private function initConfig(array $config)
	{
		$config = ArrayUtil::paraFilter($config);

		if (key_exists('channelid', $config) && !empty($config['channelid'])) $this->channelid = $config['channelid'];else throw new PayException('必须提供渠道号标识');

		if (key_exists('merid', $config) && !empty($config['merid'])) $this->merid = $config['merid']; else throw new PayException('必须提供商户编号');

		if (key_exists('termid', $config) && !empty($config['termid'])) $this->termid = $config['termid'];

		if (key_exists('key', $config) && !empty($config['key'])) $this->key = $config['key'];


//		if (key_exists('out_trade_no', $config) && !empty($config['out_trade_no'])) $this->tradetrace = $config['out_trade_no'];
//
//		if (key_exists('total_fee', $config) && !empty($config['total_fee'])) $this->tradeamt = $config['total_fee'];
//
//		if (key_exists('open_id', $config) && !empty($config['open_id'])) $this->openid = $config['open_id'];
	}
}
