<?php

namespace Payment\Common\Fu\Data;

/**
 * Class BackPubChargeData
 * @package Payment\Common\Fu\Data
 */
class BackPubChargeData extends FuBaseData
{

    public function getData()
    {
        $data = [];
        //TODO: 貌似不需要根据订单类型判断，返回参数中存在 reserved_pay_info 字段，该字段为JSON字符串，与发起支付时所需要参数相同，可以直接根据该字段值返回package
        if (!empty($this->reserved_pay_info)) {
            switch ($this->trade_type) {
                case 'JSAPI': //微信公众号
                case 'LETPAY': //小程序
                    $data = [
                        'appId' => $this->subAppid ?? $this->sdk_appid,
                        'timeStamp' => $this->sdk_timestamp ?? time(),
                        'nonceStr' => $this->sdk_noncestr,
                        'package' => $this->sdk_package,
                        'signType' => $this->sdk_signtype,
                        'paySign' => $this->sdk_paysign
                    ];
                    break;
                case 'FWC':
                    $data = [
                        'transaction_id' => $this->reserved_transaction_id
                    ];
                    break;
                default:
            }
        }
        $this->retData['package'] = !empty($this->reserved_pay_info) ? json_decode($this->reserved_pay_info, true) : $data;
        $this->retData['other'] = [
            'out_trade_no' => $this->reserved_fy_order_no ?: '',
            'attach' => $this->reserved_addn_inf ?: ''
        ];
        return parent::getData();
    }


    protected function buildData()
    {
    }

    protected function checkDataParam()
    {
    }

}
