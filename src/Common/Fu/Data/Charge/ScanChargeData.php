<?php

namespace Payment\Common\Fu\Data\Charge;

use Payment\Common\PayException;

/**
 * Class ScanChargeData
 * @package Payment\Common\Fu\Data\Charge
 */
class ScanChargeData extends ChargeBaseData
{

    /**
     * checkDataParam
     * @return mixed|void
     * @throws PayException
     */
    protected function checkDataParam()
    {
        parent::checkDataParam();

        $tradeType = $this->order_type = $this->trade_type ?? ($this->order_type ?? '');
        if (empty($tradeType)) throw new PayException('订单类型有误(支持WECHAT、ALIPAY、UNIONPAY)');

//        $openid = $this->openid;
//        if (empty($openid)) throw new PayException('用户标识有误');

        $clientIp = $this->client_ip;
        if (empty($clientIp)) $this->client_ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
    }

    /**
     * 构建用于支付的签名相关数据
     * @return array|void
     */
    protected function buildData()
    {
        $signData = [
            'version' => $this->version,
            'ins_cd' => $this->institutionId, //机构号，接入机构在富友的唯一代码
            'mchnt_cd' => $this->merchantId, //商户号, 富友分配给二级商户的商户号
            'term_id' => $this->terminalId ?? '88888888', //终端号(没有真实终端号统一填88888888)
            'random_str' => $this->randomStr, //随机字符串
            'order_type' => $this->order_type, //订单类型:ALIPAY、WECHAT、UNIONPAY(银联二维码）
            'goods_des' => $this->subject, //商品描述, 商品或支付单简要描述
            'goods_detail' => $this->body ?? '', //单品优惠功能字段(填错影响交易)
            'goods_tag' => $this->goods_tag ?? '', //商品标记
            'addn_inf' => $this->attach ?? '', //附加数据
            'mchnt_order_no' => $this->order_no, //商户订单号, 商户系统内部的订单号（5到30个字符、只能包含字母数字，区分大小写)
            'curr_type' => $this->curr_type ?? 'CNY', //货币类型，默认人民币：CNY
            'order_amt' => intval($this->amount), //总金额, 订单总金额，单位为分
            'term_ip' => $this->client_ip ?? '127.0.0.1', //终端IP
            'txn_begin_ts' => date('YmdHis', $this->startTime ?? time()), //交易起始时间, 订单生成时间，格式为yyyyMMddHHmmss
            'notify_url' => $this->notifyUrl ?? $this->notify_url, //通知地址, 接收富友异步通知回调地址，通知url必须为直接可访问的url，不能携带参数
//            'limit_pay' => $this->limitPay ?? '', //限制支付，no_credit:不能使用信用卡
//            'openid' => '', //用户标识
//            'sub_openid' => $this->openid ?? $this->sub_openid, //子商户用户标识，支付宝服务窗为用户buyer_id（此场景必填）微信公众号为用户的openid（小程序，公众号，服务窗必填）
//            'sub_appid' => $this->subAppid ?? ($this->sub_appid ?? ''), //子商户公众号id,微信交易为商户的appid（小程序，公众号必填）
            'reserved_sub_appid' => '', //$this->subAppid ?? ($this->sub_appid ?? ''), //子商户公众号id,微信交易为商户的appid（小程序，公众号必填）
            'reserved_expire_minute' => intval($this->reserved_expire_minute ?? 0), //交易关闭时间，如不设置默认为0，单位：分钟，最大值为1440
            'reserved_limit_pay' => $this->limitPay ?? '', //限制支付，no_credit:不能使用信用卡
            'reserved_fy_term_type' => $this->reserved_fy_term_type ?? 0, //富友终端类型
            'reserved_fy_term_id' => $this->reserved_fy_term_id ?? '', //富友终端号
            'reserved_fy_term_sn' => $this->reserved_fy_term_sn ?? '', //富友序列号
            'reserved_txn_bonus' => intval($this->reserved_txn_bonus ?? 0), //积分抵扣金额，单位为分
            'reserved_hb_fq_num' => intval($this->reserved_hb_fq_num ?? 0), //花呗分期期数：仅支持3、6、12
            'reserved_hb_fq_seller_percent' => intval($this->reserved_hb_fq_seller_percent ?? 0), //花呗分期商家手续费比例，目前仅支持用户出资，如需使用，请填写0
            'reserved_device_info' => $this->reserved_device_info ?? '' //设备信息，托传给微信。用于单品券核销
        ];
        foreach ($signData as $key => $value) if (false !== strpos($key, 'reserved_') && empty($value)) unset($signData[$key]);
        $this->retData = $signData;
    }

}
