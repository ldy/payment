<?php


namespace Payment\Common\Fu\Data\Charge;

use Payment\Common\Fu\Data\FuBaseData;
use Payment\Common\PayException;
use Payment\Config;

/**
 * Class ChargeBaseData
 * @package Payment\Common\Fu\Data\Charge
 */
abstract class ChargeBaseData extends FuBaseData
{

    /**
     * 检查传入的支付信息是否正确
     */
    protected function checkDataParam()
    {
        // 检查订单号是否合法
        if (empty($this->order_no) || mb_strlen($this->order_no) > 30) throw new PayException('商户系统订单号不能为空且长度不能超过30位');

        // 检查金额不能低于0.01
        if ($this->amount < bcmul(Config::PAY_MIN_FEE, 100)) throw new PayException('支付金额不能低于 ' . Config::PAY_MIN_FEE . ' 元');

        // 检查商品名称
        if (empty($this->subject)) throw new PayException('必须提供商品名称');

    }
}
