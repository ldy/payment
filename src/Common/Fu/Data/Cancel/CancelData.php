<?php

namespace Payment\Common\Fu\Data\Cancel;

use Payment\Common\Fu\Data\FuBaseData;
use Payment\Common\PayException;

/**
 * Class CancelData
 * @package Payment\Common\Fu\Data\Cancel
 */
class CancelData extends FuBaseData
{

    protected function checkDataParam()
    {
        if (empty($this->order_no) || mb_strlen($this->order_no) > 30) throw new PayException('商户系统订单号不能为空且长度不能超过30位');
        if (empty($this->refund_no) || mb_strlen($this->refund_no) > 30) throw new PayException('商户系统撤销单号不能为空且长度不能超过30位');

        $tradeType = $this->order_type = $this->trade_type ?? ($this->order_type ?? '');
        if (empty($tradeType)) throw new PayException('订单类型有误(支持WECHAT、ALIPAY，条码支付)');
    }

    protected function buildData()
    {
        $signData = [
            'version' => $this->version,
            'ins_cd' => $this->institutionId, //机构号，接入机构在富友的唯一代码
            'mchnt_cd' => $this->merchantId, //商户号, 富友分配给二级商户的商户号
            'term_id' => $this->terminalId ?? '88888888', //终端号(没有真实终端号统一填88888888)
            'random_str' => $this->randomStr, //随机字符串
            'mchnt_order_no' => $this->order_no, //商户订单号, 商户系统内部的订单号（5到30个字符、只能包含字母数字，区分大小写)
            'order_type' => $this->order_type, //订单类型:ALIPAY、WECHAT
            'cancel_order_no' => $this->refund_no, //商户撤销单号
            'operator_id' => $this->operator_id ?? '', //操作员
        ];
        if (!empty($this->reserved_fy_term_id)) $signData['reserved_fy_term_id'] = $this->reserved_fy_term_id; //富友终端号(非富友终端请勿填写)
//        foreach ($signData as $key => $value) if (false !== strpos($key, 'reserved_') && empty($value)) unset($signData[$key]);
        $this->retData = $signData;
    }

}
