<?php

namespace Payment\Common\Fu\Data\Cancel;

use Payment\Common\Fu\Data\FuBaseData;
use Payment\Common\PayException;

/**
 * Class CloseData
 * @package Payment\Common\Fu\Data\Cancel
 */
class CloseData extends FuBaseData
{

    protected function checkDataParam()
    {
        if (empty($this->order_no) || mb_strlen($this->order_no) > 30) throw new PayException('商户系统订单号不能为空且长度不能超过30位');

        $tradeType = $this->order_type = $this->trade_type ?? ($this->order_type ?? '');
        if (empty($tradeType)) throw new PayException('订单类型有误(支持WECHAT、WXAPP、WXH5、ALIPAY)');
    }

    protected function buildData()
    {
        $signData = [
            'version' => $this->version,
            'ins_cd' => $this->institutionId, //机构号，接入机构在富友的唯一代码
            'mchnt_cd' => $this->merchantId, //商户号, 富友分配给二级商户的商户号
            'term_id' => $this->terminalId ?? '88888888', //终端号(没有真实终端号统一填88888888)
            'random_str' => $this->randomStr, //随机字符串
            'mchnt_order_no' => $this->order_no, //商户订单号, 商户系统内部的订单号
            'order_type' => $this->order_type, //订单类型:WECHAT(统一下单、公众号、小程序),WXAPP(微信 app),WXH5(微信 h5),ALIPAY(统一下单，服务窗)
            'sub_appid' => $this->sub_appid ?? '', //子商户公众号id, 子商户配置多个公众号时必填
        ];
        $this->retData = $signData;
    }

}
