<?php

namespace Payment\Common\Fu\Data;

/**
 * Class BackScanChargeData
 * @package Payment\Common\Fu\Data
 */
class BackScanChargeData extends FuBaseData
{

    public function getData()
    {
        $data = [
            'qrcode' => $this->qr_code ?? '',
            'session_id' => $this->session_id
        ];
        $this->retData['package'] = $data;
        $this->retData['other'] = [
            'out_trade_no' => $this->reserved_fy_order_no ?: '',
            'channel_trade_no' => $this->reserved_channel_order_id ?: '',
            'attach' => $this->reserved_addn_inf ?: ''
        ];

        return parent::getData();
    }

    protected function buildData()
    {
    }

    protected function checkDataParam()
    {
    }

}
