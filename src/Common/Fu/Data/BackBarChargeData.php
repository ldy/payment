<?php

namespace Payment\Common\Fu\Data;

/**
 * Class BackBarChargeData
 * @package Payment\Common\Fu\Data
 */
class BackBarChargeData extends FuBaseData
{

    public function getData()
    {
        $this->retData = [
            'order_no' => $this->reserved_mchnt_order_no, //商户订单号
            'out_order_no' => $this->reserved_fy_order_no, //富友订单号
            'order_type' => $this->order_type, //订单类型
            'amount' => $this->total_amount, //订单金额
            'user_id' => $this->buyer_id ?? '', //用户在商户的id
            'buyer_id' => $this->reserved_buyer_logon_id ?? '', //买家在渠道登录账号
            'transaction_id' => $this->transaction_id,
            'channel_order_id' => $this->reserved_channel_order_id, //条码流水号，用户账单二维码对应的流水
            'time_end' => empty($this->reserved_txn_fin_ts) ? '' : date('Y-m-d H:i:s', strtotime($this->reserved_txn_fin_ts)), //用户支付时间
            'bank_type' => $this->reserved_bank_type ?? '', //付款方式
            'return_param' => $this->addn_inf ?? '', //附加数据
        ];

        return parent::getData();
    }

    protected function buildData()
    {
    }

    protected function checkDataParam()
    {
    }

}
