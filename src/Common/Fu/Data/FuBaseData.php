<?php

namespace Payment\Common\Fu\Data;

use Payment\Common\BaseData;
use Payment\Common\PayException;

/**
 * Class FuBaseData
 * @package Payment\Common\Fu\Data
 */
abstract class FuBaseData extends BaseData
{

    /**
     * 签名算法实现
     * @param string $signStr
     * @return string
     * @throws PayException
     */
    protected function makeSign($signStr)
    {
        return $this->sign($signStr);
    }

    /**
     * 签名
     * @param $signStr
     * @return string
     * @throws PayException
     */
    private function sign($signStr)
    {
        if (empty($this->privateKey)) throw new PayException('Private key does not exist');
        $privateKey = is_file($this->privateKey) ? file_get_contents($this->privateKey) : $this->privateKey;
        if (empty($privateKey)) throw new PayException('Private key error');

        $pkeyid = openssl_get_privatekey($privateKey);
        if (empty($pkeyid)) throw new PayException('Private key resource identifier false');

        $verify = openssl_sign($signStr, $signature, $pkeyid, OPENSSL_ALGO_MD5);

        openssl_free_key($pkeyid);

        return base64_encode($signature);
    }

}
