<?php

namespace Payment\Common\LTFpay\Data\Cancel;

use Payment\Common\LTFpay\Data\LTFBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class CancelData extends LTFBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
		$this->certainSignData = [
			'appId'        => $this->appId,
			'random'       => $this->randomStr,
			'merchantCode' => $this->merchantCode,
			'outTradeNo'   => $this->out_trade_no,
		];
		$signData = array_merge($this->certainSignData, []);
		if(isset($this->operatorId)){
			$signData['operatorId'] = $this->operatorId;
		}
		if(isset($this->reason)){
			$signData['reason'] = $this->reason;
		}
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		parent::checkDataParam();
	}
}
