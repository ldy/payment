<?php

namespace Payment\Common\LTFpay\Data\Charge;

use Payment\Common\LTFpay\Data\LTFBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class BarChargeData extends LTFBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
		$this->certainSignData = [
			'appId'        => $this->appId,
			'random'       => $this->randomStr,
			'merchantCode' => $this->merchantCode,
			'outTradeNo'   => $this->out_trade_no,
			'totalAmount'  => $this->total_fee,
			'authCode'     => $this->auth_code,
		];

		$signData = array_merge($this->certainSignData, []);
		if(isset($this->discountAmount)){
			$signData['discountAmount'] = $this->discountAmount;
		}
		if(isset($this->unDiscountAmount)){
			$signData['unDiscountAmount'] = $this->unDiscountAmount;
		}
		if(isset($this->goodsDetail)){
			$signData['goodsDetail'] = $this->goodsDetail;
		}
		if(isset($this->subAppId)){
			$signData['subAppId'] = $this->subAppId;
		}
		if(isset($this->operatorId)){
			$signData['operatorId'] = $this->operatorId;
		}
		if(isset($this->orderRemark)){
			$signData['orderRemark'] = $this->orderRemark;
		}
		if(isset($this->terminalId)){
			$signData['terminalId'] = $this->terminalId;
		}
		if(isset($this->subject)){
			$signData['subject'] = $this->subject;
		}
		if(isset($this->hbFqNum)){
			$signData['hbFqNum'] = $this->hbFqNum;
		}
		if(isset($this->hbFqSellerPercent)){
			$signData['hbFqSellerPercent'] = $this->hbFqSellerPercent;
		}
		if(isset($this->sysServiceProviderId)){
			$signData['sysServiceProviderId'] = $this->sysServiceProviderId;
		}
		if(isset($this->orderSource)){
			$signData['orderSource'] = $this->orderSource;
		}
		if(isset($this->appType)){
			$signData['appType'] = $this->appType;
		}
		if(isset($this->appSource)){
			$signData['appSource'] = $this->appSource;
		}
		if(isset($this->appExtNo)){
			$signData['appExtNo'] = $this->appExtNo;
		}
		if(isset($this->appVersion)){
			$signData['appVersion'] = $this->appVersion;
		}
		if(isset($this->deviceInfo)){
			$signData['deviceInfo'] = $this->deviceInfo;
		}
		// 移除数组中的空值
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		parent::checkDataParam();
		if(empty($this->auth_code)){
			throw new PayException('无效的支付授权码');
		}
	}
}
