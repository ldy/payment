<?php

namespace Payment\Common\LTFpay\Data;

use Payment\Common\BaseData;
use Payment\Common\PayException;

abstract class LTFBaseData extends BaseData{

	/**
	 * 签名算法实现  便于后期扩展微信不同的加密方式
	 *
	 * @param string $signStr
	 * @return string
	 */
	protected function makeSign($signStr){
		switch ($this->signType) {
			case 'MD5':
				$signStr .= '&key=' . $this->key;
				$sign = strtolower(md5($signStr));
				break;

			default:
				$sign = '';
		}

		$this->retData['sign'] = $sign;

		return $sign;
		// TODO: Implement makeSign() method.
	}

	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		// 检查订单号是否合法
		if (empty($this->out_trade_no) || mb_strlen($this->out_trade_no) > 64) {
			throw new PayException('商户系统的订单号不能为空且长度不能超过64位');
		}

		if (empty($this->appId) || empty($this->merchantCode)) {
			throw new PayException('必须提供合作方标识与门店编号');
		}
	}
}
