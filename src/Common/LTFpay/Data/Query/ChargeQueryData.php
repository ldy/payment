<?php

namespace Payment\Common\LTFpay\Data\Query;

use Payment\Common\LTFpay\Data\LTFBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class ChargeQueryData extends LTFBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
		$this->certainSignData = [
			'appId'            => $this->appId,
			'random'           => $this->randomStr,
			'merchantCode'     => $this->merchantCode,
			'outTradeNo'       => $this->out_trade_no,
			'outTransactionId' => $this->out_transaction_id,
		];
		$signData = array_merge($this->certainSignData, []);
		if(isset($this->operatorId)){
			$signData['operatorId'] = $this->operatorId;
		}
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		parent::checkDataParam();
		if(empty($this->out_transaction_id)){
			throw new PayException('第三方订单号不能为空');
		}
	}
}
