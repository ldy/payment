<?php

namespace Payment\Common\LTFpay\Data\Query;

use Payment\Common\LTFpay\Data\LTFBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class RefundQueryData extends LTFBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
		$this->certainSignData = [
			'appId'        => $this->appId,
			'random'       => $this->randomStr,
			'merchantCode' => $this->merchantCode,
			'refundNo'     => $this->refund_no,
		];
		$signData = array_merge($this->certainSignData, []);
		if(isset($this->operatorId)){
			$signData['operatorId'] = $this->operatorId;
		}
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	protected function checkDataParam(){
		parent::checkDataParam();
		if(empty($this->refund_no)){
			throw new PayException('必须提供退款订单号');
		}
	}
}
