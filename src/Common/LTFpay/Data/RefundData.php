<?php

namespace Payment\Common\LTFpay\Data;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class RefundData extends LTFBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
		$this->certainSignData = [
			'appId'        => $this->appId,
			'random'       => $this->randomStr,
			'merchantCode' => $this->merchantCode,
			'outTradeNo'   => $this->out_trade_no,
			'refundNo'     => $this->refund_no,
		];
		$signData = array_merge($this->certainSignData, []);
		if(isset($this->outTransactionId)){
			$signData['outTransactionId'] = $this->outTransactionId;
		}
		if(isset($this->refundAmount)){
			$signData['refundAmount'] = $this->refundAmount;
		}
		if(isset($this->refundReason)){
			$signData['refundReason'] = $this->refundReason;
		}
		if(isset($this->operatorId)){
			$signData['operatorId'] = $this->operatorId;
		}
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		parent::checkDataParam();
		if(empty($this->refund_no)){
			throw new PayException('请生成商户退款订单号');
		}
	}
}
