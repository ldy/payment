<?php

namespace Payment\Common\Sw\Data\Charge;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

/**
 * Class BarChargeData
 *
 * @inheritdoc
 * @property string $auth_code  扫码支付授权码，设备读取用户微信/支付宝等中的条码或者二维码信息
 *
 * @package Payment\Common\Sw\Data\Charge
 */
class BarChargeData extends ChargeBaseData
{

    protected function checkDataParam()
    {
        parent::checkDataParam();

        if(!isset($this->auth_no) || empty($this->auth_no)){
            throw new PayException('授权码，客户的付款码不能为空');
        }

        $payType = $this->pay_type;//010微信，020 支付宝，060qq钱包，080京东钱包，090口碑，100翼支付，110银联二维码，000自动识别类型
        if ($payType == '010') {
            if(empty($this->sub_appid))
                throw new PayException('公众号appid，支付时使用的appid');
        }

        $this->service_id = '010';
    }

    /**
     * 生成下单的数据
     */
    protected function buildData()
    {
        $this->certainSignData = [
            // 基本数据
            'pay_ver'       => trim($this->pay_ver),
            'pay_type'      => trim($this->pay_type),
            'service_id'    => $this->service_id,
            'merchant_no'   => $this->merchant_no,
            'terminal_id'   => $this->terminal_id,
            'terminal_trace'  => $this->terminal_trace,
            'terminal_time' => $this->terminal_time,
            'auth_no'   =>$this->auth_no,//授权码，客户的付款码
            'total_fee'     => $this->total_fee,  // 金额，单位分

        ];
        $signData  = array_merge($this->certainSignData,[]);
        if(isset($this->sub_appid)){
            $signData['sub_appid'] = $this->sub_appid;
        }
        if(isset($this->order_body)){
            $signData['order_body'] = $this->order_body;
        }
        if(isset($this->attach)){
            $signData['attach'] =  $this->attach;
        }
        if(isset($this->goods_detail)){
            $signData['goods_detail'] =  $this->goods_detail;

            //goods_Id	String	N	商品编号
            //goods_name	String	N	商品名称
            //quantity	String	N	商品数量
            //price	String	N	商品单价，单位为分
        }
        if(isset($this->goods_tag)){ //订单优惠标记，代金券或立减优惠功能的参数（字段值：cs和bld）
            $signData['goods_tag'] =  $this->goods_tag;
        }
        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }
}
