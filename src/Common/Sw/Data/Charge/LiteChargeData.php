<?php


namespace Payment\Common\Sw\Data\Charge;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

/**
 * Class LiteChargeData
 * 扫呗微信小程序支付
 *
 *
 * @package Payment\Common\Sw\Data\Charge
 *
 */
class LiteChargeData extends ChargeBaseData
{
    protected function checkDataParam()
    {
        parent::checkDataParam(); // TODO: Change the autogenerated stub

        // 公众号支付,必须设置openid
        $payType = $this->pay_type;//010微信，020支付宝，060qq钱包，090口碑，100翼支付
        if (($payType == '010' || $payType == '020')) {
            if(empty($this->sub_appid))
                throw new PayException('小程序appid，小程序支付时使用的appid（若传入，则open_id需要保持一致）');
            if($payType == '010' && empty($this->open_id)){
                throw new PayException('用户标识（微信openid），需要传入，通过微信官方接口获得');
            }
        }

        $this->service_id = '015';
    }

    protected function buildData()
    {
        $this->certainSignData  = [
            // 基本数据
            'pay_ver'       => trim($this->pay_ver),
            'pay_type'      => trim($this->pay_type),
            'service_id'    => $this->service_id,
            'merchant_no'   => $this->merchant_no,
            'terminal_id'   => $this->terminal_id,
            'terminal_trace'  => $this->terminal_trace,
            'terminal_time' => $this->terminal_time,
            'total_fee'     => $this->total_fee,  // 金额，单位分

//           'time_expire'   => $this->timeout_express,

        ];

        $signData = array_merge($this->certainSignData,[]);

        if(isset($this->sub_appid)){
            $signData['sub_appid'] = $this->sub_appid;
        }
        if(isset($this->open_id)){
            $signData['open_id'] =  $this->open_id;
        }
        if(isset($this->order_body)){
            $signData['order_body'] = $this->order_body;
        }
        if(isset($this->notify_url)){
            $signData['notify_url'] =  $this->notify_url;
        }
        if(isset($this->attach)){
            $signData['attach'] =  $this->attach;
        }
        if(!empty($this->coupon_credential)){
            $signData['coupon_credential'] =  $this->coupon_credential;
        }
        if(!empty($this->goods_tag)){
            $signData['goods_tag'] =  $this->goods_tag;
        }
        if(!empty($this->goods_detail)){
            $signData['goods_detail'] =  $this->goods_detail;
        }
        if(!empty($this->food_order_type)){
            $signData['food_order_type'] =  $this->food_order_type;
        }

        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }
}
