<?php

namespace Payment\Common\Sw\Data;

use Payment\Common\BaseData;

/**
 * Class BaseData
 *
 * @property null|string signType
 * @package Payment\Common\Sw\Data
 */
abstract class SwBaseData extends BaseData
{

    /**
     * 签名算法实现
     * @param string $signStr
     * @return string
     */
    protected function makeSign($signStr)
    {
        switch ($this->signType) {
            case 'MD5':
                $signStr .= '&access_token=' . $this->access_token;

                $sign = md5($signStr);
                break;

            default:
                $sign = '';
        }

         $this->retData['key_sign'] = $sign;

         return $sign;
    }
}
