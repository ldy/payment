<?php


namespace Payment\Common\Sw\Data\Cancel;

use Payment\Common\Sw\Data\SwBaseData;
use Payment\Utils\ArrayUtil;

/**
 * Class CloseData
 * 扫呗关闭订单
 *
 *
 * @package Payment\Common\Sw\Data\Charge
 *
 */
class CloseData extends SwBaseData
{
    protected function checkDataParam()
    {

        $this->pay_ver = '100';
        $this->service_id = '041';
    }

    protected function buildData(){

        $this->certainSignData =  [
            // 基本数据
            'pay_ver'       => trim($this->pay_ver),
            'pay_type'      => trim($this->pay_type),//010微信,020支付宝，000自动识别类型
            'service_id'    => $this->service_id,
            'merchant_no'   => $this->merchant_no,
            'terminal_id'   => $this->terminal_id,
            'terminal_trace'  => $this->terminal_trace,//终端流水号（socket协议：长度为6位，Http协议：长度为32位）
            'terminal_time' => $this->terminal_time,

        ];

        $signData = array_merge($this->certainSignData,[]);

        if(isset($this->out_trade_no) && !empty($this->out_trade_no)){
            //利楚唯一订单号，优先使用订单号out_trade_no发起撤销，
            //在out_trade_no获取异常的情况，可使用当前支付请求的终端交易流水号pay_trace和终端交易时间pay_time发起撤销
            $signData['out_trade_no'] = $this->out_trade_no;
        }
        if(isset($this->pay_trace)){//当前支付终端流水号
            $signData['pay_trace'] =  $this->pay_trace;
        }
        if(isset($this->pay_time)){//当前支付终端交易时间
            $signData['pay_time'] = $this->pay_time;
        }

        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }
}
