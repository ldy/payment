<?php


namespace Payment\Common\Sw\Data\Query;

use Payment\Common\Sw\Data\SwBaseData;


/**
 * 查询交易的数据结构
 * Class BackChargeQueryData
 *
 *
 * @package Payment\Common\Sw\Data\Query
 */
class BackChargeQueryData extends SwBaseData
{
    protected function buildData()
    {
        $this->retData['package'] = [
            'trade_state' => $this->trade_state??'',//交易订单状态，SUCCESS支付成功，REFUND转入退款，NOTPAY未支付，CLOSED已关闭，USERPAYING用户支付中，REVOKED已撤销，NOPAY未支付支付超时，PAYERROR支付失败
            'channel_order_no' => $this->channel_order_no??'',//银行渠道订单号，微信支付时显示在支付成功页面的条码，可用作扫码查询和扫码退款时匹配
            'channel_trade_no' => $this->channel_trade_no??'',//通道订单号，微信订单号、支付宝订单号等，返回时不参与签名
            'user_id'   => $this->user_id,//付款方用户id，“微信openid”、“支付宝账户”、“qq号”等，返回时不参与签名
            'pay_trace' => $this->pay_trace,//当前支付终端流水号
            'pay_time' => $this->pay_time,//当前支付终端交易时间
            'total_fee' => $this->total_fee,
            'end_time' => $this->end_time??'',
        ];
        $this->retData['other'] = [
            'pay_type' => $this->pay_type,
            'merchant_name' => $this->merchant_name,
            'merchant_no' => $this->merchant_no,
            'terminal_id' => $this->terminal_id,
            'terminal_trace'=> $this->terminal_trace,
            'terminal_time' => $this->terminal_time,
            'total_fee' => $this->total_fee,
            'end_time' => $this->end_time??'',
            'out_trade_no' => $this->out_trade_no??'',//订单号
            'order_body' => $this->order_body??'',
            'attach'    => $this->attach??'',
        ];

        if($this->pay_type == '090'){//口碑实收金额，pay_type为090时必填
            $this->retData['package']['receipt_fee'] = $this->receipt_fee;
        }
    }

    protected function checkDataParam()
    {
        // 对于返回数据不做检查检查
    }

}