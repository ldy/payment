<?php

namespace Payment\Common\YSEpay;

use Payment\Common\BaseData;
use Payment\Common\BaseStrategy;
use Payment\Common\YSEConfig;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;
use Payment\Utils\Curl;

abstract class YSEBaseStrategy implements BaseStrategy
{


    protected $config;

    /**
     * 支付数据
     * @var BaseData $reqData
     */
    protected $reqData;

    /**
     * FuBaseStrategy constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        /* 设置内部字符编码为 UTF-8 */
        mb_internal_encoding("UTF-8");

        try {
            $this->config = new YSEConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 发送完了请求
     * @param $body
     * @return mixed
     * @throws \Payment\Common\PayException
     */
    protected function sendReq($body)
    {
        $url = $this->getReqUrl();
        if (is_null($url)) {
            throw new PayException('目前不支持该接口。请联系开发者添加');
        }
        $method = str_replace('.','_',$body['method']).'_response';

        $body['biz_content'] = urlencode($body['biz_content']);
        $body['sign'] = urlencode($body['sign']);

        $body = ArrayUtil::arraySort($body);
        $body = ArrayUtil::createLinkstring($body,false);

        $responseTxt = $this->curlPost($body, $url);
        // 格式化为数组
        $retData = (json_decode($responseTxt, true));
        if ($retData[$method]['code'] != '10000') {
            throw new PayException('支付平台返回错误提示:' . $retData[$method]['sub_msg']);
        }
        return $retData[$method];
    }

    protected function curlPost($body, $url)
    {
        $curl = new Curl();
        return $curl->set([
            'CURLOPT_HEADER' => 0
        ])->request($url,$body);
    }

    /**
     * 处理具体的业务
     *
     * @param array $data
     * @return mixed
     * @throws \Payment\Common\PayException
     */
    public function handle(array $data)
    {
        // TODO: Implement handle() method.
        $buildClass = $this->getBuildDataClass();
        try {
            $this->reqData = new $buildClass($this->config, $data);
        } catch (PayException $e) {
            throw $e;
        }

        $this->reqData->setSign();
        $body = $this->reqData->getData();

        $ret = $this->sendReq($body);

        // 检查返回的数据是否被篡改
        $flag = true;
        if (!$flag) {
            throw new PayException('返回数据被篡改。请检查网络是否安全！');
        }

        return $this->retData($ret);
    }

    /**
     * 获取请求地址
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        if (isset($this->config->base_url) && !empty($this->config->base_url)) {
            $_pre = $this->config->base_url;
        } else {
            $_pre = YSEConfig::REQUEST_URL;
        }
        return $_pre;
    }

    /**
     * 处理微信的返回值并返回给客户端
     * @param array $ret
     * @return mixed
     *
     */
    protected function retData(array $ret)
    {
        return $ret;
    }

    /**
     * 检查微信返回的数据是否被篡改过
     *
     * @param array $retData
     * @return boolean
     * @throws \Exception
     */
    protected function verifySign(array $retData)
    {
        $retSign = $retData['sign'];
        $values = ArrayUtil::removeKeys($retData, ['sign']);

        $values = ArrayUtil::paraFilter($values);

        $values = ArrayUtil::arraySort($values);

        $signStr = ArrayUtil::createLinkstring($values);

        $signStr = str_replace(' ', '', $signStr);

        $signStr .= '&key=' . $this->config->key;
        switch ($this->config->signType) {
            case 'MD5':
                $sign = md5($signStr);
                break;
            case 'HMAC-SHA256':
                $sign = hash_hmac('sha256', $signStr, $this->config->key);
                break;
            default:
                $sign = '';
        }
//        echo $retSign;echo"<br>";
//        echo strtoupper($sign);die;
        return strtoupper($sign) === $retSign;
    }

}
