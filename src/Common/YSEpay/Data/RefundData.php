<?php

namespace Payment\Common\YSEpay\Data;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class RefundData extends YSEBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
        $this->certainSignData = [
            'partner_id'    => $this->partner_id,
            'method'        => $this->method ?? 'ysepay.online.trade.refund',
            'charset'       => $this->charset ?? 'utf-8',
            'sign_type'     => $this->sign_type ?? 'RSA',
            'notify_url'    => $this->notify_url ?? '',
            'timestamp'     => $this->timestamp ?? date('Y-m-d H:i:s',time()),
            'version'       => $this->version ?? "3.0",
        ];
        $signData = array_merge($this->certainSignData, []);

        $biz_content = [
            'out_trade_no'  =>$this->out_trade_no,
            'shopdate'  =>$this->shopdate ?? date('Ymd'),
            'trade_no'  =>$this->trade_no ?? '',
            'refund_amount'  =>$this->refund_amount, //单位：元
            'refund_reason'     => $this->refund_reason,
            'out_request_no'   => $this->out_request_no
        ];
        $signData['biz_content'] = json_encode(ArrayUtil::paraFilter($biz_content));

        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
        parent::checkDataParam();
	}
}
