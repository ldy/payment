<?php

namespace Payment\Common\YSEpay\Data\Charge;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class PubChargeData extends ChargeBaseData{

    /**
     * 构建用于支付的签名相关数据
     *
     * @return void
     */
    protected function buildData(){
        // TODO: Implement buildData() method.
        $this->certainSignData = [
            'partner_id'    => $this->partner_id,
            'method'        => $this->method ?? 'ysepay.online.weixin.pay',
            'charset'       => $this->charset ?? 'utf-8',
            'sign_type'     => $this->sign_type ?? 'RSA',
            'notify_url'    => $this->notify_url,
            'timestamp'     => $this->timestamp ?? date('Y-m-d H:i:s',time()),
            'version'       => $this->version ?? "3.0",
        ];
        $signData = array_merge($this->certainSignData, []);

        $biz_content = [
            'out_trade_no'  =>$this->out_trade_no,
            'shopdate'  =>$this->shopdate ?? date('Ymd'),
            'subject'  =>$this->subject,
            'total_amount'  =>$this->total_amount,
            'seller_id'     => $this->seller_id,
            'seller_name'   => $this->seller_name,
            'timeout_express'   => $this->timeout_express,
            'business_code' => $this->business_code,
            'sub_openid' => $this->sub_openid,
            'appid' => $this->appid,
            'is_minipg' => $this->is_minipg ?? '2',   //小程序支付，值为 1，表示小程序支付；不传或值为 2，表示公众账号内支付
            'extra_common_param' => $this->extra_common_param ?? '',   //公用回传参数
        ];
        $signData['biz_content'] = json_encode(ArrayUtil::paraFilter(ArrayUtil::arraySort($biz_content)),JSON_UNESCAPED_UNICODE);

        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }

    protected function checkDataParam(){
        parent::checkDataParam(); // TODO: Change the autogenerated stub
        if(empty($this->notify_url)){
            throw new PayException('支付通知地址不能为空');
        }
        if(empty($this->sub_openid)){
            throw new PayException('用户OPENID 不能为空');
        }
        if(empty($this->appid)){
            throw new PayException('小程序appid 不能为空');
        }
    }
}
