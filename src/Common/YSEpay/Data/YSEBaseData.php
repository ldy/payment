<?php

namespace Payment\Common\YSEpay\Data;

use Payment\Common\BaseData;
use Payment\Common\PayException;

abstract class YSEBaseData extends BaseData{

    //测试
    protected $privateKey = "MIIEpAIBAAKCAQEAtuZdH/xV/r1fhTz1znGYRrYXorZMJH60QUNFY4j050i6nBjagqfjg/DRSj57IT7+nPTf9cqP7+NMS694OlIhI9YiJ2+JcvfpJSDsXk7AyEO3KbvjYPZWKU7d7i6pNGNPXW73EGdEuyD3/DMV1GZZpKIlZN6/O3SiPSepAngyklia+69xBXGugqoWiEsT8UbtJ4YmlUB5rTH9pxCBRi91dZ+4X6aNlcD0gTNWsC4tutpTSkDPl2MkmdKU5yt5KdZJT1hlh/orIfCrlRYrpGrO9eS400zrLv/VH37vGOVKpMSMK44BULAIOJjEzYCu8sKzxz4wqebnAHWFfOFeIOR+iQIDAQABAoIBAHJYGqhAjVKw+eLGdbYW9mhetvAWoAvA2thiQiM0IEfoBCJKt3EvnOGD0JRl83yZcXyW6t0zQkFrDOvUS4nKhsVsvfWBmaCytKNtrdhXy9Zn3z7MJB8eYkd48u5RZZ9G5vs2wGcU6LdEqUSWe3NKeYifhllFmLCG/lbVUKgmlRdAFxv3esFooEso1uSGwgB7f0HJLH10jT1kSUyfAvG35mJ8oYBuN0Hrbgyepo5H7qh3l7YSOAB9yHtTe5GMUPFoexuTGjdun85ydX7YQOO6QjDYTrVfLIQvoGGP/z0IXr13W+jpBpsN3ctvtCoCJgWxKjYk2CRbxdCvE118HtReyPECgYEA3koVhF9cXksqyf6QMb+0yRvKK8Czq4EXjzwVFAX2iZ6OUdLcT73PcuQvVJW+8U0i3awoQPxBye+IoNtWc9UV6vYJ+ELhNIv49evwwc4P7WnRTmC4W8HDvoGuwCM/Xd0ci4fd0ziWSGlAiSheNUfUGWUAyKc/Ksx0/HrmrySwgdcCgYEA0qMQeQCJHmhgOGKn+Sqp3Eip675GaL1msTFSdDghxwLPseVw2D9YpronGmPsJMINiHoE+Y7F0yxGqUL93kgfCZ+Sa7RcEsVW7pxjkmaLB6qmuba/osZoXzARaMlDeu1B0uawDuC2v3oV1HbvCka90WWeqdvKyUvjt+P3W4gTtp8CgYB1jpJwjgoIaBI5/yF83dibHtI9b0zVn5C5FFpFW531S+AST8Rpe4VRSnHpdsQjUtrkhp5HN+EHDWGhSD4hRgiwWOwO2jfFlVzVmbCangJ4WSTzIV1+USvW/ThQrpNdbYkiFXlvshZwNxGFN88SuGjoAS138FMMrFIWEF0dNPcPwwKBgQDO1bS9PRtoMpIZjDbpY018tBW3GhbFjwaiBI609NGmPEp6o5TVpA6HWdBAB9LAnZTwaPEtpVb4cEfKua/Ub/maV4lOHcMz43fjwsm73tmy443oW11+l1mx7t+Jh+itcwDp+gDz+xE4oMeC5G8CJkkKr8DHhr5wQAcScdZZmRvSfQKBgQClcdT5nTzxj3W45YWP9GEeZtUkE4VnT6ZHU5KInbZhNEbxNcEg69Ve6pviadyTjSBetB/Z2OPQPeE7LIlYHmPLjDHCPDqb6D++/6zL8qxlORf2oScKaEQFLnkqR+M2uZxe/D+HABHxAbbOTdJYK6kiokhPLEMAGc//dW6e2a+EqA==";
    protected $pubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtuZdH/xV/r1fhTz1znGYRrYXorZMJH60QUNFY4j050i6nBjagqfjg/DRSj57IT7+nPTf9cqP7+NMS694OlIhI9YiJ2+JcvfpJSDsXk7AyEO3KbvjYPZWKU7d7i6pNGNPXW73EGdEuyD3/DMV1GZZpKIlZN6/O3SiPSepAngyklia+69xBXGugqoWiEsT8UbtJ4YmlUB5rTH9pxCBRi91dZ+4X6aNlcD0gTNWsC4tutpTSkDPl2MkmdKU5yt5KdZJT1hlh/orIfCrlRYrpGrO9eS400zrLv/VH37vGOVKpMSMK44BULAIOJjEzYCu8sKzxz4wqebnAHWFfOFeIOR+iQIDAQAB";

    /**
     * 签名算法实现  便于后期扩展微信不同的加密方式
     *
     * @param string $signStr
     * @return string
     */
    protected function makeSign($signStr){
        switch ($this->signType) {
            case 'MD5':
                $sign = strtoupper(md5($signStr));
                break;
            case 'RSA':
                $sign = $this->genSign($signStr,$this->privateKey);
                break;
            default:
                $sign = '';
        }
        $this->retData['sign'] = $sign;
        return $sign;
        // TODO: Implement makeSign() method.
    }

    //生成 sha1WithRSA 签名
    protected function genSign($toSign, $privateKey){
        $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n" .
            wordwrap($privateKey, 64, "\n", true) .
            "\n-----END RSA PRIVATE KEY-----";

        $key = openssl_get_privatekey($privateKey);
        openssl_sign($toSign, $signature, $key);
        openssl_free_key($key);
        $sign = base64_encode($signature);
        return $sign;
    }

    //校验 sha1WithRSA 签名
    protected function verifySign($data, $sign, $pubKey){
        $sign = base64_decode($sign);

        $pubKey = "-----BEGIN PUBLIC KEY-----\n" .
            wordwrap($pubKey, 64, "\n", true) .
            "\n-----END PUBLIC KEY-----";

        $key = openssl_pkey_get_public($pubKey);
        $result = openssl_verify($data, $sign, $key, OPENSSL_ALGO_SHA1) === 1;
        return $result;
    }

    protected function checkDataParam(){
        // TODO: Implement checkDataParam() method.
        // 检查订单号是否合法
        if (empty($this->out_trade_no) || mb_strlen($this->out_trade_no) > 64) {
            throw new PayException('商户系统的订单号不能为空且长度不能超过64位');
        }

        if (empty($this->partner_id) || empty($this->business_code)) {
            throw new PayException('必须提供商户号与业务代码');
        }
    }
}
