<?php

namespace Payment\Common\YSEpay\Data\Cancel;

use Payment\Common\YSEpay\Data\YSEBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class CancelData extends YSEBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
        $this->certainSignData = [
            'channelid'    => $this->channelid,               //渠道编号
            'opt'          => $this->opt ?? "cancel",       //操作类型（“cancel”）
            'merid'        => $this->merid,                   //商戶编号
            'termid'       => $this->termid,                  //终端编号
            'tradetrace'   => $this->tradetrace,              //原交易流水（建议使用易生流水号查询：wtorderid）
//            'oritradetrace'   => $this->oritradetrace,        //原交易流水； 与 oriwtorderid 字段可以二选一上送，如果都上送，以 本节点为准。
//            'oriwtorderid'   => $this->oriwtorderid,          //原交易的系统订单号；与 oritradetrace 字段可以二选一上送，如果都 上送，以 oritradetrace 节点为准
        ];

        $signData = array_merge($this->certainSignData, []);

        if(isset($this->version)){
            $signData['version'] = $this->version;       //填写的值为 2 时，交易成功后返回优惠信息
        }

        if(isset($this->oritradetrace)){
            $signData['oritradetrace'] = $this->oritradetrace;       //退货金额,以分为单位，不大于原交易金额与已成功退货金额之差
        }else{
            if(isset($this->oriwtorderid)){
                $signData['oriwtorderid'] = $this->oriwtorderid;       //退货金额,以分为单位，不大于原交易金额与已成功退货金额之差
            }
        }
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		parent::checkDataParam();
	}
}
