<?php

namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class LTFConfig extends ConfigInterface{

	public const UNIFIED_URL = 'wxPreCreate';  //公众号/服务窗统一下单

	public const SCANPAY_URL = 'precreate'; //主扫统一下单，扫码支付（预支付）

	public const MICROPAY_URL = 'pay '; //条码支付，商户扫用户二维码收款

	public const QUERY_URL = 'pay/query'; //订单查询

	public const REFUND_URL = 'refund'; //退款申请

	public const CLOSE_URL = 'close'; //关闭订单（只有未支付的订单才能发起关闭。订单生成后不能马上调用关单接口，最短调用时间间隔为5分钟）

	public const CANCEL_URL = 'cancel'; //订单撤销

	public const CHARGE_QUERY_URL = 'pay/query'; //订单查询

	public const REFUND_QUERY_URL = 'refund/query'; //退款查询

	public const BASE_URL = 'https://api.liantuofu.com/open/';

	public const PAY_URL='jspay';//聚合支付

	public $base_url = 'https://api.liantuofu.com/open/';

	public $version = '1.0';

	public $signType = 'MD5';

	public $appId; //合作方标识

	public $merchantCode; //门店编号

	public $payChannel="WXPAY";//支付渠道

	public $subAppId; //子商户公众号id，微信交易为商户的appid（小程序，公众号必填）

	public $randomStr; //随机字符串

	public $attach; //附加数据

	public $startTime; //交易开始时间（秒级时间戳）

	public $certainSignData = []; //专门用于签名的数据

	public $privateKey; //私钥

	public $key; //私钥

	/**
	 * FuConfig constructor.
	 * @param array $config
	 * @throws PayException
	 */
	public function __construct(array $config)
	{
		try {
			$this->initConfig($config);
		} catch (PayException $e) {
			throw $e;
		}
	}

	/**
	 * 初始化配置文件参数
	 * @param array $config
	 * @throws PayException
	 */

	/**
	 * 初始化配置文件参数
	 * @param array $config
	 * @throws PayException
	 */
	private function initConfig(array $config)
	{
		$config = ArrayUtil::paraFilter($config);

		if (key_exists('ltf_appid', $config) && !empty($config['ltf_appid'])) $this->appId = $config['ltf_appid'];else throw new PayException('必须提供合作方标识');

		if (key_exists('merchant_code', $config) && !empty($config['merchant_code'])) $this->merchantCode = $config['merchant_code']; else throw new PayException('必须提供门店编号');

		if (key_exists('sub_appid', $config) && !empty($config['sub_appid'])) $this->subAppId = $config['sub_appid'];

		if (key_exists('channel', $config) && !empty($config['channel'])) $this->payChannel = $config['channel'];

		if (key_exists('key', $config) && !empty($config['key'])) $this->key = $config['key'];


		if (key_exists('notify_url', $config) && !empty($config['notify_url'])) $this->notifyUrl = $config['notify_url'];


		$this->randomStr = StrUtil::getNonceStr(); //生成随机字符串


		$this->startTime = time();
		$this->order_type = $config['trade_type'] ?? ($config['order_type'] ?? '');
		$this->trade_type = $config['trade_type'] ?? 'MINIAPP';
	}
}
