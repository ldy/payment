<?php

namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class MiConfig extends ConfigInterface
{
    // 应用ID
    public $appId;

    // 平台分配的商户号
    public $sub_merchant_id;

    // 随机字符串，不长于32位
    public $nonce_str;

    public $trade_type;

    public $method;

    public $sign_type = 'RSA2';

    public $limit_pay;

    public $publicKeyPath;//公钥路径


    // 指定回调页面
    public $returnUrl;

    public $version = 'v1';//接口版本号


    const GATEWAY_URL = 'https://openapi.meepay.net/pay/gateway';

    // 统一下单url
    const UNIFIED_URL = 'https://openapi.meepay.net/pay/trade/wxpay_unifiedorder';

    // 撤销交易url
    const ORDER_CANCEL_URL = 'https://vsp.allinpay.com/apiweb/unitorder/cancel';


    // 申请退款url
    const REFUND_URL = 'https://vsp.allinpay.com/apiweb/unitorder/refund';

    // 支付查询url
    const CHARGE_QUERY_URL = 'https://vsp.allinpay.com/apiweb/unitorder/query';


    // 退款账户
    const REFUND_UNSETTLED = 'REFUND_SOURCE_UNSETTLED_FUNDS';// 未结算资金退款（默认使用未结算资金退款）
    const REFUND_RECHARGE = 'REFUND_SOURCE_RECHARGE_FUNDS';// 可用余额退款(限非当日交易订单的退款）



    /**
     * 初始化配置文件
     * TLConfig constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */
    private function initConfig(array $config)
    {
        $config = ArrayUtil::paraFilter($config);

        // 检查 分配的公众账号ID
        if (key_exists('app_id', $config) && !empty($config['app_id'])) {
            $this->appId = $config['app_id'];

        } else {
            //
            throw new PayException('必须提供支付平台分配的公众账号ID');
        }

        // 检查 method
//        if (key_exists('method', $config) && !empty($config['method'])) {
//            $this->method = $config['method'];
//        } else {
//            throw new PayException('必须提供method');
//        }

        // 检查 平台支付分配的商户号
        if (key_exists('sub_merchant_id', $config) && !empty($config['sub_merchant_id'])) {
            $this->sub_merchant_id = $config['sub_merchant_id'];
        } else {
            throw new PayException('必须提供支付平台分配的子商户号');
        }

        if (key_exists('trade_type', $config) && !empty($config['trade_type'])) {
            $this->trade_type = $config['trade_type'];
        } else {
            $this->trade_type  = 'MINIAPP'; // 默认是小程序支付
        }

        if (key_exists('return_raw', $config)) {
            $this->returnRaw = filter_var($config['return_raw'], FILTER_VALIDATE_BOOLEAN);
        }

        // 检查 异步通知的url
        if (key_exists('notify_url', $config) && !empty($config['notify_url'])) {
            $this->notifyUrl = trim($config['notify_url']);
        } else {
            throw new PayException('异步通知的url必须提供.');
        }

        // 设置禁止使用的支付方式
        if (key_exists('limit_pay', $config) && !empty($config['limit_pay']) && $config['limit_pay'][0] === 'no_credit') {
            $this->limitPay = $config['limit_pay'][0];

        }



        // 生成随机字符串
        $this->nonce_str = StrUtil::getNonceStr();
    }
}
