<?php

namespace Payment\Common\YSpay\Data;

use Payment\Common\BaseData;
use Payment\Common\PayException;

abstract class YSBaseData extends BaseData{

	/**
	 * 签名算法实现  便于后期扩展微信不同的加密方式
	 *
	 * @param string $signStr
	 * @return string
	 */
	protected function makeSign($signStr){
	    if(empty($this->key)) throw new PayException('请检查签名密钥是否正确');
		switch ($this->signType) {
			case 'MD5':
				$signStr .= '&key=' . $this->key;
				$sign = strtoupper(md5($signStr));
				break;
			default:
				$sign = '';
		}
		$this->retData['sign'] = $sign;
		return $sign;
		// TODO: Implement makeSign() method.
	}

	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		// 检查订单号是否合法
		if (empty($this->tradetrace) || mb_strlen($this->tradetrace) > 64) {
			throw new PayException('商户系统的订单号不能为空且长度不能超过64位');
		}

		if (empty($this->merid) || empty($this->channelid)) {
			throw new PayException('必须提供合作方标识与门店编号');
		}
	}
}
