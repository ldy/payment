<?php

namespace Payment\Common\YSpay\Data\Charge;

use Payment\Common\YSpay\Data\YSBaseData;
use Payment\Common\PayException;
use Payment\Config;

abstract class ChargeBaseData extends YSBaseData{

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws \Payment\Common\PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		// 检查金额不能低于0.01
		if ($this->tradeamt < 1) {
			throw new PayException('支付金额不能低于 ' . Config::PAY_MIN_FEE . ' 元');
		}
	}
}
