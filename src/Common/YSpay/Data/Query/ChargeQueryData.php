<?php

namespace Payment\Common\YSpay\Data\Query;

use Payment\Common\YSpay\Data\YSBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class ChargeQueryData extends YSBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
        $this->certainSignData = [
            'channelid'    => $this->channelid,               //渠道编号
            'opt'          => $this->opt ?? "tradeQuery",     //操作类型（“tradeQuery”）
            'merid'        => $this->merid,                   //商戶编号
            'termid'       => $this->termid,                  //终端编号
            'tradetrace'   => $this->tradetrace,              //原交易流水（建议使用易生流水号查询：wtorderid）
        ];

        $signData = array_merge($this->certainSignData, []);
        if(isset($this->tradetype)){
            $signData['tradetype'] = $this->tradetype;       //交易类型（“samecardQuery”：同名卡转出查询；其他查询为空）
        }
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		parent::checkDataParam();
		if(empty($this->tradetrace)){
			throw new PayException('第三方订单号不能为空');
		}
	}
}
