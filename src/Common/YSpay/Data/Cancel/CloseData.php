<?php

namespace Payment\Common\YSpay\Data\Cancel;

use Payment\Common\YSpay\Data\YSBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

class CloseData extends YSBaseData{

	/**
	 * 构建用于支付的签名相关数据
	 *
	 * @return array
	 */
	protected function buildData(){
		// TODO: Implement buildData() method.
        $this->certainSignData = [
            'channelid'    => $this->channelid,               //渠道编号
            'opt'          => $this->opt ?? "close",       //操作类型（“close”）
            'merid'        => $this->merid,                   //商戶编号
            'termid'       => $this->termid,                  //终端编号
            'tradetrace'   => $this->tradetrace,              //原交易流水（建议使用易生流水号查询：wtorderid）
        ];
		$signData = array_merge($this->certainSignData, []);
		$this->retData = ArrayUtil::paraFilter($signData);
	}

	/**
	 * 检查传入的参数. $reqData是否正确.
	 *
	 * @return mixed
	 * @throws PayException
	 */
	protected function checkDataParam(){
		// TODO: Implement checkDataParam() method.
		parent::checkDataParam();
	}
}
