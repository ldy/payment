<?php

namespace Payment\Common\Hsq;

use Payment\Common\BaseData;
use Payment\Common\BaseStrategy;
use Payment\Common\PayException;
use Payment\Common\HsqConfig;
use Payment\Utils\Curl;


abstract class HsqBaseStrategy implements BaseStrategy
{

    protected $config;

    /**
     * 支付数据
     * @var BaseData $reqData
     */
    protected $reqData;

    /**
     * FuBaseStrategy constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        /* 设置内部字符编码为 UTF-8 */
//        mb_internal_encoding("UTF-8");
        try {
            $this->config = new HsqConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 发送请求
     * @param array $body
     * @return mixed
     * @throws PayException
     */
    protected function sendReq($body)
    {
        $url = $this->getReqUrl();
        if (is_null($url)) {
            throw new PayException('目前不支持该接口。请联系开发者添加');
        }
        $body = http_build_query($body);
        if (empty($body)) throw new PayException('支付数据异常');
        $response = $this->curlPost($body, $url);
        if ($response['error']) throw new PayException('网络发生错误，请稍后再试curl返回码：' . $response['message']);
        $retData = json_decode($response['body'],true);
        $result = json_decode($retData['result'],true);
        if($result['orderStatus'] == 'FAIL') throw new PayException('支付平台返回错误提示:' . $result['respMsg']);

        return $retData;
    }

    /**
     * 请求
     * @param $body
     * @param $url
     * @return array
     */
    protected function curlPost($body, $url)
    {
        $curl = new Curl();
        $header = [
            'content-type: application/x-www-form-urlencoded'
        ];
        return $curl->set([
            'CURLOPT_HEADER' => 0,
            'CURLOPT_HTTPHEADER' => $header
        ])->post($body)->submit($url, false);
    }

    /**
     * 获取请求地址
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        return  $url ?? $this->config->base_url;
    }

    /**
     * @param array $data
     *
     * @throws PayException
     * @throws \Exception
     * @return array|string
     */
    public function handle(array $data)
    {
        $buildClass = $this->getBuildDataClass();
        $this->reqData = new $buildClass($this->config, $data);
        $this->reqData->setSign();
        $body = $this->reqData->getData();
        $ret = $this->sendReq($body);
        $flag = $this->verifySign($ret);
        if (!$flag) {
            throw new PayException('返回数据被篡改。请检查网络是否安全！');
        }
        return $this->retData($ret);
    }

    /**
     * 处理返回值并返回给客户端
     * @param array $ret
     * @return mixed
     *
     */
    protected function retData(array $ret)
    {
        return $ret;
    }

    /**
     * 签名验证
     * @param array $retData
     * @return bool
     * @throws PayException
     */
    protected function verifySign(array $retData)
    {
        if (empty($this->reqData->key)) throw new PayException('key error');
        if (empty($this->reqData->publicKey)) throw new PayException('Public key error');

        $key = $this->reqData->key;
        $publicKey = $this->reqData->publicKey;

        if(is_file($publicKey))
        {
            $publicKey = openssl_get_publickey(file_get_contents($publicKey));
        }
        else
        {
            $publicKey = $this->reqData->publicKey;
        }
        if (empty($publicKey)) throw new PayException('Public key resource identifier false');
        $retSign = $retData['sign'];

        $resSignStr = "result=".$retData['result']."&success=true"."&key=".$key;
        return openssl_verify($resSignStr,hex2bin($retSign), $publicKey, OPENSSL_ALGO_SHA256);

    }

}
