<?php

namespace Payment\Common\Hsq\Data;


/**
 * Class BackPubChargeData
 * @package Payment\Common\Hsq\Data
 */
class BackAppChargeData extends HsqBaseData
{

    public function getData()
    {
        if (!empty($this->result)) {
            $result = json_decode($this->result, true);
            if (!empty($result['qrCode'])) {
                $qrCode = json_decode($result['qrCode'], true);
                $this->retData['package'] = $qrCode;
            }
            $this->retData['other'] = [
                'channelOrderNo' => '',
                'tradeNo' => $result['tradeNo'] ?: '',//慧收钱交易订单号
                'transNo' => $result['transNo'] ?: '',//原样返回商户订单号
            ];
        }

        return parent::getData();
    }


    protected function buildData()
    {
    }

    protected function checkDataParam()
    {
    }

}
