<?php

namespace Payment\Common\Hsq\Data;

use Payment\Common\PayException;

/**
 * 退款
 * Class RefundData
 * @package Payment\Common\Hsq\Data
 */

class RefundData extends HsqBaseData
{

    protected function checkDataParam()
    {
        if (empty($this->transNo) || mb_strlen($this->transNo) > 32) throw new PayException('商户系统退款单号不能为空且长度不能超过30位');
        if (empty($this->origTransNo) || mb_strlen($this->origTransNo) > 32) throw new PayException('原商户系统订单号不能为空且长度不能超过30位');

        $totalFee = intval($this->origOrderAmt ?? 0); //订单总金额
        $refundFee = intval($this->orderAmt ?? 0); //退款总金额
        if (empty($totalFee) || empty($refundFee)) throw new PayException('订单总金额或退款金额有误');
        if (bccomp($refundFee, $totalFee) === 1) throw new PayException('退款金额不能大于订单总金额');
    }

    protected function buildData()
    {
        $signContent = [
            /* 业务请求参数 begin */
            'transNo' => $this->transNo, //商户订单号
            'refundType' => $this->refundType, //退款类型
            'origTransNo' => $this->origTransNo, //原商户订单号
            'origOrderAmt' => $this->origOrderAmt,//原订单金额
            'orderAmt' => intval($this->orderAmt), //退款金额,单位为：分
            'requestDate' => $this->requestDate, //请求时间
            'refundReason' => $this->refundReason, //退款原因
            'returnUrl' => ($this->returnUrl ?: '') , //后端通知地址, 用户支付完成后，慧收钱服务器主动通知商户服务器里指定地址
            /* 业务请求参数 end */
        ];
        $signData = [
            /* 公共请求参数 begin */
            'method' => $this->method,//方法名
            'version' => $this->version, //版本
            'format' => $this->format, //请求格式
            'merchantNo' => $this->merchantNo, //商户号
            'signType' => $this->signType, //加密类型
            'signContent' => json_encode($signContent,JSON_UNESCAPED_UNICODE),
            'key' => $this->key,
            /* 公共请求参数 end */
        ];
        $this->retData = $signData;
    }
}