<?php

namespace Payment\Common\Hsq\Data;

use Payment\Common\BaseData;
use Payment\Common\PayException;

/**
 * Class FuBaseData
 * @package Payment\Common\Hsq\Data
 */
abstract class HsqBaseData extends BaseData
{

    /**
     * 签名算法实现
     * @param string $signStr
     * @return string
     * @throws PayException
     */
    protected function makeSign($signStr)
    {
        return $this->sign($signStr);
    }

    /**
     * 签名
     * @param $signStr
     * @return string
     * @throws PayException
     */
    private function sign($signStr)
    {
        if (empty($this->privateKey)) throw new PayException('Private key does not exist');

        if(is_file($this->privateKey))
        {
            $privateKey = file_get_contents($this->privateKey);
            $private_key = array();
            openssl_pkcs12_read($privateKey, $private_key, $this->private_key_pass);
            $privateKey = $private_key['pkey'];
        }
        else
        {
            $privateKey =  $this->privateKey;
        }
        if (empty($privateKey)) throw new PayException('Private key error');
        openssl_sign($signStr, $sign, $privateKey, OPENSSL_ALGO_SHA256);
        return bin2hex($sign);

    }

}
