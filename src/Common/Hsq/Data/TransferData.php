<?php
/**
 *
 * @createTime: 2016-08-04 09:42
 * @description:
 */

namespace Payment\Common\Hsq\Data;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

/**
 * Class TransferData
 * @package Payment\Common\Hsq\Data
 *
 */
class TransferData extends HsqTransBaseData
{
    /**
     * 检查相关参数是否设置
     *
     */
    protected function checkDataParam()
    {
        $reqData = $this->data['reqData'];
        if (empty($reqData)) {
            throw new PayException('必须提供参数');
        }

        foreach($reqData as $key => $val){
            if (empty($val['trans_no'])) {
                throw new PayException('必须提供商户订单号');
            }
            // 微信使用的单位位分.此处进行转化
            if (empty($val['trans_money']) || $val['trans_money'] < 0){
                throw new PayException('必须提供转账金额');
            }
            if (empty($val['to_acc_name'])) {
                throw new PayException('必须提供收款人姓名');
            }
            if (empty($val['to_acc_no'])) {
                throw new PayException('必须提供收款人银行帐号');
            }
            if (empty($val['to_bank_name'])) {
                throw new PayException('必须提供收款人银行名称');
            }
//            if (empty($val['to_pro_name'])) {
//                throw new PayException('必须提供收款人开户行省名');
//            }
//            if (empty($val['to_city_name'])) {
//                throw new PayException('必须提供收款人开户行市名');
//            }
//            if (empty($val['to_acc_dept'])) {
//                throw new PayException('必须提供收款人开户行机构名');
//            }
//            if (empty($val['trans_card_id'])) {
//                throw new PayException('必须提供银行卡身份证件号码');
//            }
        }

    }

    protected function buildData()
    {
        $reqData = $this->data['reqData'];
        $data = ['trans_content' => ['trans_reqDatas' => [['trans_reqData' => $reqData]]]];
        $data_content = str_replace("\\\"",'"',json_encode($data,JSON_UNESCAPED_UNICODE));
        $this->retData = [
            'member_id' => $this->member_id, //商户号
            'terminal_id' => $this->terminal_id, //终端号
            'data_type' => $this->data_type, //数据类型
            'version' => $this->version, //版本号
            //请求报文
            'data_content' => $data_content,
        ];
        $this->retData = ArrayUtil::paraFilter($this->retData);
    }

}
