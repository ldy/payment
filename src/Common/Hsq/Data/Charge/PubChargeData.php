<?php

namespace Payment\Common\Hsq\Data\Charge;

use Payment\Common\PayException;

/**
 * Class PubChargeData
 * @package Payment\Common\Hsq\Data\Charge
 */
class PubChargeData extends ChargeBaseData
{

    /**
     * checkDataParam
     * @return mixed|void
     * @throws PayException
     */
    protected function checkDataParam()
    {
        parent::checkDataParam();

        $tradeType = $this->payType;
        if (empty($tradeType)) throw new PayException('订单类型有误(支持WECHAT_APPLET、ALI_APPLET)');

        $openid = $this->openid;
        if (empty($openid)) throw new PayException('未获取到用户openid');

        $spbillCreateIp = $this->spbillCreateIp;
        if (empty($spbillCreateIp)) {
            $this->spbillCreateIp = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
        }
    }

    /**
     * 构建用于支付的签名相关数据
     * @return array|void
     */
    protected function buildData()
    {
        $memo = [
            /* Memo参数域 begin */
            'openid' => $this->openid, //用户在商户appid下的唯一标识。下单前需获取到用户的Openid。
            'appid' => $this->subAppid, //微信公众号APPID
            'spbillCreateIp' => $this->spbillCreateIp ?? '127.0.0.1', //终端用户IP
            'longitude' => $this->longitude, //经度
            'latitude' => $this->latitude, //纬度
            /* Memo参数域 end */
        ];
        $signContent = [
            /* 业务请求参数 begin */
            'transNo' => $this->transNo, //商户订单号
            'payType' => $this->payType, //支付类型
            'orderAmt' => intval($this->orderAmt), //交易金额, 交易金额，单位为：分
            'goodsInfo' => $this->goodsInfo, //商品信息
            'returnUrl' => $this->returnUrl, //后端通知地址, 用户支付完成后，慧收钱服务器主动通知商户服务器里指定地址
            'requestDate' => $this->requestDate, //请求时间
            'memo' => json_encode($memo, JSON_UNESCAPED_UNICODE),
            /* 业务请求参数 end */
        ];
        if (!empty($this->subMerchantNo)) {
            $signContent['subMerchantNo'] = $this->subMerchantNo; //门店编号
        }

        $signData = [
            /* 公共请求参数 begin */
            'method' => $this->method,//方法名
            'version' => $this->version, //版本
            'format' => $this->format, //请求格式
            'merchantNo' => $this->merchantNo, //商户号
            'signType' => $this->signType, //加密类型
            'signContent' => json_encode($signContent, JSON_UNESCAPED_UNICODE),
            'key' => $this->key,
            /* 公共请求参数 end */
        ];
        $this->retData = $signData;
    }

}
