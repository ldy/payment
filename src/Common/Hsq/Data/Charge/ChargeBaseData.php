<?php


namespace Payment\Common\Hsq\Data\Charge;

use Payment\Common\Hsq\Data\HsqBaseData;
use Payment\Common\PayException;
use Payment\Config;

/**
 * Class ChargeBaseData
 * @package Payment\Common\Hsq\Data\Charge
 */
abstract class ChargeBaseData extends HsqBaseData
{

    /**
     * 检查传入的支付信息是否正确
     */
    protected function checkDataParam()
    {
        // 检查订单号是否合法
        if (empty($this->transNo) || mb_strlen($this->transNo) > 64) throw new PayException('商户系统订单号不能为空且长度不能超过64位');

        // 检查金额不能低于0.01
        if ($this->orderAmt < bcmul(Config::PAY_MIN_FEE, 100)) throw new PayException('支付金额不能低于 ' . Config::PAY_MIN_FEE . ' 元');

        // 检查商品名称
        if (empty($this->goodsInfo)) throw new PayException('必须提供商品说明');

    }
}
