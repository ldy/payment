<?php

namespace Payment\Common\Hsq\Data\Charge;

use Payment\Common\PayException;

/**
 * Class JSChargeData
 * @package Payment\Common\Hsq\Data\Charge
 */
class JSChargeData extends ChargeBaseData
{
    /**
     * checkDataParam
     * @return mixed|void
     * @throws PayException
     */
    protected function checkDataParam()
    {
        parent::checkDataParam();

        $tradeType = $this->payType;
        if (empty($tradeType)) throw new PayException('订单类型有误(WECHAT_JSAPI、ALI_JSAPI)');

        $spbillCreateIp = $this->spbillCreateIp;
        if (empty($spbillCreateIp)) {
            $this->spbillCreateIp = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
        }
    }

    /**
     * 构建用于支付的签名相关数据
     * @return array|void
     */
    protected function buildData()
    {
        $memo = [
            'spbillCreateIp' => $this->spbillCreateIp ?? '127.0.0.1', //终端用户IP
            'longitude' => $this->longitude, //经度
            'latitude' => $this->latitude, //纬度
            'openid' => $this->openid, //纬度
            'appid' => $this->appid, //纬度
        ];
        $signContent = [
            'transNo' => $this->transNo, //商户订单号
            'payType' => $this->payType, //支付类型
            'orderAmt' => intval($this->orderAmt), //交易金额, 交易金额，单位为：分
            'goodsInfo' => $this->goodsInfo, //商品信息
            'returnUrl' => $this->returnUrl , //后端通知地址, 用户支付完成后，慧收钱服务器主动通知商户服务器里指定地址
            'requestDate' => $this->requestDate, //请求时间
            'memo' => json_encode($memo,JSON_UNESCAPED_UNICODE),
        ];

        $signData = [
            'method' => $this->method,//方法名
            'version' => $this->version, //版本
            'format' => $this->format, //请求格式
            'merchantNo' => $this->merchantNo, //商户号
            'signType' => $this->signType, //加密类型
            'signContent' => json_encode($signContent,JSON_UNESCAPED_UNICODE),
            'key' => $this->key,
        ];
        $this->retData = $signData;
    }

}
