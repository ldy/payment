<?php

namespace Payment\Common\Hsq\Data;

use Payment\Common\BaseData;
use Payment\Common\PayException;
use Payment\Utils\BaoFooRsaUtil;

/**
 * Class FuBaseData
 * @package Payment\Common\Hsq\Data
 */
abstract class HsqTransBaseData extends BaseData
{

    /**
     * 签名算法实现
     * @param string $signStr
     * @return string
     * @throws PayException
     */
    protected function makeSign($signStr)
    {
        return $this->sign($signStr);
    }

    /**
     * 签名
     * @param $signStr
     * @return string
     * @throws PayException
     */
    private function sign($signStr)
    {
        // 私钥加密
        $encrypted = BaoFooRsaUtil::encryptByPFXFile($signStr, $this->privateKey, $this->private_key_pass);
        return $encrypted;

    }

}
