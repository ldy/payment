<?php

namespace Payment\Common\Hsq\Data;

use Payment\Common\PayException;

/**
 * Class BackPubChargeData
 * @package Payment\Common\Hsq\Data
 */
class BackPubChargeData extends HsqBaseData
{

    public function getData()
    {
        $data = [];
        if(!empty($this->result))
        {
            $result = json_decode($this->result,true);

            if(!empty($result['qrCode']))
            {
                switch ($result['payType'])
                {
                    case 'WECHAT_APPLET': //微信
                        $qrCode = json_decode($result['qrCode'],true);
                        $data = $qrCode;
                        break;
                    case 'ALI_APPLET': //支付宝
                        $qrCode = $result['qrCode'];
                        $data = $qrCode;
                        break;
                    default:
                }
            }
        }

        $this->retData['package'] = $data;
        $this->retData['other'] = [
            'channelOrderNo' => $result['channelOrderNo'] ?: '',
            'tradeNo' => $result['tradeNo'] ?: '',
            'transNo' => $result['transNo'] ?: '',
        ];

        return parent::getData();
    }


    protected function buildData()
    {
    }

    protected function checkDataParam()
    {
    }

}
