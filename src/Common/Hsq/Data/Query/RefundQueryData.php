<?php

namespace Payment\Common\Hsq\Data\Query;

use Payment\Common\PayException;
use Payment\Common\Hsq\Data\HsqBaseData;

/**
 * 退款查询（支持3日内所有退款交易查询）
 *
 * Class ChargeQueryData
 *
 * @property string $order_no 商户系统订单号
 * @property string $refund_no 商户系统退款单号
 * @property string $order_type 订单类型：ALIPAY(统一下单、条码支付、服务窗支付)，WECHAT(统一下单、条码支付、公众号支付)，UNIONPAY，BESTPAY(翼支付)
 *
 * @package Payment\Common\Hsq\Data\Query
 */
class RefundQueryData extends HsqBaseData
{

    protected function checkDataParam()
    {
        if (empty($this->transNo) || mb_strlen($this->transNo) > 30) throw new PayException('商户系统订单号不能为空且长度不能超过30位');

//        if (empty($this->refund_no) || mb_strlen($this->refund_no) > 30) throw new PayException('商户系统退款单号不能为空且长度不能超过30位');
//        $tradeType = $this->order_type = $this->trade_type ?? ($this->order_type ?? '');
//        if (empty($tradeType)) throw new PayException('订单类型有误(支持WECHAT、ALIPAY、UNIONPAY、BESTPAY)');
    }

    protected function buildData()
    {
        $signData = [
            'version' => $this->version,
            'ins_cd' => $this->institutionId, //机构号，接入机构在富友的唯一代码
            'mchnt_cd' => $this->merchantId, //商户号, 富友分配给二级商户的商户号
            'term_id' => $this->terminalId ?? '88888888', //终端号(没有真实终端号统一填88888888)
            'random_str' => $this->randomStr, //随机字符串
            'refund_order_no' => $this->refund_no, //商户退款单号, 商户系统内部的退款单号（5到30个字符、只能包含字母数字，区分大小写)
//            'order_type' => $this->order_type, //订单类型：ALIPAY(统一下单、条码支付、服务窗支付)，WECHAT(统一下单、条码支付、公众号支付)，UNIONPAY，BESTPAY(翼支付)
        ];

        $this->retData = $signData;
    }

}