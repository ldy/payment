<?php

namespace Payment\Common\Hsq\Data\Query;

use Payment\Common\PayException;
use Payment\Common\Hsq\Data\HsqBaseData;

/**
 * 交易查询（支持3日内所有交易查询）
 *
 * Class ChargeQueryData
 *
 * @property string $order_no 商户系统订单号
 * @property string $order_type 订单类型：ALIPAY(统一下单、条码支付、服务窗支付)，WECHAT(统一下单、条码支付、公众号支付)，UNIONPAY，BESTPAY(翼支付)
 *
 * @package Payment\Common\Hsq\Data\Query
 */
class ChargeQueryData extends HsqBaseData
{

    protected function checkDataParam()
    {
        if (empty($this->transNo) || mb_strlen($this->transNo) > 30) throw new PayException('商户订单号不能为空且长度不能超过30位');

    }

    protected function buildData()
    {
        $signContent = [
            /* 业务请求参数 begin */
            'transNo' => $this->transNo, //商户订单号
            'queryType' => $this->queryType, //查询类型
            /* 业务请求参数 end */
        ];

        $signData = [
            /* 公共请求参数 begin */
            'method' => $this->method,//方法名
            'version' => $this->version, //版本
            'format' => $this->format, //请求格式
            'merchantNo' => $this->merchantNo, //商户号
            'signType' => $this->signType, //加密类型
            'signContent' => json_encode($signContent,JSON_UNESCAPED_UNICODE),
            'key' => $this->key,
            /* 公共请求参数 end */
        ];
        $this->retData = $signData;
    }

}