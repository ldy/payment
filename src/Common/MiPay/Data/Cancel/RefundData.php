<?php

namespace Payment\Common\MiPay\Data\Cancel;

use Payment\Common\PayException;
use Payment\Common\MiPay\Data\MiBaseData;
use Payment\Utils\ArrayUtil;

/**
 * 用户退款
 * Class RefundData
 *
 * @package Payment\Common\MiPay\Data\Cancel
 * @author yeran
 */
class RefundData extends MiBaseData
{
    /**
     * 退款数据封装
     */
    protected function buildData()
    {
        $this->retData = [
            'appid' => $this->appId,
            'method' => 'trade.refund',
            'nonce_str' => $this->nonce_str,
            'version' => $this->version,
            'sign_type' => $this->sign_type,
            'out_refund_sn' => $this->out_refund_sn,
            'out_trade_sn' => $this->out_trade_sn,
            'meepay_trade_no' => $this->meepay_trade_no,
            'refund_amount' => $this->refund_amount,
//            'trxamt' => $this->trxamt,// 退款金额,分
//            'reqsn'  => $this->reqsn,// 商户退款单号
//            'oldreqsn' => $this->oldreqsn,//原交易的商户订单号
//            'oldtrxid'    => $this->oldtrxid,//原交易的收银宝平台流水
//            'remark' => $this->remark,// 备注
        ];

        $this->retData = ArrayUtil::paraFilter($this->retData);
    }

    /**
     * 检查参数
     * @author yeran
     */
    protected function checkDataParam()
    {
        $reqsn = $this->out_refund_sn;// 商户退款单号
        $oldreqsn = $this->out_trade_sn;
        $oldtrxid = $this->meepay_trade_no;
      //  $trxamt = $this->trxamt;


        if (empty($reqsn)) {
            throw new PayException('请设置退款单号 refund_no');
        }

        // 二者不能同时为空
        if (empty($oldreqsn) && empty($oldtrxid)) {
            throw new PayException('必须提供02m支付交易号或平台唯一订单号。');
        }

//        if ($trxamt<1) {
//            throw new PayException('退款金额异常');
//        }

    }
}
