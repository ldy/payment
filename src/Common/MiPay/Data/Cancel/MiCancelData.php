<?php

namespace Payment\Common\MiPay\Data\Cancel;

use Payment\Common\MiPay\Data\MiBaseData;
use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

/**
 * Class MiCancelData
 * 通联支付
 *
 * @package Payment\Common\MiPay\Data\Charge
 * @author yeran
 */
class MiCancelData extends MiBaseData
{
    protected function checkDataParam()
    {
        $reqsn = $this->out_refund_sn;// 商户退款单号
        $oldreqsn = $this->out_trade_sn;//服务商自定义单号
        $oldtrxid = $this->meepay_trade_no; //米付订单号
        if ((empty($oldreqsn) && empty($oldtrxid)) || empty($reqsn)) {
            throw new PayException('单号数据缺失');
        }
    }

    /**
     * 组装数据，用于计算sign，以及与支付平台对接
     *
     * 交易撤销数据
     *
     */
    protected function buildData()
    {
        $signData = [
            // 基本数据
            'appid' => $this->appId,
            'method' => 'trade.reverse',
            'nonce_str' => $this->nonce_str,
            'version' => $this->version,
            'sign_type' => $this->sign_type,
            'out_refund_sn' => $this->out_refund_sn,
            'out_trade_sn' => $this->out_trade_sn,
            'meepay_trade_no' => $this->meepay_trade_no,
            'refund_amount' => $this->refund_amount,
        ];
        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }

    /**
     * 处理支付平台的返回值并返回给客户端
     * @param array $ret
     * @return mixed
     * @author yeran
     */
    protected function retData(array $ret){

        return $ret;
    }
}
