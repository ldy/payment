<?php

namespace Payment\Common\MiPay\Data;


class BackMiChargeData extends MiBaseData
{
    protected function buildData()
    {
        $this->retData = [
            'appId' => $this->appid,
            'timeStamp' => time() . '',
            'nonceStr'  => $this->randomstr,
            'package'   => 'prepay_id=' . $this->chnltrxid,
            'signType'  => 'MD5',// 签名算法，暂支持MD5
        ];


    }

    protected function checkDataParam()
    {
        // 不进行检查
    }
}
