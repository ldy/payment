<?php

namespace Payment\Common\MiPay\Data\Charge;

use Payment\Common\PayException;
use Payment\Common\MiPay\Data\MiBaseData;
use Payment\Config;

/**
 * Class ChargeBaseData
 *
 * @inheritdoc
 *
 * @property string $reqsn
 * @property string $trxamt
 * @property string $client_ip  用户端实际ip
 * @property string $body
 * @property string $return_param  附加数据，在查询API和支付通知中原样返回
 * @property integer $validtime  订单有效时间，默认为5分钟，最长60分钟
 *
 * @package Payment\Common\TLpay\Data\Charge
 */
abstract class ChargeBaseData extends MiBaseData
{

    /**
     * 检查传入的支付信息是否正确
     */
    protected function checkDataParam()
    {
        $out_trade_sn = $this->out_trade_sn;
        $trxamt = $this->total_amount;//分
        $body = $this->body;

        // 检查订单号是否合法
        if (empty($out_trade_sn) || mb_strlen($out_trade_sn) > 64) {
            throw new PayException('订单号不能为空，并且长度不能超过64位');
        }

        // 检查金额不能低于0.01
        if (bccomp($trxamt, 100 * Config::PAY_MIN_FEE, 2) === -1) {
            throw new PayException('支付金额不能低于 ' . Config::PAY_MIN_FEE . ' 元');
        }

        // 检查 商品名称 与 商品描述
        if (empty($body)) {
            throw new PayException('必须提供订单商品名称');
        }

        // 订单有效时间，以分为单位，不填默认为5分钟，最大60分钟
//        if(!$this->validtime)
//            $this->validtime = 5;
//
//        if ($this->validtime >60) {
//            throw new PayException('订单有效时间最大60分钟');
//        }

        // 设置ip地址
        $clientIp = $this->spbill_create_ip;
        if (empty($clientIp)) {
            $this->spbill_create_ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1';
        }

    }
}
