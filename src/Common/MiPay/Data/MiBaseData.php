<?php

namespace Payment\Common\MiPay\Data;

use Payment\Common\BaseData;
use Payment\Utils\StrUtil;

/**
 * Class BaseData
 *
 *
 * @package Payment\Common\MiPay\Data
 */
abstract class MiBaseData extends BaseData
{

    /**
     * 签名算法实现  通联支付sign生成算法：sign = md5(string.getbyte("utf-8")).toUpperCase();
     * @param string $signStr
     * @return string
     */
    protected function makeSign($signStr){

        return strtoupper(md5(StrUtil::getBytes($signStr)));
    }
}
