<?php

namespace Payment\Common\MiPay\Data\Query;


use Payment\Common\PayException;
use Payment\Common\MiPay\Data\MiBaseData;
use Payment\Utils\ArrayUtil;

/**
 * 查询交易的数据结构
 * Class TLQueryData
 *
 *
 * @package Payment\Common\MiPay\Data\Query
 */
class MiQueryData extends MiBaseData
{

    protected function buildData()
    {
        $this->retData = [
            'appid' => $this->appId,
            'cusid'    => $this->cusid,
            'version'    => $this->version,
            'randomstr' => $this->randomstr,
            'reqsn' => $this->reqsn,//商户的交易订单号
            'trxid'  => $this->trxid,//支付的收银宝平台流水
        ];


        $this->retData = ArrayUtil::paraFilter($this->retData);
    }

    protected function checkDataParam()
    {
        $trxid = $this->trxid;// 支付的收银宝平台流水
        $reqsn = $this->reqsn;// 商户订单号，查询效率低，不建议使用

        // 二者不能同时为空
        if (empty($trxid) && empty($reqsn)) {
            throw new PayException('必须提供通联支付交易号或商户网站唯一订单号。建议使用通联支付交易号');
        }
    }
}