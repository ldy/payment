<?php


namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class HsqConfig extends ConfigInterface
{
    public const UNIFIED_URL = 'POLYMERIZE_MAIN_SWEPTN';  //小程序下单

    public const APP_ORDER_URL = 'POLYMERIZE_PROGRAM_CASHIER';  //APP下单

    public const JS_ORDER_URL = 'POLYMERIZE_MAIN_SWEPTN';  //JS下单

    public const QUERY_URL = 'POLYMERIZE_QUERY'; //订单查询

    public const REFUND_URL = 'POLYMERIZE_REFUND'; //退款申请

    public const REFUND_QUERY_URL = 'POLYMERIZE_REFUND_QUERY'; //退款查询

    public const QUERY_FILE_URL = 'QUERY_TRADE_FILE'; //对账文件下载API

    public $base_url = 'https://api.huishouqian.com/api/acquiring';

    public $method; //方法名

    public $version = '1.0'; //版本

    public $format = 'json'; //请求格式

    public $merchantNo; //商户号

    public $signType = 'RSA2';

    public $payType = 'WECHAT_APPLET'; //交易类型

    public $subMerchantNo;

    public $returnUrl; //异步回调地址

    public $requestDate; // 交易时间 格式为 yyyyMMddHHmmss

    public $privateKey; //私钥

    public $private_key_pass; //私钥密码

    public $publicKey; //公钥

    public $subAppid; //子商户公众号id，微信交易为商户的appid（小程序，公众号必填）

    public $key;



    /**
     * FuConfig constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */
    private function initConfig(array $config)
    {
        $config = ArrayUtil::paraFilter($config);

        if (key_exists('key', $config) && !empty($config['key'])) {
            $this->key = $config['key'];
        } else {
            throw new PayException('必须提供key');
        }

        if (key_exists('merchantNo', $config) && !empty($config['merchantNo'])) {
            $this->merchantNo = $config['merchantNo'];
        } else {
            throw new PayException('必须提供商户号');
        }

        if (key_exists('appid', $config) && !empty($config['appid'])) {
            $this->subAppid = $config['appid'];
        } else {
            throw new PayException('微信公众号APPID');
        }

        if (key_exists('private_key', $config) && !empty($config['private_key'])) {
            $this->privateKey = $config['private_key'];
        } else {
            throw new PayException('必须提供私钥');
        }

        if (key_exists('private_key_pass', $config) && !empty($config['private_key_pass'])) {
            $this->private_key_pass = $config['private_key_pass'];
        } else {
            throw new PayException('必须提供私钥密码');
        }

        if (key_exists('public_key', $config) && !empty($config['public_key'])) {
            $this->publicKey = $config['public_key'];
        } else {
            throw new PayException('必须提供公钥');
        }

        if (key_exists('returnUrl', $config) && !empty($config['returnUrl'])) {
            $this->returnUrl = $config['returnUrl'];
        }

        if (key_exists('signType', $config) && !empty($config['signType'])) {
            $this->signType = $config['signType'];
        }

        if (key_exists('payType', $config) && !empty($config['payType'])) {
            $this->payType = $config['payType'];
        }

        if (key_exists('subMerchantNo', $config) && !empty($config['subMerchantNo'])) {
            $this->subMerchantNo = $config['subMerchantNo'];
        }

        if (key_exists('request_url', $config) && !empty($config['request_url'])) {
            $this->base_url = $config['request_url'];
        }

        $this->requestDate = date('YmdHis');

    }
}
