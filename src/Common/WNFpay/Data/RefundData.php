<?php

namespace Payment\Common\WNFpay\Data;

use Payment\Common\PayException;
use Payment\Common\WNFpay\WNFBaseData;

class RefundData extends WNFBaseData
{

    /**
     * 构建用于支付退款的签名相关数据
     *
     * @return void
     */
    protected function buildData(){

        $data = [
            'orderNo'          => $this->orderNo ,
            'refundReason'     => $this->refundReason ,
        ];
        $this->retData = $data;
    }

    /**
     * 参数校验
     * @return mixed|void
     * @throws PayException
     */
    protected function checkDataParam(){
        if(empty($this->orderNo)){
            throw new PayException('退款订单号不能为空');
        }

    }

}