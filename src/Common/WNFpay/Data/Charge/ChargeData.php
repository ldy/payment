<?php

namespace Payment\Common\WNFpay\Data\Charge;

use Payment\Common\PayException;
use Payment\Common\WNFpay\WNFBaseData;
use Payment\Utils\ArrayUtil;

class ChargeData extends WNFBaseData
{
    /**
     * 构建用于支付的签名相关数据
     *
     * @return void
     */
    protected function buildData(){

        $data = [
            'customerOrderNo' => $this->customerOrderNo,
            'amount'          => $this->amount,
            'subject'         => $this->subject,
            'sellerNote'      => $this->sellerNote,
            'userid'          => $this->userid,//用户标识
            'appid'           => $this->appid,
            'payType'         => $this->payType,
            'callbackUrl'     => $this->callbackUrl,
            'goodsListItem'   => $this->goodsListItem,//商品列表，此处应该是数组
            'taxNo'           => $this->taxNo ?? '',
            'appKey'          => $this->appKey ?? '',
            'deptId'          => $this->deptId ?? ''
        ];
        $this->retData = $data;
    }

    /**
     * 参数校验
     * @return mixed|void
     * @throws PayException
     */
    protected function checkDataParam(){
        if(empty($this->callbackUrl)){
            throw new PayException('支付通知地址不能为空');
        }
        if(empty($this->amount)){
            throw new PayException('金额不能为空');
        }
        if(empty($this->subject)){
            throw new PayException('交易商品不能为空');
        }
        if(empty($this->sellerNote)){
            throw new PayException('商户备注不能为空');
        }
        if(empty($this->userid)){
            throw new PayException('用户标识不能为空');
        }
        if(empty($this->appid)){
            throw new PayException('小程序appid不能为空');
        }
        if(empty($this->payType)){
            throw new PayException('支付类型 不能为空');
        }
        if(empty($this->goodsListItem)){
            throw new PayException('商品列表不能为空');
        }

    }
}