<?php
namespace Payment\Common\WNFpay\Data\Query;
use Payment\Common\BaseData;
use Payment\Common\PayException;
use Payment\Common\WNFpay\WNFBaseData;

class ChargeQueryData extends WNFBaseData
{
    /**
     * 构建用于支付的签名相关数据
     *
     * @return void
     */
    protected function buildData(){

        $data = [
            'customerOrderNo' => $this->customerOrderNo,
        ];
        $this->retData = $data;
    }
    /**
     * 检查传入的参数. $reqData是否正确.
     *
     * @return mixed
     * @throws PayException
     */
    protected function checkDataParam(){
        if(empty($this->customerOrderNo)){
            throw new PayException('订单号不能为空');
        }
    }

}