<?php

namespace Payment\Common\WNFpay;

use Payment\Common\BaseData;

abstract class WNFBaseData extends BaseData
{
    protected function makeSign($signStr){
        list($microsecond , $time) = explode(' ', microtime()); //' '中间是一个空格
        $timestamp = (float)sprintf('%.0f',(floatval($microsecond)+floatval($time))*1000);
        $this->retData['timestamp'] = $timestamp;
        $this->retData['sign'] = $sign = md5(md5($this->key . $timestamp) . md5($timestamp));
        return $sign;
    }
}