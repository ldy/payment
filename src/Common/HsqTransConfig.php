<?php

namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class HsqTransConfig extends ConfigInterface
{

    public const PAY_URL = '/BF0040001.do';   //代付交易接口
    public const QUERY_URL = '/BF0040002.do'; //代付交易状态查证接口
    public const REFUND_URL = '/BF0040003.do'; //退款申请

    public $base_url = 'https://public.baofoo.com/baofoo-fopay/pay'; //接口地址

    public $version = '4.0.0'; //版本

    public $data_type = 'json'; //数据类型

    public $member_id; //商户号

    public $terminal_id; //终端号

    public $privateKey; //私钥

    public $publicKey; //公钥

    public $private_key_pass; //私钥密码


    /**
     * FuConfig constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */
    private function initConfig(array $config)
    {
        $config = ArrayUtil::paraFilter($config);

        if (key_exists('member_id', $config) && !empty($config['member_id'])) {
            $this->member_id = $config['member_id'];
        } else {
            throw new PayException('必须提供商户号');
        }

        if (key_exists('terminal_id', $config) && !empty($config['terminal_id'])) {
            $this->terminal_id = $config['terminal_id'];
        } else {
            throw new PayException('必须提供终端号');
        }

        if (key_exists('private_key', $config) && !empty($config['private_key'])) {
            $this->privateKey = $config['private_key'];
        } else {
            throw new PayException('必须提供私钥');
        }

        if (key_exists('private_key_pass', $config) && !empty($config['private_key_pass'])) {
            $this->private_key_pass = $config['private_key_pass'];
        } else {
            throw new PayException('必须提供私钥密码');
        }

        if (key_exists('public_key', $config) && !empty($config['public_key'])) {
            $this->publicKey = $config['public_key'];
        } else {
            throw new PayException('必须提供公钥');
        }

        if (key_exists('data_type', $config) && !empty($config['data_type'])) {
            $this->data_type = $config['data_type'];
        }

        if (key_exists('version', $config) && !empty($config['version'])) {
            $this->version = $config['version'];
        }

        if (key_exists('base_url', $config) && !empty($config['base_url'])) {
            $this->base_url = $config['base_url'];
        }


    }
}
