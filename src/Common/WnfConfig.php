<?php

namespace Payment\Common;

use Payment\Utils\ArrayUtil;

class WnfConfig extends ConfigInterface
{

    /**
     * 客户域名不可用，暂时使用IP，后期域名可用后在替换
     */
    // 下单url
    const PAY_URL = 'https://mall.jn-aisino.com/prod-api/api/pay';

    // 查询url
    const QUERY_URL = 'https://mall.jn-aisino.com/prod-api/api/query';

    // 退款url
    const REFUND_URL = 'https://mall.jn-aisino.com/prod-api/api/refund';

    // 指定回调页面
    public $returnUrl;

    public $key;

    /**
     * FuConfig constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */
    private function initConfig(array $config)
    {
        $config = ArrayUtil::paraFilter($config);
        if (key_exists('key', $config) && !empty($config['key'])) $this->key = $config['key'];else throw new PayException('支付密钥不存在');

    }

}