<?php

namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class YSEConfig extends ConfigInterface{

	public const REQUEST_URL = 'https://qrcode.ysepay.com/gateway.do';

	public const REFUND_URL = 'https://openapi.ysepay.com/gateway.do';

	public const QUERY_URL = 'https://search.ysepay.com/gateway.do';

    public $sign; //签名

    public $sign_type = 'RSA';

	public $partner_id;//商户号

	public $business_code;//业务代码

	public $seller_id;//收款方银盛支付用户号

	public $seller_name;//收款方银盛支付客户名

	public $charset = 'UTF-8'; //编码格式

	public $version = '3.0'; //调用的接口版本

	public $notify_url; //银盛支付服务器主动通知商户服务器里指定的页面 http 路径。

	public $biz_content; //JSON 格式，具体包含的内容参见各个接口的业务参数描述



	/**
	 * FuConfig constructor.
	 * @param array $config
	 * @throws PayException
	 */
	public function __construct(array $config)
	{
		try {
			$this->initConfig($config);
		} catch (PayException $e) {
			throw $e;
		}
	}

	/**
	 * 初始化配置文件参数
	 * @param array $config
	 * @throws PayException
	 */

	/**
	 * 初始化配置文件参数
	 * @param array $config
	 * @throws PayException
	 */
	private function initConfig(array $config)
	{
		$config = ArrayUtil::paraFilter($config);

		if (key_exists('partner_id', $config) && !empty($config['partner_id'])) $this->partner_id = $config['partner_id'];else throw new PayException('必须提供商户号标识');

		if (key_exists('seller_id', $config) && !empty($config['seller_id'])) $this->seller_id = $config['seller_id'];else throw new PayException('必须提供收款方银盛支付用户号');

		if (key_exists('seller_name', $config) && !empty($config['seller_name'])) $this->seller_name = $config['seller_name'];else throw new PayException('必须提供收款方银盛支付客户名');

        if (key_exists('business_code', $config) && !empty($config['business_code'])) $this->business_code = $config['business_code'];else throw new PayException('必须提供业务代码');

	}
}
