<?php
/**
 *
 * @createTime: 2016-07-15 14:56
 * @description: 微信配置文件
 * @link      https://github.com/helei112g/payment/tree/paymentv2
 * @link      https://helei112g.github.io/
 */

namespace Payment\Common;

use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;

final class TLConfig extends ConfigInterface
{
    // 应用ID
    public $appId;

    // 平台分配的商户号
    public $cusid;

    // 随机字符串，不长于32位
    public $randomstr;

    // 符合ISO 4217标准的三位字母代码
    public $feeType = 'CNY';

    // 用于加密的md5Key
    public $md5Key;

    // 	交易方式，小程序：W06
    public $paytype;

    // 指定回调页面
    public $returnUrl;

    public $version = 11;//接口版本号

    // 统一下单url
    const UNIFIED_URL = 'https://vsp.allinpay.com/apiweb/unitorder/pay';

    // 撤销交易url
    const ORDER_CANCEL_URL = 'https://vsp.allinpay.com/apiweb/unitorder/cancel';


    // 申请退款url
    const REFUND_URL = 'https://vsp.allinpay.com/apiweb/unitorder/refund';

    // 支付查询url
    const CHARGE_QUERY_URL = 'https://vsp.allinpay.com/apiweb/unitorder/query';


    // 退款账户
    const REFUND_UNSETTLED = 'REFUND_SOURCE_UNSETTLED_FUNDS';// 未结算资金退款（默认使用未结算资金退款）
    const REFUND_RECHARGE = 'REFUND_SOURCE_RECHARGE_FUNDS';// 可用余额退款(限非当日交易订单的退款）


    const PLATFORM_APPID = '00032161'; // 平台appId
    const orgid = '55079104816UGBA'; // 代为发起交易的机构商户号


    /**
     * 初始化配置文件
     * TLConfig constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */
    private function initConfig(array $config)
    {
        $config = ArrayUtil::paraFilter($config);

        // 检查 分配的公众账号ID
        if (key_exists('app_id', $config) && !empty($config['app_id'])) {
            $this->appId = $config['app_id'];

            if(self::PLATFORM_APPID == $this->appId){
                $this->orgid = self::orgid;
            }else{
                $this->orgid = null;
            }
        } else {
            throw new PayException('必须提供支付平台分配的公众账号ID');
        }

        // 检查 平台支付分配的商户号
        if (key_exists('cus_id', $config) && !empty($config['cus_id'])) {
            $this->cusid = $config['cus_id'];
        } else {
            throw new PayException('必须提供支付平台分配的商户号');
        }

        // 初始 MD5 key
        if (key_exists('md5_key', $config) && !empty($config['md5_key'])) {
            $this->md5Key = $config['md5_key'];
        } else {
            throw new PayException('MD5 Key 不能为空，再通联商户后台可查看');
        }


        // 检查 异步通知的url
        if (key_exists('notify_url', $config) && !empty($config['notify_url'])) {
            $this->notifyUrl = trim($config['notify_url']);
        } else {
            throw new PayException('异步通知的url必须提供.');
        }

        // 设置禁止使用的支付方式
        if (key_exists('limit_pay', $config) && !empty($config['limit_pay']) && $config['limit_pay'][0] === 'no_credit') {
            $this->limitPay = $config['limit_pay'][0];

        }

        if (key_exists('return_raw', $config)) {
            $this->returnRaw = filter_var($config['return_raw'], FILTER_VALIDATE_BOOLEAN);
        }

        if (key_exists('redirect_url', $config)) {
            $this->returnUrl = $config['redirect_url'];
        }

        // 生成随机字符串
        $this->randomstr = StrUtil::getNonceStr();
    }
}
