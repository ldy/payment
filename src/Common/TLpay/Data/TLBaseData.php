<?php
/**
 *
 * @createTime: 2016-07-28 18:05
 * @description: 微信支付相关接口的数据基类
 */

namespace Payment\Common\TLpay\Data;

use Payment\Common\BaseData;
use Payment\Utils\StrUtil;

/**
 * Class BaseData
 *
 *
 * @package Payment\Common\TLpay\Data
 */
abstract class TLBaseData extends BaseData
{

    /**
     * 签名算法实现  通联支付sign生成算法：sign = md5(string.getbyte("utf-8")).toUpperCase();
     * @param string $signStr
     * @return string
     */
    protected function makeSign($signStr){

        return strtoupper(md5(StrUtil::getBytes($signStr)));
    }
}
