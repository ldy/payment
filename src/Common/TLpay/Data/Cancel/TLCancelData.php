<?php

namespace Payment\Common\TLpay\Data\Cancel;

use Payment\Common\PayException;
use Payment\Common\TLpay\Data\TLBaseData;
use Payment\Utils\ArrayUtil;

/**
 * Class TLChargeData
 * 通联支付
 *
 * @package Payment\Common\TLpay\Data\Charge
 * anthor yeran
 */
class TLCancelData extends TLBaseData
{
    protected function checkDataParam()
    {

        $oldreqsn = $this->oldreqsn;//原交易的商户交易单号
        $oldtrxid = $this->oldtrxid;//原交易的收银宝平台流水
        $reqsn = $this->reqsn;//商户的退款交易订单号,商户平台唯一
        if ((empty($oldreqsn) && empty($oldtrxid)) || empty($reqsn)) {
            throw new PayException('单号数据缺失');
        }
    }

    /**
     * 组装数据，用于计算sign，以及与支付平台对接
     *
     * 交易撤销数据
     *
     */
    protected function buildData()
    {
        $signData = [
            // 基本数据
            'appid' => trim($this->appId),
            'cusid'    => trim($this->cusid),
            'version'  => $this->version,
            'randomstr' => $this->randomstr,

            // 业务数据
            'reqsn'  => trim($this->reqsn),//商户退款交易单号
            'trxamt' => $this->trxamt,//单位分,原订单金额
            'oldreqsn'    => $this->oldreqsn,//原交易的商户交易单号
            'oldtrxid'    => $this->oldtrxid,//原交易的收银宝平台流水
        ];
        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }

    /**
     * 处理支付平台的返回值并返回给客户端
     * @param array $ret
     * @return mixed
     * @author yeran
     */
    protected function retData(array $ret){

        // 字段  含义      取值          可空  最大长度 备注
        //cusid	商户号	平台分配的商户号	否	15
        //appid	应用ID	平台分配的APPID	否	8
        //trxid	交易单号	收银宝平台的退款交易流水号	否	20
        //reqsn	商户订单号	商户的退款交易订单号	否	32
        //trxstatus	交易状态	交易的状态	否	4	见3.1
        //fintime	交易完成时间	yyyyMMddHHmmss	是	14
        //errmsg	错误原因	失败的原因说明	是	100
        //randomstr	随机字符串	随机生成的字符串	否	32
        //sign	签名		否	32

        return $ret;
    }
}
