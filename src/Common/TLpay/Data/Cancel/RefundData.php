<?php

namespace Payment\Common\TLpay\Data\Cancel;

use Payment\Common\PayException;
use Payment\Common\TLpay\Data\TLBaseData;
use Payment\Utils\ArrayUtil;

/**
 * 用户退款
 * Class RefundData
 *
 * @package Payment\Common\TLpay\Data\Cancel
 * anthor yeran
 */
class RefundData extends TLBaseData
{
    /**
     * 退款数据封装
     */
    protected function buildData()
    {
        $this->retData = [
            'appid' => $this->appId,
            'cusid'    => $this->cusid,
            'randomstr' => $this->randomstr,
            'version' => $this->version,
            'trxamt' => $this->trxamt,// 退款金额,分
            'reqsn'  => $this->reqsn,// 商户退款单号
            'oldreqsn' => $this->oldreqsn,//原交易的商户订单号
            'oldtrxid'    => $this->oldtrxid,//原交易的收银宝平台流水
            'remark' => $this->remark,// 备注
        ];

        $this->retData = ArrayUtil::paraFilter($this->retData);
    }

    /**
     * 检查参数
     * @author yeran
     */
    protected function checkDataParam()
    {
        $reqsn = $this->reqsn;// 商户退款单号
        $oldreqsn = $this->oldreqsn;
        $oldtrxid = $this->oldtrxid;
        $trxamt = $this->trxamt;


        if (empty($reqsn)) {
            throw new PayException('请设置退款单号 refund_no');
        }

        // 二者不能同时为空
        if (empty($oldreqsn) && empty($oldtrxid)) {
            throw new PayException('必须提供通联支付交易号或商户网站唯一订单号。建议使用通联支付交易号');
        }

        if ($trxamt<1) {
            throw new PayException('退款金额异常');
        }

    }
}
