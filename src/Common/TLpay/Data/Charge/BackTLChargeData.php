<?php
/**
 *
 * @createTime: 2016-08-02 10:27
 * @description:
 */

namespace Payment\Common\TLpay\Data;


/**
 * Class BackPubChargeData
 *  小程序数据也在这里处理
 * @property string $device_info   设备号
 * @property string $trade_type  交易类型
 * @property string $prepay_id   预支付交易会话标识
 *
 * @package Payment\Common\Weixin\Data
 *
 */
class BackTLChargeData extends TLBaseData
{
    protected function buildData()
    {
        $this->retData = [
            'appId' => $this->appid,
            'timeStamp' => time() . '',
            'nonceStr'  => $this->randomstr,
            'package'   => 'prepay_id=' . $this->chnltrxid,
            'signType'  => 'MD5',// 签名算法，暂支持MD5
        ];


    }

    protected function checkDataParam()
    {
        // 不进行检查
    }
}
