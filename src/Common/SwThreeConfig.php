<?php


namespace Payment\Common;

use Payment\Utils\ArrayUtil;

final class SwThreeConfig extends ConfigInterface
{
    public $signType = 'MD5';

    public $pay_ver = 201;//版本号，当前版本201

    public $pay_type;//请求类型，010微信，020支付宝，060qq钱包，090口碑，100翼支付

    public $service_id;//接口类型

    public $merchant_no;//商户号

    public $terminal_id;//终端号

    public $sub_appid;// 微信分配的公众账号ID,公众号支付时使用的appid（若传入，则open_id需要保持一致）

    public $terminal_time;//终端交易时间，yyyyMMddHHmmss，全局统一时间格式


    public $certainSignData = null;//专门用于签名的数组

    public $sign_sort = false;//签名是否需要字典排序

    // 指定回调页面
    public $notify_url;


    const BASE_URL = 'https://pay.lcsw.cn/lcsw';// 'http://test.lcsw.cn:8045/lcsw';

    const UNIFIED_URL = '/pay/open/jspay';  // 公众号支付统一下单url

    const LITEPAY_URL = '/pay/open/minipay';// 小程序支付url

//    const FACEPAY_URL = '/pay/110/facepay';//自助收银

    const SCANPAY_URL = '/pay/open/prepay';//扫码支付（预支付）

//    const BARCODE_URL = '/pay/100/barcodepay';//刷卡（条码）支付


//    const FACEPAY_ACCESSTOKEN_URL = '/pay/110/faceinfo';//自助收银SDK调用凭证获取接口

//    const AUTH_OPENID = '/wx/jsapi/authopenid';//用于服务商通道获取微信openid,Method：GET

//    const AUTHCODE_TO_OPENID_URL = '/pay/110/authcodetoopenid';//授权码查询 OPENID 接口



    const CHARGE_QUERY_URL = '/pay/open/query'; // 支付查询url

    const REFUND_URL = '/pay/open/refund';  // 申请退款url


    const CANCEL_URL = '/pay/open/cancel';//撤销交易(只针对刷卡支付)

    const CLOSE_URL = '/pay/open/close';//关闭订单（仅限服务商模式商户且为微信支付时可用）

//    const SIGN_URL  = '/pay/100/sign';//注册终端（获取access_token）,一台机器只有一次调用机会，遗失请联系客服申请重置

    /**
     * 初始化微信配置文件
     * WxConfig constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        try {
            $this->initConfig($config);
        } catch (PayException $e) {
            throw $e;
        }

        $basePath = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'CacertFile' . DIRECTORY_SEPARATOR;
        $this->cacertPath = "{$basePath}wx_cacert.pem";
    }

    /**
     * 初始化配置文件参数
     * @param array $config
     * @throws PayException
     */
    private function initConfig(array $config)
    {
        $config = ArrayUtil::paraFilter($config);

        if (key_exists('pay_ver', $config) && !empty($config['pay_ver'])) {
            $this->pay_ver = $config['pay_ver'];
        }

        if (key_exists('pay_type', $config) && !empty($config['pay_type'])) {
            $this->pay_type = $config['pay_type'];
        } else {
            throw new PayException('必须提供请求类型，010微信，020支付宝，060qq钱包，090口碑，100翼支付');
        }

        if (key_exists('service_id', $config) && !empty($config['service_id'])) {
            $this->service_id = $config['service_id'];
        }

        if (key_exists('merchant_no', $config) && !empty($config['merchant_no'])) {
            $this->merchant_no = $config['merchant_no'];
        } else {
            throw new PayException('必须提供SW商户号');
        }

        if (key_exists('terminal_id', $config) && !empty($config['terminal_id'])) {
            $this->terminal_id = $config['terminal_id'];
        } else {
            throw new PayException('必须提供SW终端号');
        }


        if (key_exists('sub_appid', $config) && !empty($config['sub_appid'])) {
            $this->sub_appid = $config['sub_appid'];
        }

        // 检查 异步通知的url
        if (key_exists('notify_url', $config) && !empty($config['notify_url'])) {
            $this->notify_url = trim($config['notify_url']);
        } else {
            throw new PayException('异步通知的url必须提供.');
        }

        if (key_exists('attach', $config)) {
            $this->attach = trim($config['attach']);
        }

        if (key_exists('access_token', $config)) {
            $this->access_token = trim($config['access_token']);
        }

        if (key_exists('base_url', $config)) {
            $this->base_url = trim($config['base_url']);
        }

        // 设置交易开始时间 格式为yyyyMMddHHmmss，在次之前一定要设置时区
        $startTime = time();
        $this->terminal_time = date('YmdHis', $startTime);
    }
}
