<?php

namespace Payment\Common\SwThree\Data;
use Payment\Common\PayException;


/**
 * Class BackLiteChargeData
 *
 *
 * @package Payment\Common\Sw\Data
 *
 */
class BackLiteChargeData extends SwBaseData
{
    public function getData()
    {
        switch ($this->pay_type){
            case '010':{ //微信
                $data = [
                    'timeStamp' => $this->timeStamp,//time() . '',
                    'nonceStr'  => $this->nonceStr,
                    'package'   => $this->package_str,
                    'signType'  => $this->signType,// 签名算法，暂支持MD5
                    'paySign'   => $this->paySign
                ];

                break;
            }

            case '020':{ //支付宝
                $data = [
                    'ali_trade_no' => $this->ali_trade_no,
                ];

                break;
            }
            default:{
                throw new PayException('未知的官方支付方式');
                break;
            }
        }

        $this->retData['jspackage'] = $data;
        $this->retData['other'] = [
            'pay_type' => $this->pay_type,
            'merchant_name' => $this->merchant_name,
            'merchant_no' => $this->merchant_no,
            'terminal_id' => $this->terminal_id,
            'terminal_trace'=> $this->terminal_trace,
            'terminal_time' => $this->terminal_time,
            'total_fee' => $this->total_fee,
            'out_trade_no' => $this->out_trade_no
        ];

        return parent::getData();
    }

    protected function checkDataParam()
    {
        // 对于返回数据不做检查检查
    }

    /**
     * 构建用于支付的签名相关数据
     * @return array
     */
    protected function buildData()
    {
        // TODO: Implement buildData() method.
    }
}
