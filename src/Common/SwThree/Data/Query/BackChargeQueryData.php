<?php


namespace Payment\Common\SwThree\Data\Query;

use Payment\Common\SwThree\Data\SwBaseData;


/**
 * 查询交易的数据结构
 * Class BackChargeQueryData
 *
 *
 * @package Payment\Common\Sw\Data\Query
 */
class BackChargeQueryData extends SwBaseData
{
    protected function buildData()
    {
    }

    protected function checkDataParam()
    {
        // 对于返回数据不做检查检查
    }

}