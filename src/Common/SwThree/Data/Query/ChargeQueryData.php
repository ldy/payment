<?php


namespace Payment\Common\SwThree\Data\Query;


use Payment\Common\PayException;
use Payment\Common\SwThree\Data\SwBaseData;
use Payment\Utils\ArrayUtil;

/**
 * 查询交易的数据结构
 * Class ChargeQueryData
 *
 *
 * @package Payment\Common\Sw\Data\Query
 */
class ChargeQueryData extends SwBaseData
{
    protected function checkDataParam()
    {
        $this->pay_ver = '201';
        $this->service_id = '020';

        // 二者不能同时为空
        if (empty($this->merchant_no) && empty($this->terminal_id)) {
            throw new PayException('商户号、终端号都不能为空');
        }

        if (empty($this->terminal_trace) && empty($this->terminal_time)) {
            throw new PayException('终端查询流水号、终端查询时间都不能为空');
        }

        if (empty($this->out_trade_no)) {//订单号，查询凭据，可填利楚订单号、微信订单号、支付宝订单号、银行卡订单号任意一个
            throw new PayException('查询订单号不能为空（可填利楚订单号、微信订单号、支付宝订单号、银行卡订单号任意一个）');
        }
    }


    protected function buildData(){

        $this->certainSignData = [
            // 基本数据
            'pay_ver'       => trim($this->pay_ver),
            'pay_type'      => trim($this->pay_type),
            'service_id'    => $this->service_id,
            'merchant_no'   => $this->merchant_no,
            'terminal_id'   => $this->terminal_id,
            'terminal_trace'=> $this->terminal_trace,//终端查询流水号，填写商户系统的查询流水号
            'terminal_time' => $this->terminal_time,
            'out_trade_no' => $this->out_trade_no

        ];

        $signData  = array_merge($this->certainSignData,[]);

        if(isset($this->pay_trace)){//用户标识（微信openid），用于调起微信刷脸SDK
            $signData['pay_trace'] =  $this->pay_trace;
        }
        if(isset($this->pay_time)){
            $signData['pay_time'] = $this->pay_time;
        }
        $this->retData = ArrayUtil::paraFilter($signData);
    }

}