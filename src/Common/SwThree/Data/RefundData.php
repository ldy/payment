<?php


namespace Payment\Common\SwThree\Data;


use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

/**
 * 退款交易的数据结构
 * Class RefundData
 *
 *
 * @package Payment\Common\Sw\Data\Query
 */
class RefundData extends SwBaseData
{
    protected function checkDataParam()
    {
//        parent::checkDataParam();


        $this->pay_ver = '201';
        $this->service_id = '030';

        // 二者不能同时为空
        if (empty($this->merchant_no) && empty($this->terminal_id)) {
            throw new PayException('商户号、终端号都不能为空');
        }

        if (empty($this->terminal_trace) && empty($this->terminal_time)) {
            throw new PayException('终端退款流水号、终端退款时间都不能为空');
        }

        if (empty($this->out_trade_no)) {//订单号，查询凭据，订单号，查询凭据，利楚订单号、微信订单号、支付宝订单号任意一个
            throw new PayException('订单号不能为空（订单号，查询凭据，利楚订单号、微信订单号、支付宝订单号任意一个）');
        }
        if (empty($this->refund_fee)) {//退款金额，单位分
            throw new PayException('退款金额，不能为空');
        }
    }

    protected function buildData(){

        $this->certainSignData = [
            // 基本数据
            'pay_ver'       => trim($this->pay_ver),
            'pay_type'      => trim($this->pay_type),
            'service_id'    => $this->service_id,
            'merchant_no'   => $this->merchant_no,
            'terminal_id'   => $this->terminal_id,
            'terminal_trace'=> $this->terminal_trace,//终端查询流水号，填写商户系统的查询流水号
            'terminal_time' => $this->terminal_time,
            'refund_fee' => $this->refund_fee,// 金额，单位分
            'out_trade_no'     => $this->out_trade_no,//订单号，查询凭据，利楚订单号、微信订单号、支付宝订单号任意一个
        ];

        $signData  = array_merge($this->certainSignData,[]);

        if(isset($this->pay_trace)){//当前支付终端流水号，与pay_time同时传递
            $signData['pay_trace'] =  $this->pay_trace;
        }
        if(isset($this->pay_time)){//当前支付终端交易时间，yyyyMMddHHmmss，全局统一时间格式，与pay_trace同时传递
            $signData['pay_time'] = $this->pay_time;
        }
        if(isset($this->auth_code)){//短信或邮箱验证码
            $signData['auth_code'] = $this->auth_code;
        }

        $this->retData = ArrayUtil::paraFilter($signData);
    }

}