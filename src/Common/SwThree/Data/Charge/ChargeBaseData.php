<?php


namespace Payment\Common\SwThree\Data\Charge;

use Payment\Common\PayException;
use Payment\Common\SwThree\Data\SwBaseData;
use Payment\Config;

/**
 * Class ChargeBaseData
 *
 *
 * @package Payment\Common\Sw\Data\Charge
 */
abstract class ChargeBaseData extends SwBaseData
{

    /**
     * 检查传入的支付信息是否正确
     */
    protected function checkDataParam()
    {
        // 检查订单号是否合法
        if (empty($this->terminal_trace) || mb_strlen($this->terminal_trace) > 32) {
            throw new PayException('商户系统的订单号不能为空且长度不能超过32位');
        }

        // 检查金额不能低于0.01
        if ($this->total_fee < 1) {
            throw new PayException('支付金额不能低于 ' . Config::PAY_MIN_FEE . ' 元');
        }

        // 检查 商品名称 与 商品描述
        if (empty($this->terminal_id) || empty($this->merchant_no)) {
            throw new PayException('必须提供终端号与商户号');
        }
    }
}
