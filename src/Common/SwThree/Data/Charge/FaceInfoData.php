<?php


namespace Payment\Common\SwThree\Data\Charge;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

/**
 * Class FaceInfoData
 * 自助收银SDK调用凭证获取接口
 *
 *
 * @package Payment\Common\Sw\Data\Charge
 *
 */
class FaceInfoData extends ChargeBaseData
{
    protected function checkDataParam()
    {
        parent::checkDataParam();

        $payType = $this->pay_type; //010微信，020支付宝
        if($payType!='010' || $payType!='020'){
            throw new PayException('暂不支持微信、支付宝以外的人脸识别SDK初始化');
        }

        if(!isset($this->rawdata) || empty($this->rawdata))
            throw new PayException('微信、支付宝人脸识别SDK初始化数据不能为空');

        $this->pay_ver = '110';
    }

    protected function buildData()
    {
        $this->sign_sort = true;

        $signData = [
            // 基本数据
            'pay_ver'       => trim($this->pay_ver),
            'pay_type'      => trim($this->pay_type),
            'merchant_no'   => $this->merchant_no,
            'terminal_no'   => $this->terminal_id,
            'trace_no'      => $this->terminal_trace,//终端流水号，填写商户系统的订单号
            'terminal_time' => $this->terminal_time,
            'rawdata'       => $this->rawdata, //微信、支付宝人脸识别SDK初始化数据
        ];

        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }
}
