<?php


namespace Payment\Common\SwThree\Data\Cancel;

use Payment\Common\SwThree\Data\SwBaseData;
use Payment\Utils\ArrayUtil;

/**
 * Class CancelData
 * 扫呗取消订单
 *
 *
 * @package Payment\Common\Sw\Data\Charge
 *
 */
class CancelData extends SwBaseData
{
    protected function checkDataParam()
    {
        $this->pay_ver = '201';
        $this->service_id = '040';
    }

    protected function buildData(){

        $this->certainSignData = [
            // 基本数据
            'pay_ver'       => trim($this->pay_ver),
            'pay_type'      => trim($this->pay_type),
            'service_id'    => $this->service_id,
            'merchant_no'   => $this->merchant_no,
            'terminal_id'   => $this->terminal_id,
            'terminal_trace'  => $this->terminal_trace,
            'terminal_time' => $this->terminal_time,
        ];

        $signData = array_merge($this->certainSignData,[]);

        if(isset($this->out_trade_no)){
            $signData['out_trade_no'] = $this->out_trade_no;
        }
        if(isset($this->pay_trace)){
            $signData['pay_trace'] =  $this->pay_trace;
        }
        if(isset($this->pay_time)){
            $signData['pay_time'] = $this->pay_time;
        }

        // 移除数组中的空值
        $this->retData = ArrayUtil::paraFilter($signData);
    }
}
