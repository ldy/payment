<?php

namespace Payment\Common\Weixin\Data\Query;

use Payment\Common\PayException;
use Payment\Common\Weixin\Data\WxBaseData;
use Payment\Utils\ArrayUtil;

/**
 * 微信企业付款到银行卡查询，当前微信仅支持根据商户订单号来进行查询
 *
 * @property string $trans_no 企业付款单号
 *
 * Class PayBankQueryData
 * @package Payment\Common\Weixin\Data\Query
 */
class PayBankQueryData extends WxBaseData
{

    protected function buildData()
    {
        $this->retData = [
            'mch_id' => $this->mchId,
            'partner_trade_no' => $this->trans_no,
            'nonce_str' => $this->nonceStr,
        ];
        $this->retData = ArrayUtil::paraFilter($this->retData);
    }

    protected function checkDataParam()
    {
        $transNo = $this->trans_no;
        if (empty($transNo)) throw new PayException('请提供商户调用企企业付款到银行卡API时所使用的付款单号');
    }

}
