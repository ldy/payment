<?php

namespace Payment\Common\Weixin\Data;

use Payment\Common\PayException;
use Payment\Utils\ArrayUtil;

/**
 * 企业付款到银行卡
 * Class PayBankData
 *
 * @package Payment\Common\Weixin\Data
 *
 * @property string $trans_no 商户唯一订单号
 * @property string $payer_bank_no 收款方银行卡号（采用标准RSA算法，公钥由微信侧提供）
 * @property string $payer_real_name 收款方用户名（采用标准RSA算法，公钥由微信侧提供）
 * @property string $payer_bank_code 银行卡所在开户行编号
 *  - 银行编号列表：https://pay.weixin.qq.com/wiki/doc/api/tools/mch_pay.php?chapter=24_4
 *
 * @property string $amount 付款金额，单位：元。只支持2位小数，小数点前最大支持13位，金额必须大于0
 * @property string $desc 企业付款到银行卡付款说明，即订单备注
 */
class PayBankData extends WxBaseData
{

    /**
     * 构建用于支付的签名相关数据
     * @return array|void
     * @createTime 2019/10/10 12:37
     */
    protected function buildData()
    {
        $this->retData = [
            'mch_id' => $this->mchId,
            'partner_trade_no' => $this->trans_no,
            'nonce_str' => $this->nonceStr,
            'enc_bank_no' => $this->payer_bank_no,
            'enc_true_name' => $this->payer_real_name,
            'bank_code' => $this->payer_bank_code,
            'amount' => $this->amount, //此处需要处理单位为分
            'desc' => $this->desc,
        ];

        $this->retData = ArrayUtil::paraFilter($this->retData);
    }

    /**
     * 检查相关参数是否设置
     * @return mixed|void
     * @throws PayException
     * @createTime 2019/10/10 14:03
     */
    protected function checkDataParam()
    {
        $transNo = $this->trans_no;
        $bankNo = $this->payer_bank_no;
        $bankCode = $this->payer_bank_code;
        $realName = $this->payer_real_name;
        $amount = $this->amount;

        if (empty($transNo)) throw new PayException('企业付款单号不能为空，且需保持唯一性');
        if (strlen($transNo) < 8 || strlen($transNo) > 32) throw new PayException('企业付款单号最短8位最长32位');

        if (empty($bankNo)) throw new PayException('请传入收款方银行卡号');
        $this->payer_bank_no = $this->encryptByRSA($bankNo);

        if (empty($bankCode)) throw new PayException('请传入收款方银行卡所在开户行编号');

        if (empty($realName)) throw new PayException('请传入收款方用户名');
        $this->payer_real_name = $this->encryptByRSA($realName);

        if (empty($amount) || $amount < 0) throw new PayException('企业付款到银行卡金额错误');
        $this->amount = bcmul($amount, 100, 0); //微信使用的单位为分，此处进行转化
    }

}
