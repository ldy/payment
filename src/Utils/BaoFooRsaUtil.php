<?php

namespace Payment\Utils;

use Payment\Common\PayException;

 class BaoFooRsaUtil{
    /**
     * 读取私钥
     * @param type $PfxPath
     * @param type $PrivateKPASS
     * @return array
     * @throws PayException
     */
    private static function getPriveKey($PfxPath,$PrivateKPASS)
    {
        if(file_exists($PfxPath))
        {
            $PfxPath = str_replace('\\', "/", $PfxPath);
            $PKCS12 = file_get_contents($PfxPath);
            $PrivateKey = array();
            if(openssl_pkcs12_read($PKCS12, $PrivateKey, $PrivateKPASS)){
                return $PrivateKey["pkey"];
            }else{
                throw new PayException("私钥证书读取出错！原因[证书或密码不匹配]，请检查本地证书相关信息。");
            }
        }
        else
        {
            return $PfxPath;
        }
    }
    
    /**
     * 读取公钥
     * @param type $PublicPath
     * @return type
     * @throws Exception
     */
    private static function getPublicKey($PublicPath){

        if(file_exists($PublicPath))
        {
            $PublicPath = str_replace('\\', "/", $PublicPath);
            $KeyFile = file_get_contents($PublicPath);
            $PublicKey = openssl_get_publickey($KeyFile);
            if(empty($PublicKey)){
                throw new PayException("公钥不可用！路径：".$PublicPath);
            }
            return $PublicKey;
        }
        else
        {
            return openssl_get_publickey($PublicPath);
        }


    }

    /**
     * 公钥加密
     * @param type $Data    加密数据
     * @param type $PfxPath     私钥路径
     * @param type $PrivateKPASS  私钥密码
     * @return type
     * @throws Exception
     */
    public static function encryptByCERFile($Data,$PublicPath){
        try {
            if (!function_exists( 'bin2hex')) {
                throw new PayException("bin2hex PHP5.4及以上版本支持此函数，也可自行实现！");
            }
            $KeyObj = self::getPublicKey($PublicPath);
            $BASE64EN_DATA = base64_encode($Data);
            $EncryptStr = "";
            $blockSize = self::get_Key_Size($KeyObj,false);
            if($blockSize<=0){
                throw new PayException("BlockSize is 0");
            }else{
                $blockSize = $blockSize/8-11;
            }
            $totalLen = strlen($BASE64EN_DATA);
            $EncryptSubStarLen = 0;
            $EncryptTempData="";
            while ($EncryptSubStarLen < $totalLen){
                openssl_public_encrypt(substr($BASE64EN_DATA, $EncryptSubStarLen, $blockSize), $EncryptTempData, $KeyObj);
                $EncryptStr .= bin2hex($EncryptTempData);
                $EncryptSubStarLen += $blockSize;
            }
            return $EncryptStr;
        } catch (PayException $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    /**
     * 私钥加密
     * @param type $Data
     * @param type $PfxPath
     * @param type $PrivateKPASS
     * @return type
     * @throws Exception
     */
    public static function encryptByPFXFile($Data,$PfxPath,$PrivateKPASS)
    {
        if (!function_exists( 'bin2hex')) {
            throw new PayException("bin2hex PHP5.4及以上版本支持此函数，也可自行实现！");
        }
        $KeyObj = self::getPriveKey($PfxPath,$PrivateKPASS);
        $BASE64EN_DATA = base64_encode($Data);
        $EncryptStr = "";
        $blockSize = self::get_Key_Size($KeyObj);
        if($blockSize<=0){
            throw new PayException("BlockSize is 0");
        }else{
            $blockSize = $blockSize/8-11;//分段
        }
        $totalLen = strlen($BASE64EN_DATA);
        $EncryptSubStarLen = 0;
        $EncryptTempData="";
        while ($EncryptSubStarLen < $totalLen){
            openssl_private_encrypt(substr($BASE64EN_DATA, $EncryptSubStarLen, $blockSize), $EncryptTempData, $KeyObj);
            $EncryptStr .= bin2hex($EncryptTempData);
            $EncryptSubStarLen += $blockSize;
        }
        return $EncryptStr;
    }

    /**
     * 私钥解密
     * @param type $Data    解密数据
     * @param type $PublicPath  解密公钥路径    
     * @return type
     * @throws Exception
     */
    public static function decryptByPFXFile($Data,$PfxPath,$PrivateKPASS){
        try {            
            if (!function_exists( 'hex2bin')) {
                throw new PayException("hex2bin PHP5.4及以上版本支持此函数，也可自行实现！");
            }
            $KeyObj = self::getPriveKey($PfxPath,$PrivateKPASS);            
            $blockSize = self::get_Key_Size($KeyObj);            
            if($blockSize<=0){
                throw new PayException("BlockSize is 0");
            }else{
                $blockSize = $blockSize/4;
            }
            $DecryptRsult="";
            $totalLen = strlen($Data);
            $EncryptSubStarLen = 0;
            $DecryptTempData="";
            while ($EncryptSubStarLen < $totalLen) {
                openssl_private_decrypt(hex2bin(substr($Data, $EncryptSubStarLen, $blockSize)), $DecryptTempData, $KeyObj);
                $DecryptRsult .= $DecryptTempData;
                $EncryptSubStarLen += $blockSize;
            }
            return base64_decode($DecryptRsult);
        } catch (PayException $exc) {
            echo $exc->getTraceAsString();
        }
    }
    
    /**
     * 公钥解密
     * @param type $Data
     * @param type $PublicPath
     * @return type
     * @throws Exception
     */
    public static function decryptByCERFile($Data,$PublicPath) {

        if (!function_exists( 'hex2bin')) {
            throw new PayException("hex2bin PHP5.4及以上版本支持此函数，也可自行实现！");
        }
        $KeyObj = self::getPublicKey($PublicPath);
        $DecryptRsult="";
        $blockSize = self::get_Key_Size($KeyObj,false);
        if($blockSize<=0){
            throw new PayException("BlockSize is 0");
        }else{
            $blockSize = $blockSize/4;
        }
        $totalLen = strlen($Data);
        $EncryptSubStarLen = 0;
        $DecryptTempData="";
        while ($EncryptSubStarLen < $totalLen) {
            openssl_public_decrypt(hex2bin(substr($Data, $EncryptSubStarLen, $blockSize)), $DecryptTempData, $KeyObj);
            $DecryptRsult .= $DecryptTempData;
            $EncryptSubStarLen += $blockSize;
        }
        return base64_decode($DecryptRsult);
    }
    
    /**
     * 获取证书长度
     * @param type $Key_String
     * @param type $Key_Type
     * @return int
     * @throws Exception
     */
    private static function get_Key_Size($Key_String,$Key_Type=true){
        $Key_Temp=array();
        try{
            if($Key_Type){//私钥
                $Key_Temp = openssl_pkey_get_details(openssl_pkey_get_private($Key_String));
            }else if(openssl_pkey_get_public($Key_String)){//公钥
                $Key_Temp = openssl_pkey_get_details(openssl_pkey_get_public($Key_String));
            }else{
                throw new PayException("Is not a key");
            }
            IF(array_key_exists("bits",$Key_Temp)){
                return $Key_Temp["bits"];
            }else{
                return 0;
            }
        } catch (PayException $ex){
            $ex->getTrace();
            return 0;
        }
    }
}