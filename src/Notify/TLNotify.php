<?php

namespace Payment\Notify;

use Payment\Common\PayException;
use Payment\Common\TLConfig;
use Payment\Config;
use Payment\Utils\ArrayUtil;
use Payment\Utils\StrUtil;


/**
 * Class TLNotify
 * 微信回调处理
 * @package Payment\Notify
 * anthor yeran
 */
class TLNotify extends NotifyStrategy
{

    /**
     * TLNotify constructor.
     * @param array $config
     * @throws PayException
     */
    public function __construct(array $config)
    {
        parent::__construct($config);

        try {
            $this->config = new TLConfig($config);
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 获取返回的异步通知数据
     * @return array|bool
     * @author yeran
     */
    public function getNotifyData()
    {
        $params = array();
        foreach($_POST as $key=>$val) {//动态遍历获取所有收到的参数,此步非常关键,因为收银宝以后可能会加字段,动态获取可以兼容由于收银宝加字段而引起的签名异常
            $params[$key] = $val;
        }
        if(count($params)<1){//如果参数为空,则不进行处理
            return false;
        }

        if(!$this->checkNotifyData($params)){
            return false;
        }

        return $params;
    }

    /**
     * 检查异步通知的数据是否正确
     * @param array $data
     *
     * @author yeran
     * @return boolean
     */
    public function checkNotifyData(array $data)
    {
        // 检查返回数据签名是否正确
        return $this->verifySign($data);
    }

    /**
     * 检查微信返回的数据是否被篡改过
     * @param array $retData
     * @return boolean
     * @author yeran
     */
    protected function verifySign(array $retData)
    {
        if(ArrayUtil::ValidSign($retData, $this->config->md5Key)){//验签成功
            //此处进行业务逻辑处理
           return true;
        }
        else{
            return false;
        }

    }

    /**
     *
     * 封装回调函数需要的数据格式
     *
     * @param array $data
     *
     * @return array
     * @author yeran
     */
    protected function getRetData(array $data)
    {
        if ($this->config->returnRaw) {
            $data['channel'] = Config::TL_CHARGE;
            return $data;
        }

        return $data;
    }

    /**
     * 处理完后返回的数据格式
     * @param bool $flag
     * @param string $msg 通知信息，错误原因
     * @author yeran
     * @return string
     */
    protected function replyNotify($flag, $msg = 'OK')
    {
        // 默认为成功
        $return_code ='SUCCESS';
        if (! $flag) {
            $return_code ='FAIL';
        }

        return $return_code;
    }
}
