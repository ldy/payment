<?php

/**
 * @author: yeran
 * @createTime: 2018-10-30 11:30
 * @description: 查询实例化
 */

namespace Payment;

use Payment\Common\BaseStrategy;
use Payment\Common\PayException;
use Payment\Query\Ali\AliChargeQuery;
use Payment\Query\Ali\AliRefundQuery;
use Payment\Query\Ali\AliTransferQuery;
use Payment\Query\Cmb\CmbChargeQuery;
use Payment\Query\Cmb\CmbRefundQuery;
use Payment\Query\Fu\FuChargeQuery;
use Payment\Query\Fu\FuRefundQuery;
use Payment\Query\LTF\LTFChargeQuery;
use Payment\Query\LTF\LTFRefundQuery;
use Payment\Query\MiPay\MiQuery;
use Payment\Query\Sw\SwChargeQuery;
use Payment\Query\SwThree\SwChargeThreeQuery;
use Payment\Query\TLpay\TLQuery;
use Payment\Query\Wx\WxChargeQuery;
use Payment\Query\Wx\WxPayBankQuery;
use Payment\Query\Wx\WxRefundQuery;
use Payment\Query\Wx\WxTransferQuery;
use Payment\Query\YS\YSChargeQuery;
use Payment\Query\YSE\YSEChargeQuery;
use Payment\Query\WNF\WNFChargeQuery;
use Payment\Query\Hsq\HsqChargeQuery;
use Payment\Query\Hsq\HsqRefundQuery;
use Payment\Query\Hsq\HsqTransferQuery;

class QueryContext
{
    /**
     * 查询的渠道
     * @var BaseStrategy
     */
    protected $query;


    /**
     * 设置对应的查询渠道
     * @param string $channel 查询渠道
     *  - @param array $config 配置文件
     * @throws PayException
     *
     * @see Config
     *
     */
    public function initQuery($channel, array $config)
    {
        try {
            switch ($channel) {
                case Config::ALI_CHARGE:
                    $this->query = new AliChargeQuery($config);
                    break;
                case Config::ALI_REFUND:// 支付宝退款订单查询
                    $this->query = new AliRefundQuery($config);
                    break;
                case Config::ALI_TRANSFER:
                    $this->query = new AliTransferQuery($config);
                    break;

                case Config::WX_CHARGE:// 微信支付订单查询
                    $this->query = new WxChargeQuery($config);
                    break;
                case Config::WX_REFUND:// 微信退款订单查询
                    $this->query = new WxRefundQuery($config);
                    break;
                case Config::WX_TRANSFER: //微信企业付款到零钱订单查询
                    $this->query = new WxTransferQuery($config);
                    break;
                case Config::WX_PAY_BANK: //微信企业付款到银行卡订单查询
                    $this->query = new WxPayBankQuery($config);
                    break;

                case Config::CMB_CHARGE:// 招商支付查询
                    $this->query = new CmbChargeQuery($config);
                    break;
                case Config::CMB_REFUND:// 招商退款查询
                    $this->query = new CmbRefundQuery($config);
                    break;
                case Config::TL_QUERY:// 通联查询
                    $this->query = new TLQuery($config);
                    break;
                case Config::MI_QUERY:// Mi付查询
                    $this->query = new MiQuery($config);
                    break;

                case Config::SW_QUERY:// 扫呗查询
                    $this->query = new SwChargeQuery($config);
                    break;
                case Config::SW_T_QUERY:// 扫呗(3.0)查询
                    $this->query = new SwChargeThreeQuery($config);
                    break;
                case Config::FU_CHARGE:
                    $this->query = new FuChargeQuery($config);
                    break;
                case Config::FU_REFUND:
                    $this->query = new FuRefundQuery($config);
                    break;
				case Config::LTF_CHARGE:
					$this->query=new LTFChargeQuery($config);
					break;
				case Config::LTF_REFUND:
					$this->query=new LTFRefundQuery($config);
					break;

                case Config::YS_QUERY://易生支付
                    $this->query  = new YSChargeQuery($config);
                    break;
                case Config::YSE_QUERY://银盛支付
                    $this->query  = new YSEChargeQuery($config);
                    break;
                case Config::WNF_QUERY://微诺付
                    $this->query = new WNFChargeQuery($config);
                    break;
                case Config::HSQ_QUERY://慧收钱
                    $this->query = new HsqChargeQuery($config);
                    break;
                case Config::HSQ_QUERY_REFUND://慧收钱退款查询
                    $this->query = new HsqRefundQuery($config);
                    break;
                case Config::HSQ_QUERY_TRANSFER: //宝付代付交易状态查证接口
                    $this->query = new HsqTransferQuery($config);
                    break;
                default:
                    throw new PayException('当前仅支持：ALI_CHARGE ALI_REFUND WX_CHARGE WX_REFUND WX_TRANSFER WX_PAY_BANK CMB_CHARGE CMB_REFUND TLPAY MI_QUERY SW_QUERY FU_CHARGE FU_REFUND WNF_QUERY HSQ_QUERY HSQ_QUERY_REFUND HSQ_QUERY_TRANSFER ');
            }
        } catch (PayException $e) {
            throw $e;
        }
    }

    /**
     * 通过环境类调用支付异步通知
     *
     * @param array $data
     *      // 二者设置一个即可
     *      $data => [
     *          'transaction_id'    => '原付款支付宝交易号',
     *          'order_no' => '商户订单号',
     *      ];
     *
     * @return array
     * @throws PayException
     *
     */
    public function query(array $data)
    {
        if (!$this->query instanceof BaseStrategy) {
            throw new PayException('请检查初始化是否正确');
        }

        try {
            return $this->query->handle($data);
        } catch (PayException $e) {
            throw $e;
        }
    }
}
