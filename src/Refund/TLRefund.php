<?php
/**
 * @author: yeran
 * @createTime: 2018-04-22 19:01
 * @description:
 */

namespace Payment\Refund;

use Payment\Common\TLConfig;
use Payment\Common\TLpay\Data\Cancel\RefundData;
use Payment\Config;
use Payment\Common\TLpay\TLBaseStrategy;

/**
 * Class TLRefund
 * 微信退款操作
 * @package Payment\Refund
 * anthor yeran
 */
class TLRefund extends TLBaseStrategy
{
    public function getBuildDataClass()
    {
        return RefundData::class;
    }

    /**
     * 返回退款的url
     * @return null|string
     *
     */
    protected function getReqUrl()
    {
        return TLConfig::REFUND_URL;
    }

    protected function retData(array $ret)
    {
        //cusid	商户号		平台分配的商户号	否	15
        //appid	应用ID		平台分配的APPID	否	8
        //trxid	交易单号		收银宝平台的退款交易流水号	否	20
        //reqsn	商户订单号		商户的退款交易订单号	否	32
        //trxstatus	交易状态		交易的状态	否	4	见附录3.1
        //fintime	交易完成时间		yyyyMMddHHmmss	是	14
        //errmsg	错误原因		失败的原因说明	是	100
        //randomstr	随机字符串		随机生成的字符串	否	32
        //sign	签名			否	32	详见1.5

        //返回数组结构
        return $ret;
    }


}
