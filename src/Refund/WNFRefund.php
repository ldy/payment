<?php

namespace Payment\Refund;

use Payment\Common\WnfConfig;
use Payment\Common\WNFpay\Data\RefundData;
use Payment\Common\WNFpay\WNFBaseStrategy;

class WNFRefund extends WNFBaseStrategy
{
    public function getBuildDataClass(){
        return RefundData::class;
    }


    public function getReqUrl($url = null){
        return parent::getReqUrl($url ?? WnfConfig::REFUND_URL);
    }
}