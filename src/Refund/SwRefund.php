<?php
/**
 *
 * @createTime: 2019-03-24 22:42
 * @description: 退款
 */

namespace Payment\Refund;

use Payment\Common\Sw\Data\BackRefundData;
use Payment\Common\Sw\Data\RefundData;
use Payment\Common\Sw\SwBaseStrategy;
use Payment\Common\SwConfig;

/**
 * 需要商户当前账户内有大于退款金额的余额，否则会造成余额不足，退款失败；
 * 限支付30天内退款，超过30天，不能进行退款操作（具体退款限制时间由通道决定）。
 *
 * Class SwRefund
 * @package Payment\Refund\Sw
 */
class SwRefund extends SwBaseStrategy
{

    /**
     * 返回查询订单的数据
     * 
     */
    public function getBuildDataClass()
    {
        return RefundData::class;
    }

    /**
     * 返回微信查询的url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url=null)
    {
        return parent::getReqUrl($url??SwConfig::REFUND_URL);
    }

    /**
     * 处理通知的返回数据
     * @param array $ret
     * @return mixed
     * 
     */
    protected function retData(array $ret)
    {
        $back = new BackRefundData($this->config, $ret);
        $backData = $back->getData();
        // 移除sign
        unset($backData['sign']);

        return $backData;
    }
}
