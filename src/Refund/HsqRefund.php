<?php

namespace Payment\Refund;

use Payment\Common\Hsq\Data\RefundData;
use Payment\Common\Hsq\HsqBaseStrategy;
use Payment\Common\HsqConfig;

/**
 * 交易退款
 *
 * 总退款金额不能超过订单总金额
 * 目前支持对90天以内的交易进行退款，支持全额或者是多次部分退款
 * 如需要30天外交易退款，需传入 reserved_origi_dt 字段，如该字段不传入，则仅支持30内交易进行退款
 *
 * Class FuRefund
 * @package Payment\Refund
 */
class HsqRefund extends HsqBaseStrategy
{

    /**
     * 返回查询订单的数据
     *
     */
    public function getBuildDataClass()
    {
        $this->config->method = HsqConfig::REFUND_URL;
        return RefundData::class;
    }

    /**
     * 返回微信查询的url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url);
    }

    /**
     * retData
     * @param array $data
     * @return array|mixed
     */
    protected function retData(array $data)
    {
        $backData = json_decode($data['result'] ,true);
        return $backData;
    }

}
