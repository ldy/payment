<?php
/**
 * @author: yeran
 * @createTime: 2018-04-22 19:01
 * @description:
 */

namespace Payment\Refund;

use Payment\Common\MiConfig;
use Payment\Common\MiPay\Data\Cancel\RefundData;
use Payment\Common\MiPay\MiBaseStrategy;

/**
 * Class MiRefund
 * 微信退款操作
 * @package Payment\Refund
 * @author yeran
 */
class MiRefund extends MiBaseStrategy
{
    public function getBuildDataClass()
    {
        return RefundData::class;
    }

    /**
     * 返回退款的url
     * @return null|string
     *
     */
    protected function getReqUrl()
    {
        return MiConfig::GATEWAY_URL;
    }

    protected function retData(array $ret)
    {

        //返回数组结构
        return $ret;
    }


}
