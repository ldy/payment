<?php

namespace Payment\Refund;

use Payment\Common\Fu\Data\RefundData;
use Payment\Common\Fu\FuBaseStrategy;
use Payment\Common\FuConfig;

/**
 * 交易退款
 *
 * 总退款金额不能超过订单总金额
 * 目前支持对90天以内的交易进行退款，支持全额或者是多次部分退款
 * 如需要30天外交易退款，需传入 reserved_origi_dt 字段，如该字段不传入，则仅支持30内交易进行退款
 *
 * Class FuRefund
 * @package Payment\Refund
 */
class FuRefund extends FuBaseStrategy
{

    /**
     * 返回查询订单的数据
     *
     */
    public function getBuildDataClass()
    {
        return RefundData::class;
    }

    /**
     * 返回微信查询的url
     * @param null $url
     * @return string
     */
    protected function getReqUrl($url = null)
    {
        return parent::getReqUrl($url ?? FuConfig::REFUND_URL);
    }

    /**
     * retData
     * @param array $data
     * @return array|mixed
     */
    protected function retData(array $data)
    {
        $backData = [
            'order_type' => $data['order_type'] ?? '', //订单类型
            'order_no' => $data['mchnt_order_no'] ?? '', //原交易商户订单号
            'refund_no' => $data['refund_order_no'] ?? '', //商户退款单号
            'out_trade_no' => $data['transaction_id'] ?? '', //渠道交易流水号
            'out_refund_no' => $data['refund_id'] ?? '', //渠道撤销流水号
            'refund_fee' => $data['reserved_refund_amt'] ?? 0, //退款金额
            'other' => $data
        ];
        return $backData;
    }

}
