<?php
/**
 * @createTime: 2016-07-14 17:47
 * @description: 支付相关的基础配置  无法被继承
 * @link      https://github.com/helei112g/payment/tree/paymentv2
 * @link      https://helei112g.github.io/
 * @version 2.6.1
 * @version 3.0.0 by yeran 2018-10-30
 * @version 3.2.0 by wangyu 2019-06-18 新增富友支付
 */

namespace Payment;

final class Config{

	const VERSION = '3.2.1';

	//======================= 账户类型 ======================//
	const WECHAT_PAY = 'wechat';

	const ALI_PAY = 'ali';

	const CMB_PAY = 'cmb';

	const TL_PAY = 'tl'; //通联支付

	const MI_PAY = 'mipay'; //米付

	const SW_PAY = 'swpay';//扫呗支付

	const FU_PAY = 'fupay'; //富友支付

	const LTF_PAY = 'ltfpay'; //富友支付

	const YS_PAY = 'yspay'; //易生支付

	const YSE_PAY = 'ysepay'; //盛银支付

	const SW_T_PAY = 'swpay_t'; //扫呗支付(3.0)

    const WNF_PAY = 'wnfpay'; //微诺付

    const HSQ_PAY = 'hsqpay'; //慧收钱支付

    const BAOFOO_PAY = 'baofoopay'; //宝付代付

	//========================= 金额问题设置 =======================//
	const PAY_MIN_FEE = '0.01';// 支付的最小金额

	const TRANS_FEE = '50000';// 转账达到这个金额，需要添加额外信息

	//======================= 交易状态常量定义 ======================//
	const TRADE_STATUS_SUCC = 'success';// 交易成功

	const TRADE_STATUS_FAILD = 'not_pay';// 交易未完成

	//========================= ali相关接口 =======================//
	// 支付相关常量
	const ALI_CHANNEL_APP = 'ali_app';// 支付宝 手机app 支付

	const ALI_CHANNEL_WAP = 'ali_wap';// 支付宝 手机网页 支付

	const ALI_CHANNEL_WEB = 'ali_web';// 支付宝 PC 网页支付

	const ALI_CHANNEL_QR = 'ali_qr';// 支付宝 扫码支付

	const ALI_CHANNEL_BAR = 'ali_bar';// 支付宝 条码支付

	// 其他操作常量
	const ALI_CHARGE = 'ali_charge';// 支付

	const ALI_REFUND = 'ali_refund';// 退款

	const ALI_RED = 'ali_red';// 红包

	const ALI_TRANSFER = 'ali_transfer';// 转账

	//========================= 微信相关接口 =======================//
	// 支付常量
	const WX_CHANNEL_APP = 'wx_app';// 微信 APP 支付

	const WX_CHANNEL_PUB = 'wx_pub';// 微信 公众账号 支付

	const WX_CHANNEL_QR = 'wx_qr';// 微信 扫码支付  (可以使用app的帐号，也可以用公众的帐号完成)

	const WX_CHANNEL_BAR = 'wx_bar';// 微信 刷卡支付，与支付宝的条码支付对应

	const WX_CHANNEL_LITE = 'wx_lite';// 微信小程序支付

	const WX_CHANNEL_WAP = 'wx_wap';// 微信wap支付，针对特定用户

	// 其他相关常量
	const WX_CHARGE = 'wx_charge';// 支付

	const WX_REFUND = 'wx_refund';// 退款

	const WX_RED = 'wx_red';// 红包

	const WX_TRANSFER = 'wx_transfer'; //微信企业付款到零钱

	const WX_PAY_BANK = 'wx_pay_bank'; //微信企业付款到银行卡

	const APPLE_PAY = 'applepay_upacp';//apple支付

	//========================= 招商相关接口 =======================//
	// 支付常量
	const CMB_CHANNEL_APP = 'cmb_app';// 招商 app  ，实际上招商并无该概念

	const CMB_CHANNEL_WAP = 'cmb_wap';// 招商h5支付，其实app支付也是使用的h5

	const CMB_BIND = 'cmb_bind';// 签约API

	const CMB_PUB_KEY = 'cmb_pub_key';// 查询招商公钥

	const CMB_CHARGE = 'cmb_charge';// 招商支付

	const CMB_REFUND = 'cmb_refund';// 招商退款

	//========================= 通联支付相关接口 =======================//
	// 支付常量
	const TL_CHANNEL_APP = 'tl_pub';// 通联支付微信公众号h5支付

	const TL_CHANNEL_LITE = 'tl_lite';// 通联支付小程序支付

	const TL_BIND = 'tl_bind';// 签约API

	const TL_PUB_KEY = 'tl_pub_key';// 查询通联公钥

	const TL_CHARGE = 'tl_charge';// 通联支付-统一下单

	const TL_REFUND = 'tl_refund';// 通联退款

	const TL_CANCEL = 'tl_cancel';//通联交易撤销，只能撤销当天的交易，全额退款，实时返回退款结果

	const TL_QUERY = 'tl_query';

	//========================= 米付相关接口 =======================//
	// 支付常量
	const MI_CHANNEL_APP = 'mi_pub';// 米付微信公众号h5支付

	const MI_CHANNEL_LITE = 'mi_lite';// 米付小程序支付

	const MI_BIND = 'mi_bind';// 签约API

	const MI_PUB_KEY = 'mi_pub_key';// 查询公钥

	const MI_CHARGE = 'mi_charge';// 米付支付-统一下单

	const MI_REFUND = 'mi_refund';// 米付退款

	const MI_CANCEL = 'mi_cancel';// 米付交易撤销，只能撤销当天的交易，全额退款，实时返回退款结果

	const MI_QUERY = 'mi_query'; // 米付查询接口

	//========================= 扫呗相关接口 =======================//
    //http://www.lcsw.cn/doc/api/payment.html
	// 支付常量
	const SW_CHANNEL_PUB = 'sw_pub';// 微信公众号h5支付

	const SW_CHANNEL_LITE = 'sw_lite';// 小程序支付

	const SW_CHANNEL_SCAN = 'sw_scan';// 扫码支付

	const SW_CHANNEL_BAR = 'sw_bar';// 刷卡（条码）支付 用户被扫模式

	const SW_CHANNEL_FACEPAY = 'sw_facepay';// 自助收银收款

	const SW_CHANNEL_FACEPAY_TOKEN = 'sw_facepay_token';// 人脸识别初始化token

	const SW_BIND = 'sw_bind';// 签约API

	const SW_PUB_KEY = 'sw_pub_key';// 查询公钥

	const SW_CHARGE = 'sw_charge';// 统一下单

	const SW_REFUND = 'sw_refund';// 退款

	const SW_CANCEL = 'sw_cancel';// 交易撤销，只能撤销当天的交易，全额退款，实时返回退款结果

    const SW_QUERY = 'sw_query'; // 查询接口


    //========================= 扫呗（3.0）支付相关接口 =======================//
    //https://lcsw.yuque.com/docs/share/f8d585bb-e0b4-46d0-a83a-dfaedc6b55af?#9e870768

    const SW_T_CHANNEL_PUB = 'sw_t_pub';// 微信公众号h5支付

    const SW_T_CHANNEL_LITE = 'sw_t_lite';// 小程序支付

    const SW_T_CHANNEL_SCAN = 'sw_t_scan';// 扫码支付

    const SW_T_CHANNEL_BAR = 'sw_t_bar';// 刷卡（条码）支付 用户被扫模式

    const SW_T_CHANNEL_FACEPAY = 'sw_t_facepay';// 自助收银收款

    const SW_T_CHARGE = 'sw_t_charge';// 统一下单

    const SW_T_REFUND = 'sw_t_refund';// 退款

    const SW_T_CANCEL = 'sw_t_cancel';// 交易撤销，只能撤销当天的交易，全额退款，实时返回退款结果

    const SW_T_QUERY = 'sw_t_query'; // 查询接口

    //========================= 富友相关接口 =======================//
    // 支付常量
    const FU_CHANNEL_PUB = 'fu_pub';// 微信公众号h5支付

    const FU_CHANNEL_LITE = 'fu_lite';// 小程序支付

    const FU_CHANNEL_SCAN = 'fu_scan';// 扫码支付

    const FU_CHANNEL_BAR = 'fu_bar';// 刷卡（条码）支付 用户被扫模式

    const FU_CHARGE = 'fu_charge';// 统一下单

    const FU_REFUND = 'fu_refund';// 退款

    const FU_CANCEL = 'fu_cancel';// 交易撤销，只能撤销当天的交易，全额退款，实时返回退款结果

	//=======================联拓富（联富通）相关接口===================//
	//支付常量
	const LTF_CHANNEL_PUB = 'ltf_pub'; // 微信公众号h5支付

	const LTF_CHANNEL_LITE = 'ltf_lite';// 小程序支付

	const LTF_CHANNEL_SCAN = 'ltf_scan';// 扫码支付

	const LTF_CHANNEL_BAR = 'ltf_bar';// 刷卡（条码）支付 用户被扫模式

	const LTF_CHARGE = 'ltf_charge';// 统一下单

	const LTF_REFUND = 'ltf_refund';// 退款

	const LTF_CANCEL = 'ltf_cancel';// 交易撤销，只能撤销当天的交易，全额退款，实时返回退款结果

	const LTF_CHANNEL_PAY = 'ltf_pay';// 聚合支付

    //=======================易生支付 相关接口===================//
    //支付常量
    const YS_CHARGE = 'ys_charge';// 统一下单

    const YS_CHANNEL_PUB = 'ys_pub'; // 微信公众号h5支付

    const YS_CHANNEL_LITE = 'ys_lite';// 小程序支付

    const YS_CHANNEL_SCAN = 'ys_scan';// 付款码支付

//    const YS_CHANNEL_BAR = 'ys_bar';// 刷卡（条码）支付 用户被扫模式

    const YS_CHANNEL_NATIVE = 'ys_native';//主扫模式

    const YS_QUERY = 'ys_query'; // 查询接口

    const YS_REFUND = 'ys_refund';// 退款

    const YS_CANCEL = 'ys_cancel';// 交易撤销，只能撤销当天的交易，全额退款，实时返回退款结果

    //=======================银盛支付 相关接口===================//
    //支付常量
    const YSE_CHARGE = 'yse_charge';// 统一下单

    const YSE_REFUND = 'yse_refund';// 退款

    const YSE_QUERY = 'yse_query';// 订单查询

    const YSE_CHANNEL_PUB = 'yse_pub'; // 公众号支付

    const YSE_CHANNEL_LITE = 'yse_lite';// 小程序支付


    //=============================微诺付==================================

    const WNF_WECHAT_LITE = 'wnf_wx_lite'; //微信支付

    const WNF_ALI_APP = 'wnf_ali_lite'; //支付宝支付

    const WNF_QUERY = 'wnf_query'; //支付查询

    const WNF_REFUND = 'wnf_refund'; //支付退款


    //========================= 慧收钱相关接口 =======================//
    // 支付常量

    const HSQ_CHANNEL_APP = 'hsq_app'; //APP支付

    const HSQ_CHANNEL_LITE = 'hsq_lite'; //小程序支付

    const HSQ_CHANNEL_JS = 'hsq_JS'; //jsapi支付

    const HSQ_CHARGE = 'hsq_charge';// 慧收钱支付-统一下单

    // 其他相关常量
    const HSQ_QUERY = 'hsq_query'; //查询订单

    const HSQ_REFUND = 'hsq_refund'; //退款

    const HSQ_QUERY_REFUND = 'hsq_query_refund'; //查询退款

    const HSQ_TRANSFER = 'hsq_transfer'; //宝付代付交易

    const HSQ_QUERY_TRANSFER = 'hsq_query_transfer'; //宝付代付交易状态查证接口






}
